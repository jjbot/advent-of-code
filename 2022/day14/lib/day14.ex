defmodule Day14 do
  @nr_block ~r/(\d+),(\d+)/
  @poor_loc {500, 0}

  def get_input(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.flat_map(
      fn line ->
        Regex.scan(@nr_block, line)
        |> Enum.map(fn [_, x, y] -> {String.to_integer(x), String.to_integer(y)} end)
        |> Enum.chunk_every(2, 1, :discard)
      end)
    |> Enum.flat_map(
      fn [{from_x, from_y}, {to_x, to_y}] ->
        for x <- from_x..to_x, y <- from_y..to_y, do: {x, y}
      end)
    |> Enum.reduce(%{}, fn loc, acc -> Map.put(acc, loc, "#") end)
  end

  def add({x, y}, {dx, dy}), do: {x + dx, y + dy}

  def settle_sand(cave, {x, y} = location, min_x, max_x, max_y) do
    down = add(location, {0, 1})
    down_left = add(location, {-1, 1})
    down_right = add(location, {1, 1})

    cond do
      x < min_x -> {:halt, cave}
      x > max_x -> {:halt, cave}
      y > max_y -> {:halt, cave}
      !Map.has_key?(cave, down) -> settle_sand(cave, down, min_x, max_x, max_y)
      !Map.has_key?(cave, down_left) -> settle_sand(cave, down_left, min_x, max_x, max_y)
      !Map.has_key?(cave, down_right) -> settle_sand(cave, down_right, min_x, max_x, max_y)
      true -> {:cont, Map.put(cave, location, "O")}
    end
  end

  def part1() do
    cave = get_input("data/input.txt")

    {min_x, max_x} = Map.keys(cave) |> Enum.map(&(elem(&1, 0))) |> Enum.min_max()
    {_min_y, max_y} = Map.keys(cave) |> Enum.map(&(elem(&1, 1))) |> Enum.min_max()

    {i, _filled_cave} =
      Enum.reduce_while(
        1..100_000,
        cave,
        fn i, acc -> 
          case settle_sand(acc, @poor_loc, min_x, max_x, max_y) do
            {:cont, acc} -> {:cont, acc}
            {:halt, acc} -> {:halt, {i, acc}}
          end
        end)
    i - 1 # Remove the one that doesn't land anywhere
  end

  def top_sand(cave, location) do
    down = add(location, {0, 1})
    down_left = add(location, {-1, 1})
    down_right = add(location, {1, 1})

    cond do
      !Map.has_key?(cave, down) -> top_sand(cave, down)
      !Map.has_key?(cave, down_left) -> top_sand(cave, down_left)
      !Map.has_key?(cave, down_right) -> top_sand(cave, down_right)
      location == @poor_loc -> {:halt, cave}
      true -> {:cont, Map.put(cave, location, "O")}
    end
  end

  def part2() do
    cave = get_input("data/input.txt")

    {_min_y, max_y} = Map.keys(cave) |> Enum.map(&(elem(&1, 1))) |> Enum.min_max()

    cave = Enum.reduce(-1000..2000, cave, fn i, c -> Map.put(c, {i, max_y + 2}, "#") end)
    
    {i, _filled_cave} =
      Enum.reduce_while(
        1..100_000,
        cave,
        fn i, acc -> 
          case top_sand(acc, @poor_loc) do
            {:cont, acc} -> {:cont, acc}
            {:halt, acc} -> {:halt, {i, acc}}
          end
        end)
    i # don't subtract anything, we're counting the last one too
  end
end
