defmodule Day6 do
  def get_input(filename) do
    filename
    |> File.read!()
    |> String.trim()
    |> String.codepoints()
  end

  def part1() do
    "data/input.txt"
    |> get_input()
    |> Enum.chunk_every(4, 1, :discard)
    |> Enum.map(&MapSet.new/1)
    |> Enum.map(&MapSet.size/1)
    |> Enum.with_index(0)
    |> Enum.filter(fn {count, _loc} -> count == 4 end)
    |> Enum.map(fn {count, loc} -> count + loc end)
    |> Enum.take(1)
  end

  def part2() do
    "data/input.txt"
    |> get_input()
    |> Enum.chunk_every(14, 1, :discard)
    |> Enum.map(&MapSet.new/1)
    |> Enum.map(&MapSet.size/1)
    |> Enum.with_index(0)
    |> Enum.filter(fn {count, _loc} -> count == 14 end)
    |> Enum.map(fn {count, loc} -> count + loc end)
    |> Enum.take(1)
  end
end

