defmodule Day1 do
  def read_input(filename) do
    filename
    |> File.read!
    |> String.split("\n")
    |> Enum.chunk_by(fn str -> str == "" end)
    |> Enum.reject(fn l -> l == [""] end)
    |> Enum.map(fn l -> Enum.map(l, &String.to_integer/1) end)
  end

  def part1() do
    "data/input.txt"
    |> read_input()
    |> Enum.map(&Enum.sum/1)
    |> Enum.max
  end

  def part2() do
    "data/input.txt"
    |> read_input()
    |> Enum.map(&Enum.sum/1)
    |> Enum.sort()
    |> Enum.reverse()
    |> Enum.take(3)
    |> Enum.sum()
  end
end
