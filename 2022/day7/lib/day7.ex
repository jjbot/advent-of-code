defmodule Day7 do
  def get_input(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.split(&1, " ", trim: true))
  end

  def change_dir("/", dir_tree, _cur_path), do: {dir_tree, ["/"]}
  def change_dir("..", dir_tree, [_d | up_path]), do: {dir_tree, up_path}
  def change_dir(dir, dir_tree, cur_path) when is_binary(dir) do
    {dir_tree, [dir | cur_path]}
  end

  def add_dir(dir_name, dir_tree, cur_path) do
    dt = put_in(dir_tree, Enum.reverse([dir_name | cur_path]), %{})
    {dt, cur_path}
  end

  def add_file(name, size, dir_tree, cur_path) do
    s = String.to_integer(size)
    p = Enum.reverse([name | cur_path])
    dt = put_in(dir_tree, p, s)
    {dt, cur_path}
  end

  def process_line(line, {dir_tree, cur_path}) do
    case line do
      ["$", "ls"] -> {dir_tree, cur_path}
      ["$", "cd", val] -> change_dir(val, dir_tree, cur_path)
      ["dir", val] -> add_dir(val, dir_tree, cur_path)
      [size, name] -> add_file(name, size, dir_tree, cur_path)
    end
  end

  def get_dir_sizes(dir_tree, path, sizes) do
    p = Enum.reverse(path)
    {size, dirs} =
      Enum.reduce(
        Map.keys(get_in(dir_tree, p)),
        {0, []},
        fn name, {sum, dirs} ->
          full_path = [name | path]
          case get_in(dir_tree, Enum.reverse(full_path)) do
            size when is_integer(size) -> {sum + size, dirs}
            _dir ->
              {dir_size, new_dirs} = get_dir_sizes(dir_tree, full_path, dirs)
              {sum + dir_size, new_dirs}
          end
        end
      )
    {size, [{path, size} | sizes] ++ dirs}

  end

  def part1() do
    {dir_tree, _cur_dir} =
      "data/input.txt"
      |> get_input()
      |> Enum.reduce({%{"/" => %{}}, ["/"]}, fn line, acc -> process_line(line, acc) end)

    {_, dirs} = get_dir_sizes(dir_tree, ["/"], [])

    dirs
    |> Enum.filter(fn {_, size} -> size <= 100_000 end)
    |> Enum.map(fn {_, size} -> size end)
    |> Enum.sum()
  end

  def part2() do
    total_file_system = 70_000_000
    total_needed = 30_000_000
    cur_size = 46_090_134

    cur_available = total_file_system - cur_size
    to_delete = total_needed - cur_available

    {dir_tree, _cur_dir} =
      "data/input.txt"
      |> get_input()
      |> Enum.reduce({%{"/" => %{}}, ["/"]}, fn line, acc -> process_line(line, acc) end)

    {_, dirs} = get_dir_sizes(dir_tree, ["/"], [])

    dirs
    |> Enum.map(fn {_, size} -> {size, size - to_delete} end)
    |> Enum.reject(fn {_, left} -> left < 0 end)
    |> Enum.map(fn {size, _left} -> size end)
    |> Enum.min()
  end
end

