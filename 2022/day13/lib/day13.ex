defmodule Day13 do
  def get_input(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.chunk_every(2)
    |> Enum.map(
      fn [left, right] ->
        {left, _} = Code.eval_string(left)
        {right, _} = Code.eval_string(right)
        {left, right}
      end)
  end

  # Case of empty lists
  def check_order({[], []}), do: :cont

  # Case where the numbers are the same
  def check_order({[e | lrm], [e | rrm]}) when is_integer(e), do: check_order({lrm, rrm})

  # Case where both are numbers and not the same
  def check_order({[lh | _lrm], [rh | _rrm]}) when is_integer(lh) and is_integer(rh), do: lh < rh

  # Case where left side ran out before right side
  def check_order({[], [_rh | _rrm]}), do: true

  # Case where right side ran out before left side
  def check_order({[_lh | _lrm], []}), do: false

  # Case where both items are lists
  def check_order({[lh | lrm], [rh | rrm]}) when is_list(lh) and is_list(rh) do
    case check_order({lh, rh}) do
      :cont -> check_order({lrm, rrm})
      truth -> truth
    end
  end

  # Case where left is nr but right is list
  def check_order({[lh | lrm], [rh | _rrm] = r}) when is_integer(lh) and is_list(rh) do
    check_order({[[lh] | lrm], r})
  end

  # Case where left is list but right is number
  def check_order({[lh | _lrm] = l, [rh | rrm]}) when is_list(lh) and is_number(rh) do
    check_order({l, [[rh] | rrm]})
  end

  # Needed to make Enum.sort work
  def check_order(l, r), do: check_order({l, r})

  def part1() do
    "data/input.txt"
    |> get_input()
    |> Enum.with_index(1)
    |> Enum.filter(fn {pair, _index} -> check_order(pair) end)
    |> Enum.map(fn {_, idx} -> idx end)
    |> Enum.sum()
  end

  def part2() do
    parsed =
      "data/input.txt"
      |> get_input()
      |> Enum.reduce([], fn {l, r}, acc -> [l, r | acc] end)

    sorted =
      [[[2]], [[6]] | parsed]
      |> Enum.sort(&check_order/2)

    packet1 = Enum.find_index(sorted, &(&1 == [[2]])) + 1
    packet2 = Enum.find_index(sorted, &(&1 == [[6]])) + 1
    packet1 * packet2

  end
end

