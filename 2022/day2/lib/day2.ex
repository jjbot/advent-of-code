defmodule Day2 do
  # A = Rock
  # B = Paper
  # C = Scissors

  # X = Rock
  # Y = Paper
  # Z = Scissors

  @winning [{"C", "X"}, {"A", "Y"}, {"B", "Z"}]
  @draw [{"A", "X"}, {"B", "Y"}, {"C", "Z"}]
  @scores %{"X" => 1, "Y" => 2, "Z" => 3}

  @lose %{"A" => "Z", "B" => "X", "C" => "Y"}
  @tie %{"A" => "X", "B" => "Y", "C" => "Z"}
  @win %{"A" => "Y", "B" => "Z", "C" => "X"}

  def read_input(filename) do
    filename
    |> File.read!()
    |> String.trim()
    |> String.split("\n")
    |> Enum.map(&String.split(&1, " "))
    |> Enum.map(fn [o, p] -> {o, p} end)
  end

  def determine_score({_o, p} = m) do
    play_score =
      case m do
        m when m in @winning -> 6
        m when m in @draw -> 3
        _ -> 0
      end

    play_score + @scores[p]
  end

  def part1() do
    "data/input.txt"
    |> read_input()
    |> Enum.map(&determine_score/1)
    |> Enum.sum()
  end

  def pick_hand(l) do
    case l do
      {opponent, "X"} -> {opponent, @lose[opponent]}
      {opponent, "Y"} -> {opponent, @tie[opponent]}
      {opponent, "Z"} -> {opponent, @win[opponent]}
    end
  end

  def part2() do
    "data/input.txt"
    |> read_input()
    |> Enum.map(&pick_hand/1)
    |> Enum.map(&determine_score/1)
    |> Enum.sum()
  end
end
