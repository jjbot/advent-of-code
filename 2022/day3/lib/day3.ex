defmodule Day3 do
  def compartimentalize(content) do
    Enum.split(content, div(length(content), 2))
  end

  def char_to_prio(c) do
    case c do
      c when c > 64 and c < 91 -> c - 38
      c when c > 96 and c < 123 -> c - 96
    end
  end

  def get_input(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_charlist/1)
  end

  def get_overlap({l1, l2}) do
    s1 = MapSet.new(l1)
    s2 = MapSet.new(l2)
    s = MapSet.intersection(s1, s2)
    [item] = MapSet.to_list(s)
    item
  end

  def part1() do
    "data/input.txt"
    |> get_input()
    |> Enum.map(&compartimentalize/1)
    |> Enum.map(&get_overlap/1)
    |> Enum.map(&char_to_prio/1)
    |> Enum.sum()
  end

  def find_badge_type([a, b, c]) do
    [item] =
      MapSet.intersection(a, b)
      |> MapSet.intersection(c)
      |> MapSet.to_list()
    item
  end

  def part2() do
    "data/input.txt"
    |> get_input()
    |> Enum.map(&MapSet.new/1)
    |> Enum.chunk_every(3)
    |> Enum.map(&find_badge_type/1)
    |> Enum.map(&char_to_prio/1)
    |> Enum.sum()
  end
end
