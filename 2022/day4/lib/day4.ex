defmodule Day4 do
  @loc_re ~r/(\d+)-(\d+),(\d+)-(\d+)/

  def extract_line(line) do
    [_ | locations] = Regex.run(@loc_re, line)
    [start1, stop1, start2, stop2] = Enum.map(locations, &String.to_integer/1)
    {start1, stop1, start2, stop2}
  end

  def get_input(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&extract_line/1)
  end

  def check_contained({start1, stop1, start2, stop2}) do
    cond do
      start1 >= start2 and stop1 <= stop2 -> true
      start2 >= start1 and stop2 <= stop1 -> true
      true -> false
    end
  end

  def part1() do
    "data/input.txt"
    |> get_input()
    |> Enum.filter(&check_contained/1)
    |> length()
  end

  def generate_sets({start1, stop1, start2, stop2}) do
    s1 =
      Enum.to_list(start1..stop1)
      |> MapSet.new()
    s2 =
      Enum.to_list(start2..stop2)
      |> MapSet.new()

    {s1, s2}
  end

  def check_overlap({s1, s2}) do
    !MapSet.disjoint?(s1, s2)
  end

  def part2() do
    "data/input.txt"
    |> get_input()
    |> Enum.map(&generate_sets/1)
    |> Enum.filter(&check_overlap/1)
    |> length()
  end
end
