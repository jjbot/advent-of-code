defmodule Day10 do
  @nr_re ~r/\w+ ([-|\d]+)/
  @relevant_cyles [20, 60, 100, 140, 180, 220]

  def parse_line(line) do
    case line do
      "noop" -> :noop
      line ->
        [_, match] = Regex.run(@nr_re, line)
        {:addx, String.to_integer(match)}
    end
  end

  def get_input(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_line/1)
  end

  def do_op(:noop, [h | _] = cycles), do: [h | cycles]
  def do_op({:addx, amount}, [h | _] = cycles), do: [h + amount, h | cycles]

  def part1() do
    cycle_values =
      "data/input.txt"
      |> get_input()
      |> Enum.reduce([1], fn op, acc -> do_op(op, acc) end)
      |> Enum.reverse()
      |> Enum.with_index(fn elem, index -> {index + 1, elem} end)
      |> Enum.into(%{})

    @relevant_cyles
    |> Enum.map(fn cycle -> cycle * cycle_values[cycle] end)
    |> Enum.sum()
  end

  def part2() do
    "data/input.txt"
    |> get_input()
    |> Enum.reduce([1], fn op, acc -> do_op(op, acc) end)
    |> Enum.reverse()
    |> Enum.with_index(fn elem, index -> {index, elem} end)
    |> Enum.map(
      fn {idx, elem} ->
        pos = rem(idx, 40)
        if pos >= elem - 1 and pos <= elem + 1 do
          "#"
        else
          " "
        end
      end
    )
    |> Enum.chunk_every(40)
    |> Enum.map(fn line -> Enum.join(line, "") end)
    |> Enum.each(&IO.puts/1)
  end
end

