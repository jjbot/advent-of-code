defmodule Day5 do
  @line_re ~r/move (\d+) from (\d) to (\d)/

  def get_locs(nrs) do
    nrs
    |> String.split("")
    |> Enum.with_index()
    |> Enum.reduce(%{},
      fn {l, i}, acc -> 
        case l do
          "" -> acc
          " " -> acc
          c -> Map.put(acc, i, c)
        end
      end
    )
  end

  def get_crates(line, locs) do
    line
    |> String.split("")
    |> Enum.with_index(fn e, i -> {i, e} end)
    |> Enum.filter(fn {i, e} -> i in locs and e != " " end)
    |> Enum.into(%{})
  end

  def get_stacks(raw) do
    [nrs | stack_lines] = Enum.reverse(raw)

    pile_locs = get_locs(nrs)
    pile_positions = Map.keys(pile_locs)

    piles = Enum.reduce(Map.values(pile_locs), %{}, fn e, acc -> Map.put(acc, e, []) end )

    stack_lines
    |> Enum.map(fn line -> get_crates(line, pile_positions) end)
    |> Enum.reduce(piles,
      fn boxes, acc ->
        Enum.reduce(boxes, acc,
          fn {l, v}, a ->
            Map.update!(a, pile_locs[l], fn old -> [v | old] end)
          end)
      end)
  end

  def get_moves(lines) do
    lines
    |> Enum.map(
      fn line -> 
        [_, amount, from, to] = Regex.run(@line_re, line)
        {String.to_integer(amount), from, to}
      end)
  end

  def get_input(filename) do
    [stack_lines, _, instruction_lines, _] =
      filename
      |> File.read!
      |> String.split("\n")
      |> Enum.chunk_by(fn l -> l == "" end)

    stacks = get_stacks(stack_lines)

    moves = get_moves(instruction_lines)

    {stacks, moves}
  end

  def part1() do
    {stacks, moves} = get_input("data/input.txt")

    moves
    |> Enum.reduce(stacks,
      fn {amount, from, to}, s ->
        Enum.reduce(1..amount, s,
          fn _, loc_stacks ->
            [transfer | remainder] = loc_stacks[from]
            added = [transfer | loc_stacks[to]]
            Map.merge(s, %{from => remainder, to => added})
          end)
      end)
      |> Enum.map(fn {k, [v | _]} -> {k, v} end)
  end

  def part2() do
    {stacks, moves} = get_input("data/input.txt")

    moves
    |> Enum.reduce(stacks,
      fn {amount, from, to}, loc_stacks ->
         {transfer, remainder} = Enum.split(loc_stacks[from], amount)
         added = transfer ++ loc_stacks[to]
         Map.merge(loc_stacks, %{from => remainder, to => added})
      end)
    |> Enum.map(fn {k, [v | _]} -> {k, v} end)
  end
end
