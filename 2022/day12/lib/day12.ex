defmodule Day12 do
  @heights "SabcdefghijklmnopqrstuvwxyzE"
    |> String.graphemes()
    |> Enum.with_index(&({&1, &2}))
    |> Enum.into(%{})

  @directions [{-1, 0}, {1, 0}, {0, -1}, {0, 1}]
  @alot 10_000_000

  def get_heights() do
    @heights
  end

  def get_input(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.split(&1, "", trim: true))
    |> Enum.map(&Enum.with_index/1)
    |> Enum.with_index()
    |> Enum.reduce(%{},
      fn {line, y}, acc ->
        Enum.reduce(line, acc,
          fn {value, x}, acc -> Map.put(acc, {x, y}, @heights[value])
        end)
      end)
  end

  def step({x, y}, {dx, dy}), do: {x + dx, y + dy}

  def possible_steps(cur_pos, height_map, distances) do
    cur_pos_distance = Map.get(distances, cur_pos, @alot)

    @directions
    |> Enum.map(fn direction ->
      step(cur_pos, direction)
    end)
    |> Enum.filter(
      fn new_pos ->
        Map.get(height_map, new_pos, 100) - height_map[cur_pos] < 2 and 
        cur_pos_distance + 1 < Map.get(distances, new_pos, @alot)
      end)
  end

  def update_distances(height_map, distances, q) do
    {{:value, cur_loc}, q} = :queue.out(q)
    cur_distance = distances[cur_loc]

    steps = possible_steps(cur_loc, height_map, distances)
    
    # Add locations that need to be revisited to the queue, if a location is
    # already in there, skip it.
    q = Enum.reduce(
      steps, q,
      fn step, acc ->
        if(:queue.member(step, acc), do: acc,
        else: :queue.in(step, acc))
      end)
    
    # Update the distances for the locations that now have a shorter minium
    # distance.
    distances = Enum.reduce(
      steps, distances,
      fn step, acc -> Map.put(acc, step, cur_distance + 1) end
    )

    # Would be neater if this can be checked in a function header.
    if :queue.is_empty(q) do
      distances
    else
      update_distances(height_map, distances, q)
    end
  end

  def find_path(start_loc, _destination, height_map, distances) do
    distances = Map.put(distances, start_loc, 0)
    update_distances(height_map, distances, :queue.from_list([start_loc]))
  end
   
  def part1() do
    height_map =
      "data/input.txt"
      |> get_input()

    # This filtering works because you know there is only one of each
    [{start_loc, _}] = Enum.filter(height_map, fn {_k, v} -> v == 0 end)
    [{destination, _}] = Enum.filter(height_map, fn {_k, v} -> v == 27 end)

    # S has the same elevation as a
    height_map = Map.put(height_map, start_loc, 1)
    # E has the same elevation as z
    height_map = Map.put(height_map, destination, 26)

    # Init the distances map
    distances =
      (for x <- 0..144, y <- 0..41, do: {{x, y}, @alot})
      |> Enum.into(%{})

    IO.inspect(destination)
    find_path(start_loc, destination, height_map, distances)
  end

  def part2() do
    height_map =
      "data/input.txt"
      |> get_input()

    [{start_loc, _}] = Enum.filter(height_map, fn {_k, v} -> v == 0 end)
    [{destination, _}] = Enum.filter(height_map, fn {_k, v} -> v == 27 end)

    # S has the same elevation as a
    height_map = Map.put(height_map, start_loc, 1)
    # E has the same elevation as z
    height_map = Map.put(height_map, destination, 26)

    distances =
      (for x <- 0..144, y <- 0..41, do: {{x, y}, @alot})
      |> Enum.into(%{})

    height_map
    |> Enum.filter(fn {_, z} -> z == 1 end)
    |> Enum.map(fn {loc, _z} -> loc end)
    |> Enum.map(
      fn loc ->
        distances = find_path(loc, destination, height_map, distances)
        distances[destination]
      end)
    |> Enum.min()

  end
end

