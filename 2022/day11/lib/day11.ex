defmodule Monkey do
  defstruct [:nr, :items, :op, :test_nr, :true_target, :false_target, inspections: 0]

  def process_op(line) do
    [_, operation, offset] = Regex.run(~r/.*([+ | *]) (.+)/, line)

    case {operation, offset} do
      {"+", "old"} -> fn x -> x + x end
      {"+", i} ->
        increment = String.to_integer(i)
        fn x -> x + increment end
      {"*", "old"} -> fn x -> x * x end
      {"*", i} ->
        increment = String.to_integer(i)
        fn x -> x * increment end
    end
  end

  def from_block(block) do
    [monkey_line, item_line, op_line, test_line, ttrue_line, tfalse_line] = block

    [_, monkey_str] = Regex.run(~r/.*(\d+):/, monkey_line)
    monkey_nr = String.to_integer(monkey_str)

    items =
      Regex.scan(~r/(\d+)/, item_line)
      |> Enum.map(fn [x, x] -> String.to_integer(x) end)

    op = process_op(op_line)

    [_, test_str] = Regex.run(~r/.* (\d+)/, test_line)
    test = String.to_integer(test_str)

    [_, ttrue_str] = Regex.run(~r/.*(\d+)/, ttrue_line)
    ttrue = String.to_integer(ttrue_str)

    [_, tfalse_str] = Regex.run(~r/.*(\d+)/, tfalse_line)
    tfalse = String.to_integer(tfalse_str)

    %Monkey{nr: monkey_nr, items: items, op: op, test_nr: test, true_target: ttrue, false_target: tfalse}
  end

  def get_new_worry_level(old_worry, monkey) do
    # For part 1, just devide the worry level by 3, as given in the assignment
    # div(monkey.op.(old_worry), 3)

    # For part 2, you can take the remainder of the factor you are trying to check,
    # which is basically multiplying all the "divisible by" numbers
    rem(monkey.op.(old_worry), 9699690)
  end

  def play(monkey, item) do
    new_worry_level = get_new_worry_level(item, monkey)

    target = 
      case rem(new_worry_level, monkey.test_nr) do
        0 -> monkey.true_target
        _ -> monkey.false_target
      end

    {new_worry_level, target}
  end

  def process_items(monkey, monkeys) do
    new_monkeys =
      monkey.items
      |> Enum.reverse()
      |> Enum.reduce(
        monkeys,
        fn item, monkeys_acc ->
          {item_worry, target} = play(monkey, item)
          new_target_monkey = Map.update!(monkeys_acc[target], :items,
            fn items ->
              Enum.reverse([item_worry | items])
            end)
          new_source_monkey = Map.update!(monkeys_acc[monkey.nr], :inspections, &(&1 + 1))

          monkeys_acc
          |> Map.put(target, new_target_monkey)
          |> Map.put(monkey.nr, new_source_monkey)
        end
      )

    new_monkey = Map.put(new_monkeys[monkey.nr], :items, [])
    new_monkeys = Map.put(new_monkeys, monkey.nr, new_monkey)
    new_monkeys
  end
end

defmodule Day11 do
  def process_block(block) do
    block
    |> String.split("\n", trim: true)
    |> Enum.map(&String.trim/1)
  end

  def get_input(filename) do
    filename
    |> File.read!()
    |> String.split("\n\n")
    |> Enum.map(&process_block/1)
    |> Enum.map(&Monkey.from_block/1)
    |> Enum.map(fn m -> {m.nr, m} end)
    |> Enum.into(%{})
  end

  def part1() do
    monkeys =
      "data/input.txt"
      |> get_input()
    
    Enum.reduce(
      1..10_000,
      monkeys,
      fn _i, monkeys ->
        Enum.reduce(
          0..7,
          monkeys,
          fn m, monkeys ->
            Monkey.process_items(monkeys[m], monkeys)
          end
        )
      end
    )
    |> Enum.map(fn {_k, v} -> v.inspections end)
    |> Enum.sort()
    |> Enum.map(&Integer.to_string/1)
  end
end

