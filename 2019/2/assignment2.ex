defmodule Assignment2 do
  def get_input() do
    "aoc_2019_02_input.txt"
    |> File.read!()
    |> String.trim()
    |> String.split(",", trim: true)
    |> Enum.with_index()
    |> Enum.reduce(%{}, fn {value, idx}, acc -> Map.put(acc, idx, String.to_integer(value)) end)
  end

  def compute(operation, in1, in2, out, code) do
    Map.put(code, out, operation.(code[in1], code[in2]))
  end

  def process(start, code) do
    [opcode, in1, in2, out] =
      start..start+3
      |> Enum.map(fn i -> code[i] end)

    case opcode do
      1 -> {:cont, {start + 4, compute(&Kernel.+/2, in1, in2, out, code)}}
      2 -> {:cont, {start + 4, compute(&Kernel.*/2, in1, in2, out, code)}}
      99 -> {:halt, code}
    end
  end

  def part_a() do
    code =
      get_input()
      |> Map.put(1, 12)
      |> Map.put(2, 2)

    Enum.reduce_while(1..100, {0, code}, fn _, {idx, code} -> process(idx, code) end)
  end

  def part_b() do
    vals =
      for noun <- 0..99, verb <- 0..99 do
        code =
          get_input()
          |> Map.put(1, noun)
          |> Map.put(2, verb)

        out = Enum.reduce_while(1..1000, {0, code}, fn _, {idx, code} -> process(idx, code) end)
        {out[0], noun, verb}
      end
      |> Enum.reduce(%{}, fn {out, noun, verb}, acc -> Map.put(acc, out, {noun, verb}) end)

    # looking for: 19690720
    {noun, verb} = vals[19690720]
    100 * noun + verb
  end

end
