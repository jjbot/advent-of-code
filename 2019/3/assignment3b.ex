defmodule Assignment3B do
  @instruction_pattern ~r/([D|L|R|U])(\d+)/

  def parse_instruction(str) do
    [_, direction, amount] = Regex.run(@instruction_pattern, str)
    {direction, String.to_integer(amount)}
  end

  def gen_steps({"D", amount}), do: for _step <- 1..amount, do: {0, -1}
  def gen_steps({"L", amount}), do: for _step <- 1..amount, do: {-1, 0}
  def gen_steps({"R", amount}), do: for _step <- 1..amount, do: {1, 0}
  def gen_steps({"U", amount}), do: for _step <- 1..amount, do: {0, 1}

  def add_step({x, y} = _location, {dx, dy} = _increment), do: {x + dx, y + dy}

  def walk(pos, step, grid, distance) do
    new_pos = add_step(pos, step)
    new_distance = distance + 1
    {
      Map.update(grid, new_pos, new_distance, fn old -> old end),
      new_pos,
      new_distance
    }
  end

  def walk_line(steps, grid) do
    steps
    |> Enum.reduce({grid, {0, 0}, 0}, fn step, {grid, pos, distance} -> walk(pos, step, grid, distance) end)
    |> elem(0)
  end

  def get_input(file \\ "aoc_2019_03_input.txt") do
    file
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(fn line -> String.split(line, ",", trim: true) |> Enum.map(&parse_instruction/1) end)
    |> Enum.map(fn instructions -> Enum.flat_map(instructions, &gen_steps/1) end)
  end

  def run_inputs(lines) do
    [a, b] = lines
    pos_a = walk_line(a, %{})
    pos_b = walk_line(b, %{})
    {pos_a, pos_b}
  end

  def part_b() do
    {pos_a, pos_b} =
      get_input()
      |> run_inputs()
    
    locs_a = Enum.reduce(Map.keys(pos_a), %MapSet{}, fn loc, acc -> MapSet.put(acc, loc) end)
    locs_b = Enum.reduce(Map.keys(pos_b), %MapSet{}, fn loc, acc -> MapSet.put(acc, loc) end)

    MapSet.intersection(locs_a, locs_b)
    |> MapSet.to_list()
    |> Enum.reduce(%{}, fn loc, acc -> Map.put(acc, loc, pos_a[loc] + pos_b[loc]) end)
    |> Map.values()
    |> Enum.min()
  end
end
