defmodule Assignment3B do
  @moduledoc """
  Interesting edge case: self overlap of the wires, which is something I din't
  keep in mind during the first implementation.
  """
  @instruction_pattern ~r/([D|L|R|U])(\d+)/

  def parse_instruction(str) do
    [_, direction, amount] = Regex.run(@instruction_pattern, str)
    {direction, String.to_integer(amount)}
  end

  def gen_steps({"D", amount}), do: for _step <- 1..amount, do: {0, -1}
  def gen_steps({"L", amount}), do: for _step <- 1..amount, do: {-1, 0}
  def gen_steps({"R", amount}), do: for _step <- 1..amount, do: {1, 0}
  def gen_steps({"U", amount}), do: for _step <- 1..amount, do: {0, 1}

  def add_step({x, y} = _location, {dx, dy} = _increment), do: {x + dx, y + dy}

  def walk(pos, step, grid, line_nr) do
    new_pos = add_step(pos, step)
    {
      Map.update(
        grid,
        new_pos, line_nr,
        # Need to check if the overlap is with the current line
        fn 
          ^line_nr -> line_nr 
          _ -> "X"
        end
      ),
      new_pos
    }
  end

  def walk_line(steps, grid, line_nr) do
    steps
    |> Enum.reduce({grid, {0, 0}}, fn step, {grid, pos} -> walk(pos, step, grid, line_nr) end)
    |> elem(0)
  end

  def get_input() do
    "aoc_2019_03_input.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(fn line -> String.split(line, ",", trim: true) |> Enum.map(&parse_instruction/1) end)
    |> Enum.map(fn instructions -> Enum.flat_map(instructions, &gen_steps/1) end)
  end

  def run_inputs(lines) do
    lines
    |> Enum.with_index()
    |> Enum.reduce(%{}, fn {steps, line_nr}, grid -> walk_line(steps, grid, line_nr) end)
    |> Enum.filter(fn {_pos, val} -> val == "X" end)
    |> IO.inspect()
    |> Enum.map(fn {{x, y}, _val} -> abs(x) + abs(y) end)
    |> Enum.min()
  end

  def test_0() do
    ["R8,U5,L5,D3", "U7,R6,D4,L4"]
    |> run_inputs()
  end

  def test_1() do
    ["R75,D30,R83,U83,L12,D49,R71,U7,L72", "U62,R66,U55,R34,D71,R55,D58,R83"]
    |> run_inputs()
  end

  def test_2() do
    ["R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51", "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7"]
    |> run_inputs()
  end

  def part_a_with_map() do
    get_input()
    |> run_inputs()
  end

  def to_mapset(positions) do
    positions
    |> IO.inspect()
    |> Enum.reduce(%MapSet{}, fn k, acc -> MapSet.put(acc, k) end)
  end

  def gen_locations(steps) do
    Enum.reduce(steps, {[], {0, 0}},
      fn step, {locs, loc} ->
        new_loc = add_step(loc, step)
        {[new_loc | locs], new_loc} 
      end
    )
  end

  def part_a_with_mapset() do
    [a, b] = get_input()

    pos_a = a |> gen_locations() |> elem(0) |> to_mapset()
    pos_b = b |> gen_locations() |> elem(0) |> to_mapset()

    MapSet.intersection(pos_a, pos_b)
    |> MapSet.to_list()
    |> Enum.map(fn {x, y} -> abs(x) + abs(y) end)
    |> Enum.min()
  end
end
