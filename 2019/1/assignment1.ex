defmodule Assignment1 do
  def get_input() do
    "aoc_2019_01_input.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_integer(&1))
  end

  def process_nr(nr) do
    case div(nr, 3) - 2 do
      x when x >= 0 -> x
      _ -> 0
    end
  end

  def part_a() do
    get_input()
    |> Enum.map(&process_nr(&1))
    |> Enum.sum()
  end

  def get_fuel(0) do
    0
  end

  def get_fuel(mass) do
    fuel = process_nr(mass)
    fuel + get_fuel(fuel)
  end

  def part_b() do
    get_input()
    |> Enum.map(&get_fuel(&1))
    |> Enum.sum()
  end
end
