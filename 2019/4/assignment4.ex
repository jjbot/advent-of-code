defmodule Assignment4 do

  @type pair_list :: [{pos_integer(), pos_integer()}]
  @type digit_frequencies :: %{integer() => pos_integer()}

  def soft_increasing([x, y]), do: x <= y

  def equal([x, y]), do: x == y

  def part_a() do
    108457..562041
    |> Stream.map(&to_string/1)
    |> Enum.map(&String.codepoints/1)
    |> Enum.map(fn line -> Enum.map(line, &(String.to_integer(&1))) end)
    |> Enum.map(fn nr -> Enum.chunk_every(nr, 2, 1, :discard) end)
    |> Enum.filter(fn line -> Enum.all?(line, &soft_increasing/1) && Enum.any?(line, &equal/1) end)
    |> length()
  end

  def part_b() do
    # additional requirement: have at least one group with exactly 2 digits that are the same
    108457..562041
    |> Stream.map(&to_string/1)
    |> Enum.map(&String.codepoints/1)
    |> Enum.map(fn line -> Enum.map(line, &(String.to_integer(&1))) end)
    |> Enum.map(fn nr -> {nr, Enum.chunk_every(nr, 2, 1, :discard)} end)
    |> Enum.filter(fn {_nr, line} -> Enum.all?(line, &soft_increasing/1) && Enum.any?(line, &equal/1) end)
    |> Enum.map(fn {nr, _line} -> Enum.group_by(nr, & &1) end)
    |> Enum.map(fn groups -> Enum.map(groups, fn {k, v} -> {k, length(v)} end) end)
    |> Enum.filter(fn groups -> Enum.any?(groups, fn {_k, v} -> v == 2 end) end)
    |> length()
  end

  ########################
  # Cleaned implementation
  ########################

  @doc """
  Convert the input `number` to a list of digit pairs.

  Go from `123456` to `[[1, 2], [2, 3], [3, 4], [4, 5], [5, 6]]`.
  """
  @spec convert_number_a(pos_integer()) :: pair_list()
  def convert_number_a(number) do
    number
    |> to_string()
    |> String.codepoints()
    |> Enum.map(&String.to_integer/1)
    |> Enum.chunk_every(2, 1, :discard)
  end

  @spec part_a_clean(pos_integer(), pos_integer()) :: pos_integer()
  def part_a_clean(start_nr \\ 108457, end_nr \\ 562041) do
    start_nr..end_nr
    |> Stream.map(&convert_number_a/1)
    |> Enum.filter(fn pairs -> Enum.all?(pairs, &soft_increasing/1) && Enum.any?(pairs, &equal/1) end)
    |> length()
  end

  @doc """
  Convert the input `number` to a list of digits.

  Go from `123456` to `[1, 2, 3, 4, 5, 6]`.

  Unlike the part_a version of this function, it does not convert the list in digit pairs,
  as we need to digits themselves to calculate the frequency of their occurrence.
  """
  @spec convert_number_b(pos_integer()) :: [integer()]
  def convert_number_b(nr) do
    nr
    |> to_string()
    |> String.codepoints()
    |> Enum.map(&String.to_integer/1)
  end

  @doc """
  Check if the `pairs` are soft increasing and contain at least one pair where both
  values are the same digit, and use the `frequencies` to determine whether there is
  at least one digit that appears exectly twice.
  """
  @spec valid_b_sequence?({pair_list(), digit_frequencies()}) :: boolean()
  def valid_b_sequence?({pairs, frequencies}) do
    Enum.all?(pairs, &soft_increasing/1) &&
      Enum.any?(pairs, &equal/1) &&
      Enum.any?(Map.values(frequencies), & &1 == 2)
  end

  @spec part_b_clean(pos_integer(), pos_integer()) :: pos_integer()
  def part_b_clean(start_nr \\ 108457, end_nr \\ 562041) do
    start_nr..end_nr
    |> Stream.map(&convert_number_b/1)
    |> Enum.map(fn line -> {Enum.chunk_every(line, 2, 1, :discard), Enum.frequencies(line)} end)
    |> Enum.filter(&valid_b_sequence?/1)
    |> length()
  end
end
