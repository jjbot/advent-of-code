defmodule Day202301 do
  @r_part1 ~r"(\d)"
  @r_part2 ~r"(\d|one|two|three|four|five|six|seven|eight|nine)"
  @r_part2_reverse ~r"(\d|eno|owt|eerht|ruof|evif|xis|neves|thgie|enin)"

  @lookup_table %{
    "one" => "1",
    "two" => "2",
    "three" => "3",
    "four" => "4",
    "five" => "5",
    "six" => "6",
    "seven" => "7",
    "eight" => "8",
    "nine" => "9"
  }

  def part1(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&extract_line1/1)
    |> Enum.sum()
  end

  def extract_line1(line) do
    nrs = Regex.scan(@r_part1, line)

    [[first, _] | _] = nrs
    [[last, _] | _] = Enum.reverse(nrs)

    String.to_integer(first <> last)
  end

  def part2(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&extract_line2/1)
    |> Enum.sum()
  end

  def extract_line2(line) do
    IO.puts(line)
    values = Regex.scan(@r_part2, line)
    seulav = Regex.scan(@r_part2_reverse, String.reverse(line))

    [[first, _] | _] = values
    [[last, _] | _] = seulav

    String.to_integer(lookup(first) <> lookup(String.reverse(last)))
  end

  def lookup(s) do
    IO.puts(s)
    case String.length(s) do
      1 -> s
      n when n > 1 -> @lookup_table[s]
    end
  end
end
