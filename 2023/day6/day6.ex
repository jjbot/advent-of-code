defmodule D202306 do
  def part1() do
    "input.txt"
    |> read_input()
    |> Enum.map(fn {time, distance} ->
      for charge <- 1..(time - 1), charge * (time - charge) > distance, do: charge
    end)
    |> Enum.map(&length/1)
    |> Enum.product()
  end

  def part2() do
    [time, distance] = read_input2("input.txt")
    {time, distance}

    IO.puts("Time: #{time}, distance: #{distance}")

    half = div(time, 2)

    left = find_left(half, time, 0, half + 1, distance)
    check_left = (time - left) * left >= distance
    IO.puts(check_left)

    right = find_right(half, time, half - 1, time, distance)
    check_right = (time - right) * right >= distance
    IO.puts(check_right)

    right - left + 1
  end

  def find_left(current, total, left, right, distance) do
    IO.puts("c: #{current} l: #{left}, r: #{right}")
    if left == right or current == left or current == right do
      current
    else
      if current * (total - current) >= distance do
        delta = div(current - left, 2)
        find_left(current - delta, total, left, current, distance)
      else
        delta = div(right - current, 2)
        find_left(current + delta, total, current, right, distance)
      end
    end
  end

  def find_right(current, total, left, right, distance) do
    IO.puts("c: #{current} l: #{left}, r: #{right}")
    if left == right or current == left or current == right do
      current
    else
      if current * (total - current) >= distance do
        delta = div(right - current, 2)
        find_right(current + delta, total, current, right, distance)
      else
        delta = div(current - left, 2)
        find_right(current - delta, total, left, current, distance)
      end
    end
  end

  def read_input2(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&get_number/1)
  end

  def get_number(line) do
    Regex.scan(~r"(\d+)", line)
    |> Enum.map(fn [_, nr_str] -> nr_str end)
    |> Enum.reduce("", fn str, acc -> acc <> str end)
    |> String.to_integer()
  end

  def read_input(filename) do
    [time_line, distance_line] =
      filename
      |> File.read!()
      |> String.split("\n", trim: true)
      |> Enum.map(&get_numbers/1)

    Enum.zip([time_line, distance_line])
  end

  def get_numbers(line) do
    Regex.scan(~r"(\d+)", line)
    |> Enum.map(fn [_, nr_str] -> String.to_integer(nr_str) end)
  end
end
