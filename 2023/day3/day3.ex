defmodule D202303 do
  @numbers ~w"0 1 2 3 4 5 6 7 8 9"
  @symbols ~w"# $ % & * + - / = @"

  def part1() do
    locations = readfile("input.txt")

    locations
    |> find_symbol_positions()
    |> Enum.flat_map(fn loc -> find_nrs(loc, locations) end)
    |> Enum.sum()
  end

  def find_symbol_positions(locations) do
    locations
    |> Enum.filter(fn {_, v} -> v in @symbols end)
    |> Enum.map(fn {loc, _v} -> loc end)
  end

  def find_nrs(loc, locations) do
    find_nr_seeds(loc, locations)
    |> Enum.map(fn nr_list -> Enum.join(nr_list, "") |> String.to_integer() end)
    |> Enum.uniq()
  end

  def neighbours({x, y}) do
    [
      {x - 1, y - 1},
      {x, y - 1},
      {x + 1, y - 1},
      {x - 1, y},
      {x + 1, y},
      {x - 1, y + 1},
      {x, y + 1},
      {x + 1, y + 1},
    ]
  end

  def find_nr_seeds(loc, locations) do
    loc
    |> neighbours()
    |> Enum.filter(fn loc -> Map.get(locations, loc, nil) in @numbers end)
    |> Enum.map(fn loc -> grow_nr(loc, locations) end)
  end

  def grow_nr({x, y} = loc, locations) do
    v = Map.get(locations, loc)
    grow_left([v], {x - 1, y}, locations)
    |> grow_right({x + 1, y}, locations)
  end

  def grow_left(nrs, {x, y} = loc, locations) do
    v = Map.get(locations, loc, nil)
    if v in @numbers do
      grow_left([v | nrs], {x - 1, y}, locations)
    else
      nrs
    end
  end

  def grow_right(nrs, {x, y} = loc, locations) do
    v = Map.get(locations, loc, nil)
    if v in @numbers do
      grow_right(nrs ++ [v], {x + 1, y}, locations)
    else
      nrs
    end
  end

  def readfile(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.with_index(fn line, idx_y ->
      String.split(line, "", trim: true)
      |> Enum.with_index(fn c, idx_x ->
        {idx_x, idx_y, c}
      end)
    end)
    |> Enum.flat_map(fn x -> x end)
    |> Enum.reduce(%{}, fn {x, y, c}, acc -> Map.put(acc, {x, y}, c) end)
  end

  def part2() do
    locations = readfile("input.txt")

    locations
    |> find_gear_positions()
    |> Enum.map(fn loc -> find_nrs(loc, locations) end)
    |> Enum.filter(&length(&1) == 2)
    |> Enum.map(fn [first, second] -> first * second end)
    |> Enum.sum()
  end

  def find_gear_positions(locations) do
    locations
    |> Enum.filter(fn {_, v} -> v == "*" end)
    |> Enum.map(fn {loc, _v} -> loc end)
  end
end
