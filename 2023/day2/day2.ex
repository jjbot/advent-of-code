defmodule D202302 do
  @line_re ~r"Game (\d+):\s(.*)"
  @color_re ~r"(\d+)\s(\w+)"
  # 12 red cubes, 13 green cubes, and 14 blue cubes
  @maxes %{
    "red" => 12,
    "green" => 13,
    "blue" => 14
  }

  def part1() do
    readfile("input.txt")
    |> Enum.filter(fn {_game_id, rounds} ->
      Enum.all?(rounds, &check_round/1)
    end)
    |> Enum.map(fn {game_id, _} -> game_id end)
    |> Enum.sum()
  end

  def process_rest(rest) do
    String.split(rest, ";")
    |> Enum.map(fn s ->
      Regex.scan(@color_re, s)
      |> Enum.reduce(
        %{},
        fn [_, amount, color], acc ->
          Map.put(acc, color, String.to_integer(amount))
        end
      )
    end)
  end

  def part2() do
    readfile("input.txt")
    |> Enum.map(fn {_game_id, rounds} -> process_rounds(rounds) end)
    |> Enum.sum()
  end

  def process_rounds(rounds) do
    rounds
    |> Enum.reduce(
      %{"red" => [], "green" => [], "blue" => []},
      fn round, acc ->
        Enum.reduce(round, acc, fn {color, count}, acc ->
          update_in(acc, [color], fn l -> [count | l] end)
        end)
      end
    )
    |> Enum.map(fn {_k, l} -> Enum.max(l, &>=/2, fn -> 0 end) end)
    |> Enum.product()
  end

  def readfile(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(fn
      line ->
        [_, nr, rest] = Regex.run(@line_re, line)
        {String.to_integer(nr), process_rest(rest)}
    end)
  end

  def check_round(round) do
    Enum.all?(round, fn {color, count} ->
      count <= @maxes[color]
    end)
  end
end
