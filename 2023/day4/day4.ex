defmodule D202304 do
  @line_re ~r/^Card\s+(\d+):([\w|\s]+)\|([\w|\s]+)/
  @nr_re ~r"(\d+)"

  def part1() do
    readfile("input.txt")
    |> Enum.map(fn {_, winning, gotten} ->
      MapSet.intersection(winning, gotten)
      |> MapSet.size()
      |> calculate_points()
    end)
    |> Enum.sum()
  end

  def calculate_points(0), do: 0
  def calculate_points(1), do: 1
  def calculate_points(n), do: :math.pow(2, n - 1)

  def part2() do
    cards = readfile("input.txt")
    card_nrs = Enum.map(cards, fn {nr, _, _} -> nr end)
    counts = Enum.reduce(cards, %{}, fn {nr, _, _}, acc -> Map.put(acc, nr, 1) end)

    repeats =
      Enum.reduce(cards, %{}, fn {nr, winning, gotten}, acc ->
        Map.put(acc, nr, MapSet.size(MapSet.intersection(winning, gotten)))
      end)

    card_nrs
    |> Enum.reduce(counts, fn nr, acc -> update_counts(nr, acc, repeats) end)
    |> Map.values()
    |> Enum.sum()
  end

  def update_counts(card_nr, counts, repeats) do
    if Map.get(repeats, card_nr, 0) > 0 do
      updates = for nr <- (card_nr + 1)..(card_nr + repeats[card_nr]), do: {nr, counts[card_nr]}

      Enum.reduce(
        updates,
        counts,
        fn {nr, amount}, acc ->
          Map.update!(acc, nr, fn old -> old + amount end) end
      )
    else
      counts
    end
  end

  def readfile(filename) do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(fn line ->
      [_, card_id, winning, gotten] = Regex.run(@line_re, line)

      {
        String.to_integer(card_id),
        MapSet.new(find_nrs(winning)),
        MapSet.new(find_nrs(gotten))
      }
    end)
  end

  def find_nrs(s) do
    Regex.scan(@nr_re, s, capture: :all_but_first)
    |> Enum.map(fn [x] -> String.to_integer(x) end)
  end
end
