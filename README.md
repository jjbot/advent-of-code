# Advent of Code Solutions

This repository contains my solutions to the [Advent of Code](https://adventofcode.com/)
problems. Solutions are provided in [Elixir](https://elixir-lang.org/).

## Solved problems
|  | 2015 | 2016 | 2017 | 2018 | 2019 | 2020 | 2021 | 2022 | 2023 |
|--|------|------|------|------|------|------|------|------|------|
|01|      |      |:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark:/:white_check_mark:|:white_check_mark:/:white_check_mark:
|02|      |      |:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark:/:white_check_mark:|:white_check_mark:/:white_check_mark:
|03|      |      |:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark:/:white_check_mark:|:white_check_mark:/:white_check_mark:
|04|      |      |:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark:/:white_check_mark:|:white_check_mark:/:white_check_mark:
|05|      |      |      |:white_check_mark: / :white_check_mark:|      |:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark:/:white_check_mark:|
|06|      |      |:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|      |:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark:/:white_check_mark:|:white_check_mark: / :white_check_mark:|
|07|      |      |      |:white_check_mark: / :white_check_mark:|      |:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark:/:white_check_mark:|
|08|      |      |      |:white_check_mark: / :white_check_mark:|      |:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark:/:white_check_mark:|
|09|      |      |      |:white_check_mark: / :white_check_mark:|      |:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark:/:white_check_mark:|
|10|      |      |      |:white_check_mark: / :white_check_mark:|      |:white_check_mark: / :black_square_button:|:white_check_mark: / :white_check_mark:|:white_check_mark:/:white_check_mark:|
|11|      |      |      |:white_check_mark: / :white_check_mark:|      |:white_check_mark: / :white_check_mark:|:white_check_mark: / :white_check_mark:|:white_check_mark:/:white_check_mark:|
|12|      |      |      |      |      |:white_check_mark: / :white_check_mark:||:white_check_mark:/:white_check_mark:|
|13|      |      |      |:white_check_mark: / :white_check_mark:|      |:white_check_mark: / :white_check_mark:||:white_check_mark:/:white_check_mark:|
|14|      |      |      |      |      |:white_check_mark: / :white_check_mark:||:white_check_mark:/:white_check_mark:|
|14|      |      |      |      |      |:white_check_mark: / :white_check_mark:||:white_check_mark:|
|16|      |      |:white_check_mark: / :white_check_mark:|      |      |:white_check_mark: / :white_check_mark:||
|17|      |      |      |      |      |:white_check_mark: / :white_check_mark:||
|18|      |      |      |      |      |:white_check_mark: / :white_check_mark:||
|19|      |      |      |      |      |     ||
|20|      |      |      |      |      |:white_check_mark: / :white_check_mark:||
|21|      |      |      |      |      |:white_check_mark: / :white_check_mark:||
|22|      |      |      |      |      |:white_check_mark: / :white_check_mark:||
|23|      |      |      |      |      |:white_check_mark: / :white_check_mark:||
|24|      |      |      |      |      |:white_check_mark: / :white_check_mark:||
|25|      |      |      |      |      |     ||
