defmodule Assignment4 do
  # Required fields, for part a
  @required ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

  # Regex patterns for part b
  @year_pattern ~r"^(\d{4})$"
  @height_pattern ~r"^(\d+)(\w{2})$"
  @hair_colour_pattern ~r"^#[0-9a-z]{6}$"
  @passport_pattern ~r"^[0-9]{9}$"

  @doc """
  Check if all the required fields are present in the provided map.
  """
  @spec has_required?(%{String.t() => String.t()}) :: boolean()
  def has_required?(pp) do
    @required
    |> Enum.map(fn field -> Map.has_key?(pp, field) end)
    |> Enum.all?()
  end

  @doc """
  Read data from file and transform it into a list of maps. Keys are the fields
  required in the passports, the values are the associated values.
  """
  @spec get_input() :: [%{String.t() => String.t()}]
  def get_input() do
    "aoc_2020_4_input.txt"
    |> File.read!()
    |> String.split("\n")
    |> Stream.chunk_by(fn line -> line == "" end)
    |> Stream.reject(fn chunk -> List.first(chunk) == "" end)
    |> Stream.map(fn chunk -> Enum.join(chunk, " ") end )
    |> Stream.map(&String.split/1)
    |> Stream.map(
      fn info ->
        Enum.map_reduce(info, %{}, fn field, acc ->
          [key, value] = String.split(field, ":")
          {{key, value}, Map.put(acc, key, value)}
        end) 
      end )
    |> Stream.map(fn pp -> elem(pp, 1) end)
  end

  @spec part_a() :: integer()
  def part_a() do
    get_input()
    |> Stream.filter(fn pp -> has_required?(pp) end)
    |> Enum.count()
  end

  @doc """
  Create a function that checks if the provided string represents a year between
  start_year and end_year (inclusive on both).
  """
  @spec get_year_check(integer(), integer()) :: function()
  def get_year_check(start_year, end_year) do
    fn input ->
      case Regex.run(@year_pattern, input) do
        [_, year_str] ->
          year = String.to_integer(year_str)
          year >= start_year and year <= end_year
        _ -> false
      end
    end
  end

  @spec check_height(String.t()) :: boolean()
  def check_height(input) do
    case Regex.run(@height_pattern, input) do
      [_, height_str, "cm"] ->
        case String.to_integer(height_str) do
          height when height >= 150 and height <= 193 -> true
          _ -> false
        end

      [_, height_str, "in"] ->
        case String.to_integer(height_str) do
          height when height >= 59 and height <= 76 -> true
          _ -> false
        end

      _ -> false
    end
  end

  @spec check_hair_colour(String.t()) :: boolean()
  def check_hair_colour(input) do
    Regex.match?(@hair_colour_pattern, input)
  end

  @spec check_eye_colour(String.t()) :: boolean()
  def check_eye_colour(input) do
    input in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]
  end

  @spec check_passport_id(String.t()) :: boolean()
  def check_passport_id(input) do
    Regex.match?(@passport_pattern, input)
  end

  @spec part_b() :: integer()
  def part_b() do

    # Create a map that holds references to functions that allow the fields (in
    # this map the keys) to be used to find the function that checks the associated
    # value.
    functions = %{
      "byr" => get_year_check(1920, 2002),
      "iyr" => get_year_check(2010, 2020),
      "eyr" => get_year_check(2020, 2030),
      "hgt" => &check_height/1,
      "hcl" => &check_hair_colour/1,
      "ecl" => &check_eye_colour/1,
      "pid" => &check_passport_id/1,
      "cid" => fn _ -> true end
    }

    get_input()
    |> Stream.filter(fn pp -> has_required?(pp) end)
    |> Stream.map(
      fn fields -> Enum.map(fields, fn {k, v} -> functions[k].(v) end)
      end )
    |> Stream.filter(&Enum.all?/1)
    |> Enum.count()
  end
end
