defmodule Assignment9 do
  def check_chunk([to_match | numbers]) do
    sum_match = for x <- numbers, y <- numbers, x + y == to_match, do: true
    if Enum.any?(sum_match) do
      []
    else
      [to_match]
    end
  end

  def part_a() do
    "aoc_2020_9_input.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_integer/1)
    |> Enum.chunk_every(26, 1)
    |> Enum.map(&Enum.reverse/1)
    |> Enum.flat_map(&check_chunk/1)
  end

  def find_sum(list, target) do
    {summed, l} = Enum.reduce_while(list, {0, []}, fn x, {sum, l} ->
      s = x + sum
      if s >= target do
        {:halt, {s, l}}
      else
        {:cont, {s, [x | l]}}
      end
    end)

    if summed == target do
      l
    else
      [_ | tail] = list
      find_sum(tail, target)
    end
  end

  def part_b() do
    target = 41682220

    "aoc_2020_9_input.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_integer/1)
    |> find_sum(target)
    |> (fn l -> Enum.min(l) + Enum.max(l) end).()
  end

end
