defmodule Food do
  defstruct ingredients: [], allergens: []

  def has_ingredient(food, ingredient), do: ingredient in food.ingredients

  def has_allergen(food, allergen), do: allergen in food.allergens
end


defmodule Assignment21 do
  @line_pattern ~r/(.*)\s\(contains\s(.*)\)/

  def process_line(line) do
    [ingredients, allergens] = Regex.run(@line_pattern, line, capture: :all_but_first)
    %Food{
      ingredients: MapSet.new(String.split(ingredients, " ", trim: true)),
      allergens: String.split(allergens, ", ", trim: true)
    }
  end

  def find_ingredients(allergen, foods) do
    [food | foods_with_allergen] = Enum.filter(foods, &(Food.has_allergen(&1, allergen)))
    ingredients = Enum.reduce(foods_with_allergen, food.ingredients, fn food, ingredients -> MapSet.intersection(food.ingredients, ingredients) end)
    {allergen, ingredients}
  end

  def get_input(filename \\ "aoc_2020_21_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&process_line/1)
  end

  def remove_mapped(mapped, options) do
    found = MapSet.new(Map.values(mapped))
    Enum.map(options, fn {allergen, ingredients} -> {allergen, MapSet.difference(ingredients, found)} end)
  end

  def make_unique([], mapped) do
    mapped
  end

  def make_unique(options, mapped) do
    {to_process, mapped} =
      Enum.reduce(
        options,
        {[], mapped},
        fn {allergen, ingredients}, {to_process, mapped} ->
          if MapSet.size(ingredients) == 1 do
            [ingredient | _] = MapSet.to_list(ingredients)
            {to_process, Map.put(mapped, allergen, ingredient)}
          else
            {[{allergen, ingredients} | to_process], mapped}
          end
        end
      )
    
    options = remove_mapped(mapped, to_process)
    make_unique(options, mapped)
  end

  def part1() do
    foods = get_input()

    ingredients_with_allergens =
      foods
      |> Enum.flat_map(fn food -> food.allergens end)
      |> MapSet.new()
      |> Enum.map(&(find_ingredients(&1, foods)))
      |> make_unique(%{})
      |> Map.values()
      |> MapSet.new()

    all_ingredients =
      foods
      |> Enum.flat_map(fn food -> food.ingredients end)
      |> MapSet.new()

    MapSet.difference(all_ingredients, ingredients_with_allergens)
    |> MapSet.to_list()
    |> Enum.map(fn ingredient -> Enum.filter(foods, fn food -> Food.has_ingredient(food, ingredient) end) end)
    |> Enum.map(&length/1)
    |> Enum.sum()
  end

  def part2() do
    foods = get_input()
    
    foods
    |> Enum.flat_map(fn food -> food.allergens end)
    |> MapSet.new()
    |> Enum.map(&(find_ingredients(&1, foods)))
    |> make_unique(%{})
    |> Enum.sort()
    |> Enum.map(fn {_, ingredient} -> ingredient end)
    |> Enum.join(",")
  end
end
