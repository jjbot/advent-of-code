defmodule Assignment5 do

  def get_seat_pos(str) do
    {row_str, col_str} = String.split_at(str, 7)

    row =
      row_str
      |> String.split("")
      |> Enum.reject(fn line -> line == "" end)
      |> Enum.reduce({0, 127}, fn letter, {min_row, max_row} ->
          half_way = ceil( (max_row - min_row) / 2)
          case letter do
            "B" -> {min_row + half_way, max_row}
            "F" -> {min_row, max_row - half_way}
          end
        end)
      |> elem(1)


    col =
      col_str
      |> String.split("")
      |> Enum.reject(fn line -> line == "" end)
      |> Enum.reduce({0, 7}, fn letter, {min_row, max_row} ->
          half_way = ceil( (max_row - min_row) / 2)
          case letter do
            "R" -> {min_row + half_way, max_row}
            "L" -> {min_row, max_row - half_way}
          end
        end)
      |> elem(1)

    {row, col}
  end

  def get_taken_seat_ids() do
    "aoc_2020_5_input.txt"
    |> File.read!()
    |> String.split("\n")
    |> Enum.reject(&(&1 == ""))
    |> Enum.map(fn str -> {str, get_seat_pos(str)} end)
    |> Enum.map(fn {str, {row, col}} -> {str, {row, col}, (row * 8) + col} end)
  end

  def part_a() do
    get_taken_seat_ids()
    |> Enum.max_by(fn x -> elem(x, 2) end)
  end

  def part_b() do
    taken_seats =
      get_taken_seat_ids()
      |> Enum.map(&(elem(&1, 2)))

    0..1024
    |> Enum.reject(&(&1 in taken_seats))
    |> Enum.reject(&(&1 + 1 not in taken_seats))
    |> Enum.reject(&(&1 - 1 not in taken_seats))
  end
end
