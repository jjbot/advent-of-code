defmodule Assignment14 do
  @pattern ~r"(.*)\s=\s(.*)"
  @mem_pattern ~r"mem\[(\d+)\]"

  def apply_mask(mask, nr) do
    one_mask = mask |> String.replace("X", "0") |> String.to_integer(2)
    zero_mask = mask |> String.replace("X", "1") |> String.to_integer(2)

    nr 
    |> Bitwise.band(zero_mask)
    |> Bitwise.bor(one_mask)
  end

  def parse_line(line) do
    [_, left, right] = Regex.run(@pattern, line)
    {left, right}
  end

  def part_a() do
    "aoc_2020_14_input.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_line/1)
    |> Enum.reduce({"", %{}}, fn {left, right}, {mask, mem} ->
      if left == "mask" do
        {right, mem}
      else
        {mask, Map.put(mem, left, apply_mask(mask, String.to_integer(right)))}
      end
    end)
    |> elem(1)
    |> Enum.reduce(0, fn {_loc, val}, acc -> acc + val end)
  end

  def generate_all([], prefix) do
    [prefix]
  end

  def generate_all([current | tail], prefix) do
    generate_all(tail, prefix <> "0" <> current) ++ generate_all(tail, prefix <> "1" <> current)
  end

  def generate_all(s) do
    [prefix | tail] = String.split(s, "X")
    generate_all(tail, prefix)
  end

  def generate_mem_addresses(address, mask) do
    address
    |> Integer.to_string(2)
    |> String.pad_leading(36, "0")
    |> String.split("", trim: true)
    |> Enum.zip(String.split(mask, "", trim: true))
    |> Enum.reduce(
      [],
      fn {a, m}, acc ->
        case {a, m} do
          {_, "X"} -> ["X" | acc]
          {a, "0"} -> [a | acc]
          {_, "1"} -> ["1" | acc]
        end
      end)
    |> Enum.reverse()
    |> Enum.join()
    |> generate_all()

  end

  def part_b() do
    "aoc_2020_14_input.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_line/1)
    |> Enum.reduce({"", %{}}, fn {left, right}, {mask, mem} ->
      if left == "mask" do
        {right, mem}
      else
        [_, mem_str] = Regex.run(@mem_pattern, left)
        mem_loc = String.to_integer(mem_str)
        value = String.to_integer(right)
        {mask, Enum.reduce(generate_mem_addresses(mem_loc, mask), mem, fn l, acc -> Map.put(acc, l, value) end)}
      end
    end)
    |> elem(1)
    |> Enum.reduce(0, fn {loc, val}, acc -> acc + val end)
  end
end
