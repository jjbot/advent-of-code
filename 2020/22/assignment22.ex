defmodule Assignment22 do
  def process_player_lines(input) do
    input
    |> String.split("\n", trim: true)
    |> Enum.drop(1)
    |> Enum.map(&String.to_integer/1)
    |> :queue.from_list()
  end

  def play(q1, q2) do
    case {:queue.is_empty(q1), :queue.is_empty(q2)} do
      {true, _} ->
        {:player2, q2}

      {_, true} ->
        {:player1, q1}

      _ ->
        {{:value, v1}, q1} = :queue.out(q1)
        {{:value, v2}, q2} = :queue.out(q2)
        if v1 > v2 do
          q1 = :queue.in(v1, q1)
          q1 = :queue.in(v2, q1)
          play(q1, q2)
        else
          q2 = :queue.in(v2, q2)
          q2 = :queue.in(v1, q2)
          play(q1, q2)
        end
    end
  end

  def get_input(filename \\ "aoc_2020_22_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n\n", trim: true)
    |> Enum.map(&process_player_lines/1)
  end

  def part1() do
    [q1, q2] = get_input()
    {_winner, q} = play(q1, q2)
    :queue.to_list(q)
    |> Enum.reverse()
    |> Enum.with_index(1)
    |> Enum.reduce(0, fn {val, idx}, acc -> acc + (val * idx) end)
  end

  #################################################################
  # part 2
  #################################################################

  def play_recursive(q1, q2, memory) do
    if MapSet.member?(memory, {q1, q2}) do
      # IO.puts("found already played round")
      {:player1, q1}
    else
      case {:queue.is_empty(q1), :queue.is_empty(q2)} do
        {true, _} ->
          {:player2, q2}

        {_, true} ->
          {:player1, q1}

        _ ->
          {{:value, v1}, q1_new} = :queue.out(q1)
          {{:value, v2}, q2_new} = :queue.out(q2)

          if :queue.len(q1_new) >= v1 && :queue.len(q2_new) >= v2 do
            {q1_selected, _} = :queue.split(v1, q1_new)
            {q2_selected, _} = :queue.split(v2, q2_new)

            case play_recursive(q1_selected, q2_selected, MapSet.new()) do
              {:player1, _q1_won} ->
                q1_new = :queue.in(v1, q1_new)
                q1_new = :queue.in(v2, q1_new)
                play_recursive(q1_new, q2_new, MapSet.put(memory, {q1, q2}))

              {:player2, _q2_won} ->
                q2_new = :queue.in(v2, q2_new)
                q2_new = :queue.in(v1, q2_new)
                play_recursive(q1_new, q2_new, MapSet.put(memory, {q1, q2}))
            end
          else
            if v1 > v2 do
              q1_new = :queue.in(v1, q1_new)
              q1_new = :queue.in(v2, q1_new)
              play_recursive(q1_new, q2_new, MapSet.put(memory, {q1, q2}))
            else
              q2_new = :queue.in(v2, q2_new)
              q2_new = :queue.in(v1, q2_new)
              play_recursive(q1_new, q2_new, MapSet.put(memory, {q1, q2}))
            end
          end
      end
    end
  end

  def part2() do
    # not it: 8705, 32078

    [q1, q2] = get_input()
    memory = MapSet.new()
    # play_recursive(q1, q2, memory)
    {_winner, q} = play_recursive(q1, q2, memory)

    :queue.to_list(q)
    |> Enum.reverse()
    |> Enum.with_index(1)
    |> Enum.reduce(0, fn {val, idx}, acc -> acc + (val * idx) end)
  end
end
