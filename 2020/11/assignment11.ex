defmodule Assignment11 do
  @directions [{0, 1}, {1, 0}, {0, -1}, {-1, 0}, {-1, -1}, {1, -1}, {-1, 1}, {1, 1}]

  @doc """
  Get the eight neighbouring cell coordinates for the provided location.
  """
  def get_neighbours({x, y}) do
    for dx <- [-1, 0, 1], dy <- [-1, 0, 1], !(dx == 0 and dy == 0), do: {x + dx, y + dy}
  end

  @doc """
  Based on the status of the neighbouring cells and the cell under consideration,
  determine the status of the provided location in the next iteration.
  """
  def get_next_seat_status(pos, seats) do
    occupied_count =
      Enum.reduce(
        get_neighbours(pos),
        0,
        fn npos, acc -> if Map.get(seats, npos, ".") == "#", do: acc + 1, else: acc end
      )

    case {seats[pos], occupied_count} do
      {".", _} -> "."
      {"L", count} when count == 0 -> "#"
      {"#", count} when count >= 4 -> "L"
      _ -> seats[pos]
    end
  end

  @doc """
  Iterate until a stable solution materialises.
  """
  def stabalise_a_seating(seats) do
    new_seats =
      Enum.reduce(
        seats,
        %{},
        fn {pos, _s}, acc -> Map.put(acc, pos, get_next_seat_status(pos, seats)) end
      )
    
    if new_seats == seats, do: seats, else: stabalise_a_seating(new_seats)
  end

  @doc """
  Get input for the assignment.

  We convert the input lines into a dictionary that holds the x and y coordinates
  as the keys and the current status as the value.

  Value options:
  "." -> empty (no seat)
  "#" -> occupied
  "L" -> empty

  It has 94 lines and each line has 97 columns.
  """
  def get_input() do
    "aoc_2020_11_input.txt"
    # "test.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.split(&1, "", trim: true))
    |> Enum.with_index()
    |> Enum.flat_map(
      fn {l, x} -> 
        Enum.with_index(l) |> Enum.map(fn {s, y} -> {x, y, s} end)
      end)
    |> Enum.reduce(%{}, fn {x, y, s}, acc -> Map.put(acc, {x, y}, s) end)
  end

  def part_a() do
    get_input()
    |> stabalise_a_seating()
    |> Map.values()
    |> Enum.frequencies()
    |> (fn m -> m["#"] end).()
  end

  # Part B

  @doc """
  Get the cell locations of the line of sight from the current position. Direction
  is determined by the steps argument. Possible values for these steps are stored
  in the @directions field of the module.

  !IMPORTANT: remove yourself from the list (guess what I spent 30 minutes of
  debugging time on...)
  """
  def get_line_of_sight(pos, {dx, dy}) do
    pos
    |> Stream.iterate(fn {x, y} -> {x + dx, y + dy} end)
    |> Enum.take_while(fn {x, y} -> x >= 0 and x < 94 and y >= 0 and y < 97 end)
    |> Enum.reject(&(&1 == pos))
  end

  @doc """
  Get the status for the first seat along the line of sight. Converts it directly
  to 0 (no seat or empty) or 1 (seat occupied).
  """
  def get_first_sighted_seat_status(pos, step, seats) do
    pos
    |> get_line_of_sight(step)
    |> Enum.reduce_while(
      ".",
      fn loc, acc ->
        case Map.get(seats, loc, ".") do
          "." -> {:cont, acc}
          x -> {:halt, x}
        end
      end)
    |> (fn s -> if s == "#", do: 1, else: 0 end).()
  end

  @doc """
  Get the next status for the provided position.
  """
  def get_next_seat_status_b(pos, seats) do
    occupied_count =
      Enum.reduce(
        @directions,
        0,
        fn direction, acc ->
          acc + get_first_sighted_seat_status(pos, direction, seats)
        end
      )

    case {seats[pos], occupied_count} do
      {".", _} -> "."
      {"L", count} when count == 0 -> "#"
      {"#", count} when count >= 5 -> "L"
      _ -> seats[pos]
    end
  end

  @doc """
  Helper function to create a new iteration of the seat layout. Helps with
  debugging.
  """
  def get_new_b_seating(seats) do
    Enum.reduce(
      seats,
      %{},
      fn {pos, _s}, acc -> Map.put(acc, pos, get_next_seat_status_b(pos, seats)) end
    )
  end

  @doc """
  Create next version of seating and check if the seating has stabalised.
  """
  def stabalise_b_seating(seats) do
    new_seats = get_new_b_seating(seats)
    if new_seats == seats, do: seats, else: stabalise_b_seating(new_seats)
  end

  def part_b() do
    get_input()
    |> stabalise_b_seating()
    |> Map.values()
    |> Enum.frequencies()
    |> (fn m -> m["#"] end).()
  end
end
