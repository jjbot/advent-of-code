defmodule Assignment7 do
  @base_pattern ~r"([\w | \s]+)\sbags\scontain\s([, | \w | \s]+)\."
  @contained_pattern ~r"(\d+)\s([a-z]+\s[a-z]+)\s"

  def parse_contained(contained) do
    if String.contains?(contained, "no other bags") do
      []
    else
      Regex.scan(@contained_pattern, contained)
      |> Enum.map(fn [_h | relevant] ->
        [count, bag] = relevant
        {String.to_integer(count), bag}
      end)
    end
  end

  @doc """
  Parse out one line of the input file.

  Example:
  mirrored tomato bags contain 3 faded maroon bags, 3 dark green bags.
  """
  def parse_line(line) do
    [_, container, contained] = Regex.run(@base_pattern, line)
    contained = parse_contained(contained)

    {container, contained}
  end

  def find_containers(mapping, bag) do
    case Map.get(mapping, bag) do
      bags when is_list(bags) ->
        other =
          Enum.map(bags, fn new_bag -> find_containers(mapping, new_bag) end)
          |> Enum.flat_map(&(&1))
        bags ++ other
      nil -> []
    end
  end

  def get_input() do
    "aoc_2020_7_input.txt"
    |> File.read!()
    |> String.split("\n")
    |> Stream.reject(&(&1 == ""))
    |> Stream.map(&parse_line/1)
  end

  def part_a() do
    mapping =
      get_input()
      # Stream now still contains the amount of contained bags, we don't need
      # that in part_a, lets remove them.
      # So, we go from {container, [{count_0, contained_0}, {count_1, contained_1}, ...]}
      # to {container, [contained_0, contained_1, ...]}
      |> Stream.map(
        fn {container, contained} ->
          {container, Enum.map(contained, &elem(&1, 1) )}
        end)
      # Reverse the mapping, and join into one big list.
      # [{contained_0, container}, {contained_1, container}, ...]
      |> Stream.flat_map(
        fn {container, contained} ->
          Enum.map(contained, &({container, &1}))
        end)
      # Create a dictionary from the list, creating this structure
      # %{contained_0 => [container_0, container_1, ...], contained_1 => [container_3, container_4, ...}
      # We have now reversed the input: we started with containers and the bags
      # they contain, we now have contained pointing to the bags that can contain them.
      |> Enum.reduce(
        %{},
        fn {container, contained}, acc -> 
          Map.update(acc, contained, [container], &([container | &1])) 
        end) 

    find_containers(mapping, "shiny gold") |> MapSet.new() |> MapSet.size()
  end

  @doc """
  Count the number of bags a bag must contain.
  """
  def count_bags(mapping, bag) do
    IO.puts(inspect mapping[bag])
    case Map.get(mapping, bag) do
      nested_bags when is_list(nested_bags) ->
        Enum.reduce(nested_bags, 0, fn {count, nested_bag}, acc -> acc + count + (count * count_bags(mapping, nested_bag)) end )
      _ ->
        0
    end
  end

  def part_b() do
    mapping =
      "aoc_2020_7_input.txt"
      |> File.read!()
      |> String.split("\n")
      |> Stream.reject(&(&1 == ""))
      |> Stream.map(&parse_line/1)
      |> Stream.reject(&(elem(&1, 1) == []))
      |> Enum.reduce(%{}, fn {bag, bags}, acc -> Map.put(acc, bag, bags) end)

    # find_containers(mapping, "shiny gold") |> MapSet.new() |> MapSet.size()
    count_bags(mapping, "shiny gold")
  end

  # 20189
end
