defmodule Bag do
  use GenServer

  defstruct contained: %{}, name: ""

  def init({name, contained}) do
    {:ok, %Bag{name: name, contained: contained}}
  end

  def start_link({name, contained}) do
    GenServer.start_link(__MODULE__, {name, contained}, name: {:global, name})
  end

end
