defmodule Bags do
  @moduledoc """
  Documentation for `Bags`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Bags.hello()
      :world

  """
  def hello do
    :world
  end
end
