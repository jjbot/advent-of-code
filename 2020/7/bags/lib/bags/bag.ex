defmodule Bag do
  use GenServer

  defstruct name: "", contains: []

  @impl GenServer
  def init({name, contains}) do
    {:ok, %Bag{name: name, contains: contains}}
  end

  def start_link({name, contains}) do
    GenServer.start_link(__MODULE__, {name, contains}, name: {:global, name})
  end

  @doc """
  Checks if a `bag` contains another bag.
  """
  def contains(bag, name) do
    GenServer.call({:global, bag}, {:contains, name})
  end

  @doc """
  Get the total amount of bags in the bag stack.

  This includes the original container bag. To get the answer to part 2, remove
  one bag from the total.
  """
  def count_bags(name) do
    GenServer.call({:global, name}, :count_bags)
  end

  def get_contains(name) do
    GenServer.call({:global, name}, :get_contains)
  end

  @impl GenServer
  def handle_call({:contains, name}, _from, state) do
    current = Enum.reduce(
      state.contains,
      false,
      fn {_count, contained}, acc ->
        acc or name == contained or contains(contained, name)
      end)
    {:reply, current, state}
  end

  def handle_call(:count_bags, _from, state) do
    count =
      state.contains
      |> Enum.reduce(1, fn {count, name}, acc -> acc + count * count_bags(name) end)
    {:reply, count, state}
  end

  def handle_call(:get_contains, _from, state) do
    {:reply, state.contains, state}
  end
end
