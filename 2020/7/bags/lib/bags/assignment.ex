defmodule Assignment do
  @base_pattern ~r"([\w | \s]+)\sbags\scontain\s([, | \w | \s]+)\."
  @contained_pattern ~r"(\d+)\s([a-z]+\s[a-z]+)\s"

  def parse_contained(contained) do
    if String.contains?(contained, "no other bags") do
      []
    else
      Regex.scan(@contained_pattern, contained)
      |> Enum.map(fn [_h | relevant] ->
        [count, bag] = relevant
        {String.to_integer(count), bag}
      end)
    end
  end

  @doc """
  Parse out one line of the input file.

  Example:
  mirrored tomato bags contain 3 faded maroon bags, 3 dark green bags.
  """
  def parse_line(line) do
    [_, container, contained] = Regex.run(@base_pattern, line)
    contained = parse_contained(contained)

    {container, contained}
  end

  def start_bag(bag_spec) do
    DynamicSupervisor.start_child(BagsSupervisor, {Bag, bag_spec})
  end

  def get_input() do
    "data/aoc_2020_7_input.txt"
    |> File.read!()
    |> String.split("\n")
    |> Stream.reject(&(&1 == ""))
    |> Enum.map(&parse_line/1)
  end

  def load_bags() do
    get_input()
    |> Enum.map(&start_bag/1)
  end
end
