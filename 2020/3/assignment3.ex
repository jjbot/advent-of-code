defmodule Assignment3 do
  def get_input() do
    "input.txt"
    |> File.read!()
    |> String.split("\n")
    |> Enum.filter(&(&1 != ""))
  end

  def get_tree_count(cur, pos, tree_count) do
    if String.at(cur, pos) == "#" do
      tree_count + 1
    else
      tree_count
    end
  end

  def skip([], _) do
    []
  end

  def skip(hill, 0) do
    hill
  end

  def skip([_cur | slope], skip_count) do
    skip(slope, skip_count - 1)
  end
  
  def walk([], _pos, _right, _skip_count, tree_count) do
    tree_count
  end

  def walk([cur | slope], pos, right, skip_count, tree_count) do
    tree_count = get_tree_count(cur, pos, tree_count)
    hill = skip(slope, skip_count)
    walk(hill, rem(pos + right, 31), right, skip_count, tree_count)
  end

  def run1() do
    input = get_input()
    walk(input, 0, 3, 0, 0)
  end

  def run2() do
    input = get_input()
    walk(input, 0, 1, 0, 0) * walk(input, 0, 3, 0, 0) * walk(input, 0, 5, 0, 0) * walk(input, 0, 7, 0, 0) * walk(input, 0, 1, 1, 0)
  end

  def map_walk(input, dx, dy) do
    input
    |> Enum.map_every(dy, &(&1))
    |> Enum.map_reduce(
      {[], 0},
      fn line, {trees, loc} -> {nil, {[String.at(line, rem(loc, 31)) | trees], loc + dx}} end
    )
    |> (fn {_, {x, _}} -> x end).()
    # |> elem(1) |> elem(0)
    |> Enum.count(fn x -> x == "#" end)
  end

  def run2b() do
    input = get_input()
    map_walk(input, 3, 1)
  end
end
