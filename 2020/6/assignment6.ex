defmodule Assignment6 do
  def part_a() do
    "aoc_2020_6_input.txt"
    |> File.read!()
    |> String.split("\n")
    |> Enum.chunk_by(&(&1 == ""))
    |> Enum.reject(fn [h | _] -> h == "" end)
    |> Enum.map(&Enum.join(&1, ""))
    |> Enum.map(&String.split(&1, "") |> Enum.reject(fn c -> c == "" end) |> MapSet.new() |> MapSet.size())
    |> Enum.reduce(0, fn x, acc -> acc + x end)
  end

  def part_b() do
    "aoc_2020_6_input.txt"
    |> File.read!()
    |> String.split("\n")
    |> Enum.chunk_by(&(&1 == ""))
    |> Enum.reject(fn [h | _] -> h == "" end)
    |> Enum.map(fn forms -> {length(forms), Enum.join(forms, "")} end)
    |> Enum.map(fn {form_count, answers} ->
      {
        form_count,
        String.split(answers, "")
        |> Enum.reject(&(&1 == ""))
        |> Enum.reduce(%{}, fn letter, acc -> Map.update(acc, letter, 1, &(&1 + 1)) end)
      }
      end)
    |> Enum.map(fn {form_count, counts} -> Enum.filter(counts, fn {_k, v} -> v == form_count end) end)
    |> Enum.map(&(length(&1)))
    |> Enum.reduce(0, fn x, acc -> acc + x end)
  end
end
