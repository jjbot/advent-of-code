defmodule Assignment12 do
  @line_pattern ~r"^(\w)(\d+)$"

  def parse_line(line) do
    [_, command, amount] = Regex.run(@line_pattern, line)
    {command, String.to_integer(amount)}
  end

  def part_a() do
    {:ok, ferry} = GenStateMachine.start_link(Ferry, {"E", {0, 0}})

    "data/aoc_2020_12_input.txt"
    # "data/test.txt"
    |> File.read!()
    |> String.split("\n", trim: true) 
    |> Enum.map(&parse_line/1)
    |> Enum.each(fn instruction -> GenStateMachine.cast(ferry, instruction) end)

    :sys.get_state(ferry)
  end


  def part_b() do
    {:ok, waypoint} = GenStateMachine.start_link(Waypoint, {{0, 0}, {10, 1}})
    "data/aoc_2020_12_input.txt"
    # "data/test.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_line/1)
    |> Enum.each(fn instruction -> GenStateMachine.cast(waypoint, instruction) end)
    
    :sys.get_state(waypoint)
  end
end
