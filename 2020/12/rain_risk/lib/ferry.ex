defmodule Ferry do
  @moduledoc """
  Finite state machine respresenting the Advent of Code 2020 - 12 Ferry.
  
  The ferry can be positioned in the 4 directions of a compass, and can be told
  to either travel in the direction it is facing or move in a specific direction,
  regardless of its orientation.
  """

  @orientation_degrees %{"N" => 0, "E" => 90, "S" => 180, "W" => 270}
  @degrees_orientation %{0 => "N", 90 => "E", 180 => "S", 270 => "W"}

  use GenStateMachine

  def handle_event(:cast, {"N", amount}, orientation, {x, y}) do
    {:next_state, orientation, {x, y + amount}}
  end

  def handle_event(:cast, {"S", amount}, orientation, {x, y}) do
    {:next_state, orientation, {x, y - amount}}
  end

  def handle_event(:cast, {"E", amount}, orientation, {x, y}) do
    {:next_state, orientation, {x + amount, y}}
  end

  def handle_event(:cast, {"W", amount}, orientation, {x, y}) do
    {:next_state, orientation, {x - amount, y}}
  end

  def handle_event(:cast, {"L", degrees}, orientation, position) do
    {:next_state, update_orientation(orientation, -degrees), position}
  end

  def handle_event(:cast, {"R", degrees}, orientation, position) do
    {:next_state, update_orientation(orientation, degrees), position}
  end

  def handle_event(:cast, {"F", amount}, "N" = orientation, {x, y}) do
    {:next_state, orientation, {x, y + amount}}
  end

  def handle_event(:cast, {"F", amount}, "S" = orientation, {x, y}) do
    {:next_state, orientation, {x, y - amount}}
  end

  def handle_event(:cast, {"F", amount}, "E" = orientation, {x, y}) do
    {:next_state, orientation, {x + amount, y}}
  end

  def handle_event(:cast, {"F", amount}, "W" = orientation, {x, y}) do
    {:next_state, orientation, {x - amount, y}}
  end

  def update_orientation(cur_orientation, rotation) do
    cur_degrees = rem(@orientation_degrees[cur_orientation] + rotation + 360, 360)
    @degrees_orientation[cur_degrees]
  end

end
