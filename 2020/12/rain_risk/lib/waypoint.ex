defmodule Waypoint do
  use GenStateMachine

  def handle_event(:cast, {"N", amount}, ship, {wx, wy}) do
    {:next_state, ship, {wx, wy + amount}}
  end

  def handle_event(:cast, {"S", amount}, ship, {wx, wy}) do
    {:next_state, ship, {wx, wy - amount}}
  end

  def handle_event(:cast, {"E", amount}, ship, {wx, wy}) do
    {:next_state, ship, {wx + amount, wy}}
  end

  def handle_event(:cast, {"W", amount}, ship, {wx, wy}) do
    {:next_state, ship, {wx - amount, wy}}
  end

  def handle_event(:cast, {"R", degrees}, ship, waypoint) do
    {:next_state, ship, rotate(waypoint, degrees)}
  end

  def handle_event(:cast, {"L", degrees}, ship, waypoint) do
    {:next_state, ship, rotate(waypoint, 360 - degrees)}
  end

  def handle_event(:cast, {"F", amount}, {sx, sy}, {wx, wy}) do
    {:next_state, {sx + amount * wx, sy + amount * wy}, {wx, wy}}
  end

  def rotate({wx, wy}, 0) do
    {wx, wy}
  end

  def rotate({wx, wy}, 90) do
    {wy, -wx}
  end

  def rotate({wx, wy}, 180) do
    {-wx, -wy}
  end

  def rotate({wx, wy}, 270) do
    {-wy, wx}
  end
end
