defmodule Assignment20 do
  # Initial attempt, only kept for documentation purposes. See assignment20_2.ex
  # for the actual implementation.
  @compass [:north, :east, :south, :west]

  def create_tile([header | lines]) do
    [id] = Regex.run(~r/(\d+)/, header, capture: :all_but_first)
    
    tile = Enum.map(lines, &String.graphemes(&1))
    [north | _] = tile
    [south | _] = Enum.reverse(tile)
    west = Enum.map(tile, fn [x | _] -> x end)
    east = Enum.map(
      tile,
      fn line ->
        [x | _] = Enum.reverse(line)
        x
      end
    )

    %{
      id: String.to_integer(id),
      north: north,
      east: east,
      south: south,
      west: west,
      match_count: 0,
      matches: %MapSet{}
    }

  end

  def lines_match?(l1, l2) do
    Enum.zip(l1, l2) |> Enum.all?(fn {v1, v2} -> v1 == v2 end) ||
      Enum.zip(l1, Enum.reverse(l2)) |> Enum.all?(fn {v1, v2} -> v1 == v2 end)
  end

  def tiles_match?(tile1, tile2)

  def tiles_match?(%{id: id}, %{id: id}), do: false

  def tiles_match?(t1, t2) do
    combi = for d1 <- @compass, d2 <- @compass, do: {d1, d2}

    combi
    |> Enum.any?(fn {d1, d2} -> lines_match?(t1[d1], t2[d2]) end)
  end

  def match_tile(tile, tiles) do
    Enum.reduce(
      tiles,
      tile,
      fn iter_tile, subject_tile ->
        if tiles_match?(iter_tile, subject_tile) do
          Map.update!(subject_tile, :match_count, & &1 + 1)
          |> Map.update!(:matches, &(MapSet.put(&1, iter_tile.id)))
        else
          subject_tile
        end
      end
    )
  end

  def get_input(file \\ "aoc_2020_20_input.txt") do
    file
    |> File.read!()
    |> String.trim()
    |> String.split("\n")
    |> Enum.chunk_by(& &1=="")
    |> Enum.reject(& &1==[""])
    |> Enum.map(&create_tile/1)
  end

  def part1() do
    tiles = get_input()

    tiles
    |> Enum.map(&match_tile(&1, tiles))
    |> Enum.filter(&(&1.match_count == 2))
    |> Enum.map(&(&1.id))
    |> Enum.reduce(1, fn nr, acc -> nr * acc end)
  end
end
