defmodule Tile do
  @moduledoc """
  Data structure and functions for dealing with Tiles.
  """

  @enforce_keys [:id, :content]
  defstruct [
    id: 0,
    content: [],
    locked: false,
    neighbours: %{north: nil, east: nil, south: nil, west: nil}
  ]

  @type edge :: [String.t()]

  @type t :: %Tile{
    id: non_neg_integer(),
    content: [edge],
    locked: boolean(),
    neighbours: %{atom() => non_neg_integer() | nil}
  }

  @type compass :: :north | :east | :south | :west
  @compass [:north, :east, :south, :west]

  @spec set_neighbour(t(), t(), compass()) :: t()
  def set_neighbour(tile, neighbour, direction) do
    put_in(tile.neighbours[direction], neighbour.id)
  end

  @spec get_edge(t(), compass()) :: edge()
  def get_edge(tile, edge)

  def get_edge(%Tile{content: [edge | _]}, :north) do
    edge
  end

  def get_edge(%Tile{content: content}, :east) do
    Enum.map(content, fn line -> [x | _] = Enum.reverse(line); x end)
  end

  def get_edge(%Tile{content: content}, :south) do
    [bottom | _] = Enum.reverse(content)
    bottom
  end

  def get_edge(%Tile{content: content}, :west) do
    Enum.map(content, fn [x | _] -> x end)
  end

  @spec from_raw([String.t()]) :: t()
  def from_raw([header | lines]) do
    [id] = Regex.run(~r/(\d+)/, header, capture: :all_but_first)
    content = Enum.map(lines, &String.graphemes(&1))
    %Tile{id: String.to_integer(id), content: content}
  end

  @spec flip_horizontal(t()) :: t()
  def flip_horizontal(tile) do
    content = Enum.map(tile.content, &Enum.reverse/1)
    %{tile | content: content}
  end

  @spec flip_vertical(t()) :: t()
  def flip_vertical(tile) do
    content = Enum.reverse(tile.content)

    %{tile | content: content}
  end

  @spec rotate_clockwise(t()) :: t()
  def rotate_clockwise(tile) do
    content =
      tile.content
      # FIXME: how is this different from Enum.reverse?
      |> Enum.reduce([], fn line, acc -> [line | acc] end)
      |> Enum.zip()
      |> Enum.map(&Tuple.to_list/1)

    %{tile | content: content}
  end

  @doc """
  Count the number set neighbours.
  """
  @spec neighbour_count(t()) :: non_neg_integer()
  def neighbour_count(tile) do
    @compass
    |> Enum.reduce(
      0,
      fn direction, acc ->
        if tile.neighbours[direction], do: acc + 1, else: acc
      end)
  end

  def remove_first_and_last(l) do
    [_ | content] = l
    [_ | content] = Enum.reverse(content)
    Enum.reverse(content)
  end

  @doc """
  Remove the borders (edges) from the tile. They are not part of the final image.
  """
  @spec trim_borders(t()) :: t()
  def trim_borders(tile) do
    content =
      tile.content
      |> remove_first_and_last()
      |> Enum.map(&remove_first_and_last/1)

    %{tile | content: content}
  end

  @spec get_full_lines(t(), Assignment202.image()) :: [edge()]
  def get_full_lines(tile, image) do
    case tile.neighbours[:east] do
      nil ->
        tile.content

      tile_id ->
        Enum.zip(get_full_lines(image[tile_id], image), tile.content)
        |> Enum.map(fn {r, l} -> l ++ r end)
    end
  end
end

defmodule Assignment202 do
  @moduledoc """
  Monster:
    00000000001111111111
    01234567890123456789
  0                   # 
  1 #    ##    ##    ###
  2  #  #  #  #  #  #
  """
  @type image :: %{pos_integer() => Tile.t()}

  @compass [:north, :east, :south, :west]
  @static_match_directions [{:north, :south}, {:east, :west}, {:south, :north}, {:west, :east}]

  @monster %{
    {18, 0} => "#",
    {0, 1} => "#",
    {5, 1} => "#",
    {6, 1} => "#",
    {11, 1} => "#",
    {12, 1} => "#",
    {17, 1} => "#",
    {18, 1} => "#",
    {19, 1} => "#",
    {1, 2} => "#",
    {4, 2} => "#",
    {7, 2} => "#",
    {10, 2} => "#",
    {13, 2} => "#",
    {16, 2} => "#",
  }

  @spec get_input(String.t()) :: [Tile.t()]
  def get_input(file \\ "aoc_2020_20_input.txt") do
    file
    |> File.read!()
    |> String.trim()
    |> String.split("\n")
    |> Enum.chunk_by(& &1=="")
    |> Enum.reject(& &1==[""])
    |> Enum.map(&Tile.from_raw/1)
  end

  @doc """
  Try to add a `Tile` to the `Image`.

  Will succees when there is at least one tile in the image that is a neighbour
  of the provided tile. Returns the updated image when adding succeeds, and the
  Tile when it doesn't.
  """
  @spec add_tile(Tile.t(), image()) :: {:matched, image()} | {:no_match, Tile.t()}
  def add_tile(tile, image) when image == %{} do
    # Deal with the case where the image is empty
    {:matched, Map.put(image, tile.id, %{tile | locked: true})}
  end

  def add_tile(tile, image) do
    # This reduce function is a bit involved...
    # Basically, this keeps track of two things: the current tile under consideration
    # and the list of tiles that are found to be its neighbours. We need to keep track
    # of the tile itself because it will be changed during the matching process, as we
    # will update the neighbours map and the locked flag.
    # The list of matched neighbours needs to be kept as we'll need to update the image
    # map with their values. By keeping this list, we can later update the image to
    # reflect the changes in the individual tiles.
    # FIXME: Wouldn't you just be able to use the image map directly in the reduce
    # function so that you don't have to go through the array?
    {dynamic_tile, matched} =
      Enum.reduce(
        image,
        {tile, []},
        fn {_, static_tile}, {dynamic_tile, changed} ->
          case match_tiles(static_tile, dynamic_tile) do
            {:matched, static_tile, dynamic_tile} ->
              {dynamic_tile, [static_tile | changed]}
            :no_match ->
              {dynamic_tile, changed}
          end
        end
      )

    # Now we can look at the amount of matched static tiles. If there are any, the new
    # Tile matched, so the updated Tiles need to be put in the Image, as does the newly
    # matched tile. Otherwise, we just return {:no_match, dynamic_tile} so the caller
    # can decide what to do.
    case matched do
      [] ->
        {:no_match, dynamic_tile}

      l ->
        image =
          Enum.reduce(l, image, fn tile, image -> Map.put(image, tile.id, tile) end)
          |> Map.put(dynamic_tile.id, put_in(dynamic_tile.locked , true))
        {:matched, image}
    end
  end

  @doc """
  Find the opposite cardinal direction.
  """
  @spec opposite(Tile.compass()) :: Tile.compass()
  def opposite(direction)
  def opposite(:north), do: :south
  def opposite(:east), do: :west
  def opposite(:south), do: :north
  def opposite(:west), do: :east

  @doc """
  See if two lines match.
  """
  @spec match_lines(Tile.edge(), Tile.edge()) :: boolean()
  def match_lines(l1, l2) do
    Enum.zip(l1, l2) |> Enum.all?(fn {v1, v2} -> v1 == v2 end)
  end

  @doc """
  Recursively check if rotating the dynamic tile matches the provided edge of
  the static tile.
  """
  @spec match_rotation(Tile.t(), Tile.compass(), Tile.t(), non_neg_integer())
      :: {Tile.t(), Tile.t()} | nil
  def match_rotation(static_tile, static_direction, dynamic_tile, rotations_left)

  def match_rotation(_, _, _, 0), do: nil

  def match_rotation(static_tile, stat_dir, dynamic_tile, rotations) do
    dyn_dir = opposite(stat_dir)
    if match_lines(Tile.get_edge(static_tile, stat_dir), Tile.get_edge(dynamic_tile, dyn_dir)) do
      static_tile = put_in(static_tile.neighbours[stat_dir], dynamic_tile.id)
      dynamic_tile =
        put_in(dynamic_tile.neighbours[dyn_dir], static_tile.id)
        |> Map.put(:locked, true)
      {static_tile, dynamic_tile}
    else
      match_rotation(static_tile, stat_dir, Tile.rotate_clockwise(dynamic_tile), rotations - 1)
    end
  end

  @doc """
  Try to match two tiles: one static (already part of the image) and one dynamic (still
  to be placed).

  Implementation is split into two parts: one where the dynamic tile has not yet matched
  any other tile, and is thus free to rotate and inverse, and one where the orientation
  has already been set and we can just deterimine if the edges line up with any other
  tile.
  """
  @spec match_tiles(Tile.t(), Tile.t()) :: {:matched, Tile.t(), Tile.t()} | :no_match
  def match_tiles(static_tile, dynamic_tile)

  def match_tiles(t1, %Tile{locked: false} = t2) do
    # Dynamic Tile has not yet been locked, free to rotate and invert.
    directions =
      @compass
      |> Enum.filter(fn direction -> t1.neighbours[direction] == nil end)

    options = for tile <- [t2, Tile.flip_horizontal(t2)], stat_dir <- directions, do: {tile, stat_dir}

    result =
      options
      |> Enum.map(fn {d, stat_dir} -> match_rotation(t1, stat_dir, d, 4) end)
      |> Enum.reject(&(&1 == nil))

    case result do
      [{static, dynamic}] -> {:matched, static, dynamic}
      [] -> :no_match
    end

  end

  def match_tiles(t1, %Tile{locked: true} = t2) do
    # Dynamic Tile has been locked, so orientation has been set. Only match edges.
    case Enum.reduce(
      @static_match_directions,
      [],
      fn {t1_dir, t2_dir}, acc ->
        if match_lines(Tile.get_edge(t1, t1_dir), Tile.get_edge(t2, t2_dir)) do 
          [{t1_dir, t2_dir} | acc]
        else
          acc
        end
      end
    ) do
      [] ->
        :no_match

      [{t1_dir, t2_dir}] ->
        {:matched, put_in(t1.neighbours[t1_dir], t2.id), put_in(t2.neighbours[t2_dir], t1.id)}
    end
  end

  @doc """
  Construct an image from a list of `Tiles`.
  """
  @spec construct_image([Tile.t()]) :: %{pos_integer() => Tile.t()}
  def construct_image(tiles) do
    construct_image(tiles, %{}, [])
  end

  @doc """
  Construct an `image` from an input list of `Tiles`. Also keeps track of a list
  of unmatched tiles that will be added to the image later on.

  The function breaks down into three scenarios:
  - where both the input list and the unmatched tiles list are empty: tis means
    we're done and can just return the image.
  - where the input list is empty but there are still tiles in the unmatched list:
    we need to process the unmatched tiles by providing them as the input list.
  - where the input list contains unmatched tiles: we need to try to match the
    first available tile to the current image.
  """
  @spec construct_image([Tile.t()], image(), [Tile.t()]) :: image()
  def construct_image(tile_list, image, non_matched)

  def construct_image([], image, []), do: image

  def construct_image([], image, unmatched) do
    IO.puts("ran out of fresh tiles, continuing with unmatched. length: #{length(unmatched)}")
    construct_image(unmatched, image, [])
  end

  def construct_image([tile | tiles], image, unmatched) do
    case add_tile(tile, image) do
      {:matched, image} ->
        construct_image(tiles, image, unmatched)
      {:no_match, tile} ->
        construct_image(tiles, image, [tile | unmatched])
    end
  end

  @doc """
  Run part 1 of the 2020-20 Assignment.

  Answer: 30425930368573
  """
  def part1() do
    get_input()
    # randomisation is not required, but a good way to check if the solution works
    # for different inputs.
    |> Enum.shuffle()
    |> construct_image()
    |> Enum.filter(fn {_id, tile} -> Tile.neighbour_count(tile) == 2 end)
    |> Enum.reduce(1, fn {id, _tile}, acc -> acc * id end)
  end

  def find_west_border(image) do
    [{_, top_left_tile}] =
      Enum.filter(
        image,
        fn {_k, tile} ->
          tile.neighbours[:north] == nil && tile.neighbours[:west] == nil
        end
      )

    Enum.reduce_while(
      1..100,
      {top_left_tile, []},
      fn _, {tile, acc} ->
        if tile.neighbours[:south] do
          {:cont, {image[tile.neighbours[:south]], [tile | acc]}}
        else
          {:halt, [tile | acc]}
        end
      end
    )
    |> Enum.reverse()
  end

  @doc """
  Add two locations together.
  """
  def add_locations(location1, location2)
  def add_locations({x1, y1} = _l1, {x2, y2} = _l2), do: {x1 + x2, y1 + y2}

  @doc """
  Check if the location in the image is the start of a sea monster.

  This is done by taking the relative positions a sea monster needs to occupy
  and compare this to the image.
  """
  def has_seamonster?(location, image) do
    Map.keys(@monster)
    |> Enum.map(fn monster_loc -> add_locations(monster_loc, location) end)
    |> Enum.all?(fn loc -> image[loc] == "#" end)
  end

  @doc """
  Walk over all positions in the image to see if a sea monster is there.
  """
  def find_monsters(image) do
    image
    |> Map.keys()
    |> Enum.filter(fn loc -> has_seamonster?(loc, image) end)
  end

  @doc """
  Rotate the entire image.
  TODO: this could be the same function as used for the tiles.
  """
  def rotate(matrix) do
    matrix
    |> Enum.reduce([], fn line, acc -> [line | acc] end)
    |> Enum.zip()
    |> Enum.map(&Tuple.to_list/1)
  end

  @doc """
  Indicate where the monsters are on the map by replacing the current character
  on that location with an "O".
  """
  def indicate_monster_locations(monster_locations, image) do
    monster_locations
    |> Enum.flat_map(
      fn monster_loc ->
        Enum.map(
          Map.keys(@monster),
          fn internal -> add_locations(monster_loc, internal) end)
      end)
    |> Enum.reduce(image, fn loc, acc -> Map.put(acc, loc, "O") end)
  end

  @doc """
  Run part 2 of the 2020-20 Assignment.

  Answer: 2453
  """
  def part2() do
    tiled_image =
      get_input()
      |> construct_image()
      |> Enum.reduce(
        %{},
        fn {id, tile}, acc -> Map.put(acc, id, Tile.trim_borders(tile)) end
      )

    # FIXME: too manual atm. Should be replaced with something that goes over
    # the image rotations to find the monsters.
    image =
      tiled_image
      |> find_west_border()
      |> Enum.flat_map(fn tile -> Tile.get_full_lines(tile, tiled_image) end)
      |> rotate()
      |> rotate()
      |> Enum.map(fn line -> Enum.with_index(line) end)
      |> Enum.with_index()
      |> Enum.flat_map(fn {line, y} -> Enum.map(line, fn {value, x} -> {x, y, value} end) end)
      |> Enum.reduce(%{}, fn {x, y, value}, acc -> Map.put(acc, {x, y}, value) end)

    monster_locations = find_monsters(image)
    image = indicate_monster_locations(monster_locations, image)
    Map.values(image) |> Enum.frequencies()
  end
end
