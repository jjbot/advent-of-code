defmodule Assignment15 do
  @input_a [20, 0, 1, 11, 6, 3]
  @example_input1 [0, 3, 6]
  @test_input1 [1, 3, 2]
  @test_input2 [2, 1, 3]

  def init_last_spoken(input) do
    input
    |> Enum.with_index(1)
    |> Enum.reduce(%{}, fn {value, index}, acc -> Map.put(acc, value, index) end)
  end

  def next_spoken(value, mem, round) do
    next =
      case Map.get(mem, value) do
        nil -> 0
        previous_round when is_integer(previous_round) -> round - previous_round
      end

    if rem(round, 10_000_000) == 0 do
      IO.puts("Round: #{round}: #{value}")
    end

    {Map.put(mem, value, round), next, round + 1}
  end

  def part_a(input, steps) do
    [x | remember] = Enum.reverse(input)
    last_spoken_mem = init_last_spoken(Enum.reverse(remember))

    Enum.reduce(
      0..(steps - length(input)),
      {last_spoken_mem, x, length(input)},
      fn _, {mem, next, round} -> next_spoken(next, mem, round) end
    )
  end
end
