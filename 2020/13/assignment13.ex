defmodule Assignment13 do
  require IEx

  def part_a() do
    [timestamp_str, timeline_str] =
      "aoc_2020_13_input.txt"
      |> File.read!()
      |> String.split("\n", trim: true)

    timestamp = String.to_integer(timestamp_str)
    timeline_str
    |> String.split(",")
    |> Enum.reject(&(&1 == "x"))
    |> Enum.map(&String.to_integer/1)
    |> Enum.map(&({&1, &1 - rem(timestamp, &1)}))
    |> Enum.min_by(&(elem(&1, 1)))
    |> (fn {line, time} -> line * time end).()
  end

  def check_bus(t, bus) do
    {line, position} = bus
    rem(t + position, line) == 0
  end

  def check_time(t, busses) do
    Enum.all?(busses, &(check_bus(t, &1)))
  end

  # def iterate(timestamp, step, [], _) do
  #   timestamp 
  # end

  # def iterate(timestamp, step, [bus | busses], done) do
  #   IEx.inspect_opts()
  #   IO.puts("timestamp: #{timestamp}, step: #{step}, bus: #{inspect bus}")
  #   next = timestamp + step
  #   if check_bus(next, bus) do
  #     step = Enum.reduce(done, next, fn {line, _}, acc -> line * acc end)
  #     iterate(next, step, busses, [bus | done])
  #   else
  #     iterate(next, step, [bus | busses], done)
  #   end
  # end

  # def part_b() do
  #   [_timestamp_str, timeline_str] =
  #     # "aoc_2020_13_input.txt"
  #     "test.txt"
  #     |> File.read!()
  #     |> String.split("\n", trim: true)

  #   busses =
  #     timeline_str
  #     |> String.split(",")
  #     |> Enum.with_index()
  #     |> Enum.reject(&(elem(&1, 0) == "x"))
  #     |> Enum.map(fn {x, i} -> {String.to_integer(x), i} end)
  #   
  #   # largest = Enum.max_by(busses, &(elem(&1, 0)))
  #   # others = List.delete(busses, largest)
  #   
  #   # {step, offset} = largest
  #   # start = floor(100_000_000_000_000 / step) * step - offset
  #   # start = floor(1_000_000_000 / step) * step - offset
  #   # start = -offset
  #   # Stream.iterate(start, &(&1 + step))
  #   # |> Stream.filter(fn t -> check_time(t, others) end)
  #   # |> Enum.take(1)

  #   # iterate(0, 1, busses, [])
  # end

  def bus_arrival?(timestamp, {id, index}) do
    IO.puts("arrival :: timestamp: #{timestamp}, id: #{id}, index: #{index}")
    rem(timestamp + index, id) == 0
  end

  def iterate(timestamp, _step, [], _) do
    timestamp
  end

  def iterate(timestamp, step, [bus | busses], done) do
    IO.puts("iterate :: timestamp: #{timestamp}, step: #{step}, bus: #{inspect bus}")
    next_time = timestamp + step
    if bus_arrival?(next_time, bus) do
      IO.puts("correct arrival")
      new_step = Enum.reduce([bus | done], 1, fn {id, _}, acc -> id * acc end)
      iterate(next_time, new_step, busses, [bus | done])
    else
      iterate(next_time, step, [bus | busses], done)
    end
  end

  def part_b2() do
    [_timestamp_str, timeline_str] =
      "aoc_2020_13_input.txt"
      # "test.txt"
      |> File.read!()
      |> String.split("\n", trim: true)

    busses =
      timeline_str
      |> String.split(",")
      |> Enum.with_index()
      |> Enum.reject(&(elem(&1, 0) == "x"))
      |> Enum.map(fn {x, i} -> {String.to_integer(x), i} end)

    iterate(0, 1, busses, [])
    
  end
end
