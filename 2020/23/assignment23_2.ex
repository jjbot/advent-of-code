defmodule Circle do
  defstruct [:current_cup, :destination_cup]

  def new([value | []], [ccw | _] = cups, circle) do
    [cw | _] = Enum.reverse(cups)
    {:ok, cup} = Agent.start(fn -> {ccw, value, cw} end)
    Agent.update(cw, fn {_, value, cw} -> {cup, value, cw} end)
    Agent.update(ccw, fn {ccw, value, _} -> {ccw, value, cup} end)

    {Enum.reverse([cup | cups]), Map.put(circle, value, cup)}
  end

  def new([value | nrs], [] = _cups, %{} = _circle) do
    {:ok, cup} = Agent.start(fn -> {nil, value, nil} end)

    new(nrs, [cup], Map.put(%{}, value, cup))
  end

  def new([value | nrs], [ccw | _] = cups, circle) do
    {:ok, cup} = Agent.start(fn -> {ccw, value, nil} end)
    Agent.update(ccw, fn {ccw, value, _} -> {ccw, value, cup} end)

    new(nrs, [cup | cups], Map.put(circle, value, cup))
  end

  def new(values), do: new(values, [], %{})

  def pick_up_three(current_cup) do
    {_, _, first} = Agent.get(current_cup, & &1)
    {_, _, middle} = Agent.get(first, & &1)
    {_, _, last} = Agent.get(middle, & &1)
    {_, _, next} = Agent.get(last, & &1)

    Agent.update(current_cup, fn {ccw, value, _cw} -> {ccw, value, next} end)
    Agent.update(next, fn {_ccw, value, cw} -> {current_cup, value, cw} end)

    {first, middle, last}
  end

  def find_destination_cup(current_cup, removed, circle, largest_values) do
    {_ccw, value, _cw} = Agent.get(current_cup, & &1)

    removed_values =
      Enum.map(
        removed,
        fn removed_pid ->
          {_, value, _} = Agent.get(removed_pid, & &1)
          value
        end
      )

    start_value = value - 1
    available_value =
      Enum.reduce_while(
        1..6,
        start_value,
        fn _i, v -> if v in removed_values, do: {:cont, v - 1}, else: {:halt, v} end
      )

    case available_value do
      0 ->
        highest_value =
          largest_values
          |> Enum.reject(fn nr -> nr in removed_values end)
          |> Enum.max()

        circle[highest_value]

      value ->
        circle[value]
    end
  end

  def put_down_three(destination_cup, [first, _middle, last]) do
    {_dc_ccw, _dc_value, dc_cw} = Agent.get(destination_cup, & &1)

    Agent.update(destination_cup, fn {dc_ccw, dc_value, _} -> {dc_ccw, dc_value, first} end)
    Agent.update(first, fn {_f_ccw, f_value, f_cw} -> {destination_cup, f_value, f_cw} end)
    Agent.update(last, fn {l_ccw, l_value, _l_cw} -> {l_ccw, l_value, dc_cw} end)
    Agent.update(dc_cw, fn {_ccw, value, cw} -> {last, value, cw} end)
    :ok
  end

  def play_round(current_cup, circle, largest_values) do
    {first, middle, last} = pick_up_three(current_cup)
    destination_cup = find_destination_cup(current_cup, [first, middle, last], circle, largest_values)
    put_down_three(destination_cup, [first, middle, last])
    {_ccw, _value, cw} = Agent.get(current_cup, & &1)
    cw
  end

  def print_cups_inner(cup, seen) do
    {_ccw, value, cw} = Agent.get(cup, & &1)
    case value do
      1 ->
        seen |> Enum.reverse() |> Enum.join("") |> IO.inspect(label: "circle")
      nr ->
        print_cups_inner(cw, [nr | seen])
    end
  end

  def print_cups(circle) do
    cup_one = circle[1]
    {_ccw, _value, cw} = Agent.get(cup_one, & &1)
    print_cups_inner(cw, [])
  end
end


defmodule Assignment23 do
  @input "186524973"
  @example "389125467"

  def get_input(input \\ @example) do
    input
    |> String.graphemes()
    |> Enum.map(&String.to_integer/1)
    |> Circle.new()
  end

  def part1() do
    {[cc | _], circle} = get_input(@input)
    # {[cc | _], circle} = get_input(@example)
    largest_values = Enum.map([0, 1, 2], fn nr -> 9 - nr end)
    
    Enum.reduce(
      1..100,
      cc,
      fn _i, cc ->
        Circle.play_round(cc, circle, largest_values)
      end
    )
    Circle.print_cups(circle)
  end

  def part2() do
    remaining = Enum.map(10..1_000_000, & &1)
    all = [1, 8, 6, 5, 2, 4, 9, 7, 3] ++ remaining
    largest_values = Enum.map([0, 1, 2], fn nr -> 1_000_000 - nr end)

    {[cc | _], circle} = Circle.new(all)
    IO.puts("Done creating circle")

    Enum.reduce(
      1..10_000_000,
      cc,
      fn i, cc ->
        if rem(i, 100_000) == 0, do: IO.puts(i)
        Circle.play_round(cc, circle, largest_values)
      end
    )
    first = circle[1]
    {_, _, second} = Agent.get(first, & &1)
    {_, value2, third} = Agent.get(second, & &1)
    {_, value3, _} = Agent.get(third, & &1)
    value2 * value3
  end
end
