defmodule Circle do
  defstruct list: [], current_cup: 0, destination_cup: 0, removed: []

  def get_cc_index(circle), do: Enum.find_index(circle.list, fn x -> x == circle.current_cup end)
  
  def get_dc_index(circle), do: Enum.find_index(circle.list, fn x -> x == circle.destination_cup end)

  def set_destination_cup(circle), do: set_destination_cup(circle, circle.current_cup)

  def set_destination_cup(circle, label) do
    dc =
      case label - 1 do
        dc when dc < 1 ->
          Enum.max(circle.list)

        dc ->
          if dc in circle.removed do
            set_destination_cup(circle, label - 1).destination_cup
          else
            dc
          end
      end

    %{circle | destination_cup: dc}
  end

  def remove_three(circle) do
    cc_idx = get_cc_index(circle)

    removed =
      1..3
      |> Enum.map(fn i -> Enum.at(circle.list, rem(cc_idx + i, 9)) end)

    new_list = Enum.reject(circle.list, fn v -> v in removed end)

    %{circle | list: new_list, removed: removed}
  end

  def stich_removed(circle) do
    list =
      case get_dc_index(circle) do
        idx when idx < 5 ->
          {h, t} = Enum.split(circle.list, idx + 1)
          h ++ circle.removed ++ t

        _ ->
          circle.list ++ circle.removed
      end

    %{circle | list: list, removed: []}
  end

  def update_current_cup(circle) do
    cidx = get_cc_index(circle)
    nidx = rem(cidx + 1, 9)
    %{circle | current_cup: Enum.at(circle.list, nidx)}
  end

  def play_round(circle) do
    circle
    |> remove_three()
    |> set_destination_cup()
    |> stich_removed()
    |> update_current_cup()
  end
end


defmodule Assignment23 do
  @input "186524973"
  @example "389125467"

  def get_input(input \\ @example) do
    [first | _] = raw =
      input
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)

    %Circle{list: raw, current_cup: first}
  end

  def part1() do
    circle = get_input(@input)
    
    circle = Enum.reduce(1..100, circle, fn _, circle -> Circle.play_round(circle) end)
    pos1 = Enum.find_index(circle.list, fn x -> x == 1 end)

    Enum.map(1..8, fn idx -> Enum.at(circle.list, rem(idx + pos1, 9)) end)
    |> Enum.join("")
  end

  def part2() do
    circle = get_input(@input)
    
    circle = Enum.reduce(1..10_000_000, circle, fn _, circle -> Circle.play_round(circle) end)
    pos1 = Enum.find_index(circle.list, fn x -> x == 1 end)

    Enum.map(1..8, fn idx -> Enum.at(circle.list, rem(idx + pos1, 9)) end)
    |> Enum.join("")
  end
end
