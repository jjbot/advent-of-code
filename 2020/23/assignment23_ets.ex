defmodule Cup do
  defstruct [:value, :next]
end

defmodule Assignment23 do
  def init(nrs) do
    circle = :ets.new(:circle, [:set, :protected, :named_table])

    {first, rest} = Enum.split(nrs, 1)
    neighbours = Stream.concat(rest, first)

    Stream.zip(nrs, neighbours)
    |> Enum.each(fn {nr, neighbour} -> :ets.insert(circle, {nr, %Cup{value: nr, next: neighbour}}) end)

    circle
  end

  def pick_up_three(current_cup) do
    [{^current_cup, %Cup{next: left}}] = :ets.lookup(:circle, current_cup)
    [{^left, %Cup{value: left_value, next: middle}}] = :ets.lookup(:circle, left)
    [{^middle, %Cup{value: middle_value, next: right}}] = :ets.lookup(:circle, middle)
    [{^right, %Cup{value: right_value, next: next}}] = :ets.lookup(:circle, right)

    :ets.insert(:circle, {current_cup, %Cup{value: current_cup, next: next}})

    [left_value, middle_value, right_value]
  end

  def find_destination_cup(current_cup, removed_values, largest_values) do
    available_value =
      Enum.reduce_while(
        1..6,
        current_cup - 1,
        fn _i, v -> if v in removed_values, do: {:cont, v - 1}, else: {:halt, v} end
      )

    case available_value do
      0 ->
        largest_values
        |> Enum.reject(fn nr -> nr in removed_values end)
        |> Enum.max()

      value ->
        value
    end

  end

  def put_down_three(dc, [first, _middle, last]) do
    [{^dc, %Cup{next: next}}] = :ets.lookup(:circle, dc)
    :ets.insert(:circle, {dc, %Cup{value: dc, next: first}})
    :ets.insert(:circle, {last, %Cup{value: last, next: next}})
  end

  def play_round(cc, largest_values) do
    removed = pick_up_three(cc)
    dc = find_destination_cup(cc, removed, largest_values)
    put_down_three(dc, removed)
    [{^cc, %Cup{next: next}}] = :ets.lookup(:circle, cc)
    next
  end

  def print_cups(1, seen) do
    seen
    |> Enum.reverse()
    |> Enum.join("")
    |> IO.puts()
  end

  def print_cups(nr, seen) do
    [{^nr, %Cup{next: next}}] = :ets.lookup(:circle, nr)
    print_cups(next, [nr | seen])
  end

  def print_cups() do
    [{1, cup_one}] = :ets.lookup(:circle, 1)
    print_cups(cup_one.next, [])
  end

  def part1() do
    "186524973"
    |> String.graphemes()
    |> Enum.map(&String.to_integer/1)
    |> init()

    Enum.reduce(1..100, 1, fn _i, cc -> play_round(cc, [7, 8, 9]) end)
  end

  def part2() do
    largest = [1_000_000, 999_999, 999_998]

    "186524973"
    |> String.graphemes()
    |> Enum.map(&String.to_integer/1)
    |> Stream.concat(10..1_000_000)
    |> init()

    Enum.reduce(
      1..10_000_000,
      1,
      fn i, cc ->
        if rem(i, 100_000) == 0, do: IO.puts(i)
        play_round(cc, largest)
      end
    )

  end
end
