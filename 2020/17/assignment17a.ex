defmodule Assignment17A do
  @replace_map %{"." => "0", "#" => "1"}
  @x_range -15..15
  @y_range -15..15
  @z_range -15..15

  @type location :: {integer(), integer(), integer()}
  @type cell_state :: 0 | 1
  @world_state :: %{location() => cell_state()}

  @doc """
  Read the assignment input from file.

  Returns a map that stores the "on" and "off" values as 1 and 0 respectively.
  Locations are referred to by a {x, y, z} tuple.
  """
  @spec get_input() :: world_state()
  def get_input() do
    "aoc_2020_17_input.txt"
    |> File.read!()
    |> String.replace([".", "#"], fn c -> @replace_map[c] end)
    |> String.split("\n", trim: true)
    |> Enum.map(&String.split(&1, "", trim: true))
    |> Enum.map(fn l -> Enum.map(l, &String.to_integer/1) end)
    |> Enum.map(&Enum.with_index/1)
    |> Enum.with_index()
    |> Enum.flat_map(
      fn {values, y} ->
        Enum.map(values, fn {value, x} -> {value, x, y} end)
      end)
    |> Enum.reduce(%{}, fn {v, x, y}, acc -> Map.put(acc, {x, y, 0}, v) end)
  end

  @doc """
  Get the amount of active neighbours.
  """
  @spec neighbour_state(world_state(), location()) :: non_neg_integer()
  def neighbour_state(space, {x, y, z}) do
    locations =
      for dx <- [-1, 0, 1],
          dy <- [-1, 0, 1],
          dz <- [-1, 0, 1],
          dx != 0 or dy != 0 or dz != 0,
      do: {x + dx, y + dy, z + dz}

    Enum.reduce(locations, 0, fn c, acc -> acc + Map.get(space, c, 0) end)
  end

  @doc """
  Based on the provided rules to determine the next state of a cell, compute its
  value.
  """
  @spec generate_next_state(world_state(), location()) :: cell_state()
  def generate_next_state(space, coord) do
    case {neighbour_state(space, coord), Map.get(space, coord, 0)} do
      {count, 0} when count == 3 -> 1
      {count, 1} when count == 2 or count == 3 -> 1
      _ -> 0
    end
  end

  @doc """
  Generate the next iteration of world state.

  `space` is the current world_state, and `count` the amount of times to iterate.
  """
  @spec generate_next_space(world_state(), non_neg_integer()) :: world_state()
  def generate_next_space(space, 0), do: space

  def generate_next_space(space, count) do
    IO.puts(count)

    locs =
      for x <- @x_range,
          y <- @y_range,
          z <- @z_range,
        do: {x, y, z}

    Enum.reduce(locs, %{}, fn c, acc -> Map.put(acc, c, generate_next_state(space, c)) end)
    |> generate_next_space(count - 1)
  end

  @doc """
  Run the first part of the challenge.

  This will read the input from disk, and run 6 iterations of the update algorithm.
  """
  @spec run_a() :: pos_integer()
  def run_a() do
    get_input()
    |> generate_next_space(6)
    |> Map.values()
    |> Enum.sum()
  end
end
