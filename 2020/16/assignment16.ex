defmodule Assignment16 do
  @range_pattern ~r/(\d+)-(\d+)/

  @doc """
  Parse the input file.

  It uses the empty lines to chunk the input into the parts we are interested
  in. Content parsing is done downstream.
  """
  @spec get_input() :: {[String.t()], String.t(), [String.t()]}
  def get_input() do
    [range_lines, _, [_, ticket_line], _, [_ | nearby_tickets], _] =
      "aoc_2020_16_input.txt"
      |> File.read!()
      |> String.split("\n")
      |> Enum.chunk_by(&(&1 == ""))

    {range_lines, ticket_line, nearby_tickets}
  end

  @doc """
  Convert the range lines into a list of tuples indicating all the ranges.

  This now destroys the association with what the range is being used for,
  as we flatmap all ranges present in the input lines. For part A this is
  fine, as we are only interested in whether a number appears in any range
  at all.
  """
  @spec get_ranges([String.t()]) :: [{pos_integer(), pos_integer()}]
  def get_ranges(range_lines) do
    range_lines
    |> Enum.flat_map(fn l -> Regex.scan(@range_pattern, l) end)
    |> Enum.map(
      fn [_, start, stop] ->
        {String.to_integer(start), String.to_integer(stop)}
      end
    )
  end

  def get_named_ranges(range_lines) do
    range_lines
    |> Enum.map(fn l -> String.split(l, ": ") end)
    |> Enum.map(fn [name, range_txt] -> {name, process_range_entry(range_txt)} end)
  end

  defp process_range_entry(range_txt) do
    Regex.scan(@range_pattern, range_txt, capture: :all_but_first)
    |> Enum.map(fn [start, stop] -> {String.to_integer(start), String.to_integer(stop)} end)
    |> Enum.reduce([], fn {start, stop}, acc -> acc ++ (for x <- start..stop, do: x) end)
    |> Enum.uniq()
  end

  @doc """
  Convert the list of range tuples into one list of numbers spanning all the
  ranges. This also gets rid of any duplicates. We can do this for part A as
  we're only interested in which numbers have been covered at all.
  """
  @spec flatten_ranges([{pos_integer(), pos_integer()}]) :: [pos_integer()]
  def flatten_ranges(ranges) do
    ranges
    |> Enum.map(fn {start, stop} -> start..stop end)
    |> Enum.map(fn c -> Enum.map(c, &(&1)) end)
    |> Enum.reduce([], fn nrs, acc -> acc ++ nrs end)
    |> Enum.uniq()
  end

  def process_ticket_line(ticket_line) do
    ticket_line
    |> String.split(",")
    |> Enum.map(&String.to_integer/1)
  end

  @doc """
  Run part A of the assignment.
  """
  @spec run_a() :: pos_integer()
  def run_a() do
    {range_lines, _ticket_line, nearby_tickets} = get_input()

    available_nrs =
      range_lines
      |> get_ranges()
      |> flatten_ranges()

    nearby_tickets
    |> Enum.map(&String.split(&1, ","))
    |> Enum.map(fn l -> Enum.map(l, &(String.to_integer(&1))) end)
    |> Enum.flat_map(fn l -> Enum.reject(l, &(&1 in available_nrs)) end)
    |> Enum.reduce(0, fn nr, acc -> nr + acc end)
  end

  def ticket_valid?(ticket, valid_nrs) do
    ticket
    |> Enum.all?(&(&1 in valid_nrs))
  end

  def find_ticket_labels(ticket, labels) do
    labels
    |> Enum.filter(fn {_name, values} -> Enum.all?(ticket, &(&1 in values)) end)
    |> Enum.map(fn {name, _values} -> name end)
  end

  def filter_known([], known), do: known

  def filter_known(to_strip, known) do
    [{[field], nr} | sorted] = Enum.sort(to_strip, fn {f1, _nr1}, {f2, _nr2} -> length(f1) <= length(f2) end)
    to_strip = Enum.map(sorted, fn {fields, nr} -> {List.delete(fields, field), nr} end)
    filter_known(to_strip, [{field, nr} | known])
  end

  def run_b() do
    {range_lines, ticket_line, nearby_tickets} = get_input()

    valid_nrs =
      range_lines
      |> get_ranges()
      |> flatten_ranges()

    named_ranges = get_named_ranges(range_lines)

    nearby_tickets
    |> Enum.map(&process_ticket_line/1)
    |> Enum.filter(&ticket_valid?(&1, valid_nrs))
    |> Enum.zip()
    |> Enum.map(&Tuple.to_list/1)
    |> Enum.map(&find_ticket_labels(&1, named_ranges))
    |> Enum.with_index()
    |> filter_known([])

    # Now you have the ticket label name to number mapping, and you can use that to match the numbers
    # on your ticket to mapping for the labels that start with "departure".
    # correct answer then becomes: 314360510573
  end
end
