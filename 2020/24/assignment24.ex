defmodule Assignment24 do
  @double_directions %{
    "w" => {-2, 0},
    "sw" => {-1, 1},
    "se" => {1, 1},
    "e" => {2, 0},
    "ne" => {1, -1},
    "nw" => {-1, -1}
  }

  def convert_to_instructions(line) do
    line
    |> String.codepoints()
    |> convert([])
    |> Enum.map(fn direction -> @double_directions[direction] end)
  end

  def convert(input_list, steps)

  def convert([], acc) do
    Enum.reverse(acc)
  end

  def convert([last | []], acc) do
    Enum.reverse([last | acc])
  end

  def convert([h1 | [h2 | r2] = r1], acc) do
    h = h1 <> h2

    if h in ["ne", "se", "sw", "nw"] do
      convert(r2, [h | acc])
    else
      convert(r1, [h1 | acc])
    end
  end

  def walk({ix, iy}, {px, py}), do: {px + ix, py + iy}

  def find_position(instructions) do
    instructions
    |> Enum.reduce({0, 0}, fn instruction, position -> walk(instruction, position) end)
  end

  def flip_tile(floor, position) do
    Map.update(
      floor,
      position,
      "black",
      fn 
        "black" -> "white"
        "white" -> "black"
      end
    )
  end

  def neighbour_colour_counts(location, floor) do
    location
    |> get_neighbour_positions()
    |> Enum.map(&Map.get(floor, &1, "white"))
    |> Enum.frequencies()
  end

  def get_neighbour_positions(location) do
    Map.values(@double_directions)
    |> Enum.map(fn step -> walk(location, step) end)
  end

  def generate_tiles_to_check(floor) do
    black_tiles =
      floor
      |> Enum.filter(fn {_location, colour} -> colour == "black" end)
      |> Enum.map(&elem(&1, 0))

    tile_set = MapSet.new(black_tiles)

    black_tiles
    |> Enum.flat_map(&get_neighbour_positions/1)
    |> MapSet.new()
    |> MapSet.union(tile_set)
  end

  def next_tile_colour(position, floor) do
    colours = neighbour_colour_counts(position, floor)

    case {Map.get(floor, position, "white"), Map.get(colours, "black", 0)} do
      {"black", 0} -> "white"
      {"black", count} when count > 2 -> "white"
      {"white", 2} -> "black"
      _ -> Map.get(floor, position, "white")
    end
  end

  def next_floor(floor) do
    floor
    |> generate_tiles_to_check()
    |> Enum.reduce(%{}, fn position, new_floor -> Map.put(new_floor, position, next_tile_colour(position, floor)) end)
  end

  def read_input(filename \\ "aoc_2020_24_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&convert_to_instructions/1)
    |> Enum.map(&find_position/1)
    |> Enum.reduce(%{}, fn position, floor -> flip_tile(floor, position) end)
  end

  def part1() do
    read_input()
    |> Enum.filter(fn {_posistion, colour} -> colour == "black" end)
    |> Enum.count()
  end

  def part2() do
    floor0 = read_input()

    Enum.reduce(1..100, floor0, fn _it, floor -> next_floor(floor) end)
    |> Map.values()
    |> Enum.frequencies()
  end
end
