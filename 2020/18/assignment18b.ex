defmodule Assignment18B do
  @moduledoc """
  ## Day 18: Operation Order
  
  AoC link: https://adventofcode.com/2020/day/18

  Solve the mathematical equations. Operator precedence has changed: now
  need to do addition before multiplication.

  Approach followed: manual recursion.
  """
  @example1 "2 * 3 + (4 * 5)" # 46
  @example2 "5 + (8 * 3 + 9 + 3 * 4 * 3)" # 1445
  @example3 "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))" # 669060
  @example4 "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2" # 23340

  @spec compute(String.t()) :: non_neg_integer()
  def compute(s) do
    compute(s, nil, nil)
  end

  def compute_inner(tail) do
    {:closed, inner_value, new_tail} = compute(tail)
    {inner_value, new_tail}
  end

  @doc """
  Main compute loop.

  This still follows the same approach as the first part, but needs to changed
  significantly to deal with operator presedence.
  """
  def compute([], value, nil), do: value

  def compute(["(" | tail], nil, nil) do
    {right_value, new_tail} = compute_inner(tail)
    compute(new_tail, right_value, nil)
  end

  def compute(["(" | tail], left_value, "+") do
    {right_value, new_tail} = compute_inner(tail)
    compute(new_tail, left_value + right_value, nil)
  end

  def compute(["(" | tail], left_value, "*") do
    # First compute the inner part of the equation.
    {right_value, new_tail} = compute_inner(tail)

    case compute(new_tail, right_value, nil) do
      # If the return value of compute is a number, we can execute the
      # multiplication.
      right_value when is_integer(right_value) ->
        left_value * right_value

      # If there is more to do on the right side, first evaluate that, as there
      # might be an addition to do first.
      {:closed,  right_value, tail} ->
        {:closed, left_value * right_value, tail}
    end
  end

  def compute([")" | tail], left_value, nil) do
    {:closed, left_value, tail}
  end

  def compute(["+" | tail], left_value, nil) do
    compute(tail, left_value, "+")
  end

  def compute(["*" | tail], left_value, nil) do
    compute(tail, left_value, "*")
  end

  def compute([nr | tail], nil, nil) do
    compute(tail, String.to_integer(nr), nil)
  end

  def compute([nr | tail], left_value, "+") do
    compute(tail, String.to_integer(nr) + left_value, nil)
  end

  def compute([nr | tail], left_value, "*") do
    case compute(tail, String.to_integer(nr), nil) do
      # If the return value of compute is a number, we can execute the
      # multiplication.
      right_value when is_integer(right_value) ->
        left_value * right_value

      # If there is more to do on the right side, first evaluate that, as there
      # might be an addition to do first.
      {right_value, new_tail} ->
        right_outcome = compute(new_tail, right_value, nil)
        left_value * right_outcome

      # This operation is part of an inner evaluation (between brackets) and are
      # now walking back. Execute the multiplication but keep the tuple structure
      # until the beginning of the inner part has been reached.
      {:closed, right_value, new_tail} ->
        {:closed, left_value * right_value, new_tail}
    end
  end

  @spec compute_string(String.t()) :: non_neg_integer()
  def compute_string(str) do
    str
    |> String.replace(" ", "")
    |> String.split("", trim: true)
    |> compute()
  end

  def part_b() do
    "aoc_2020_18_input.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&compute_string/1)
    |> Enum.sum()
  end
end
