defmodule Assignment18A do
  @moduledoc """
  ## Day 18: Operation Order
  AoC link: https://adventofcode.com/2020/day/18

  Solve the mathematical equations. Operator precedence has changed: all
  operators have the same precedence.

  Approach followed: manual recursion.
  """
  @example1 "2 * 3 + (4 * 5)" # 26
  @example2 "5 + (8 * 3 + 9 + 3 * 4 * 3)" # 437
  @example3 "5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))" # 12240
  @example4 "((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2" # 13623

  @spec compute(String.t()) :: non_neg_integer()
  def compute(s) do
    compute(s, nil, nil)
  end

  @doc """
  Main compute loop.

  Walks through the characters in the equation one by one. Addition and
  multiplication are executed as soon as the two input arguments are available.
  When encountering a '(', it first evaluates the inner content, before handing
  the result back to the operator preceeding it.
  """
  @spec compute([String.t()], non_neg_integer(), fun()) :: non_neg_integer()
  def compute([], value, _operation), do: value

  def compute(["(" | s], nil, nil) do
    case compute(s, nil, nil) do
      {inner_value, []} ->
        inner_value

      {inner_value, rest} ->
        compute(rest, inner_value, nil)
    end
  end

  def compute(["(" | s], value, operation) do
    case compute(s, nil, nil) do
      # The inner compute is done, and you get back the inner value and the
      # remaining equation sequence. When the sequence is empty, you are done,
      # just execute the operation, which gives the final result.
      {inner_value, []} ->
        operation.(value, inner_value)

      # Here, there is more of the equation to evaluate. Execute the operation,
      # and use the outcome of that as input for the computation of the rest of
      # the equation.
      {inner_value, rest} ->
        compute(rest, operation.(value, inner_value), nil)
    end
  end

  # This is the trick: you can't just continue, but need to potentially
  # evaluate the left hand operator now. Basically, this is a 'back track'
  # step, where you hand control back to the step that encountered the '('.
  def compute([")" | s], value, nil), do: {value, s}

  def compute(["+" | s], value, nil), do: compute(s, value, &Kernel.+/2)

  def compute(["*" | s], value, nil), do: compute(s, value, &Kernel.*/2)

  def compute([c | s], _value, nil), do: compute(s, String.to_integer(c), nil)

  def compute([c | s], value, operation) do
    new_value = operation.(value, String.to_integer(c))
    compute(s, new_value, nil)
  end

  @doc """
  Evaluate a single line of the input.
  """
  @spec compute_string(String.t()) :: non_neg_integer()
  def compute_string(str) do
    str
    |> String.replace(" ", "")
    |> String.split("", trim: true)
    |> compute()
  end

  @doc """
  Execute part A of this challenge.
  """
  def part_a() do
    "aoc_2020_18_input.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&compute_string/1)
    |> Enum.sum()
  end
end
