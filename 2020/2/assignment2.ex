# Advent of code 2020 - 2
# https://adventofcode.com/2020/day/2
#
# Part A
# Check password validity: Input gives you the minimum and maximum number of times
# a character can appear in the password.
#
# Part B
# Exactly one of the provided positions needs to contain the specified letter.

defmodule Assignment2 do
  # Pattern to match: 1-3 b: cdefg
  @pattern ~r"(\d+)-(\d+)\s(\w):\s(\w+)"

  @doc """
  Read input from `filename` and run it through the regex. Returns a list of
  {min, max, letter, password} tuples.
  """
  @spec get_input(String.t()) :: [{String.t(), String.t(), String.t(), String.t()}]
  def get_input(filename \\ "input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&Regex.run(@pattern, &1, capture: :all_but_first))
  end

  @doc """
  Convert one line to the formats we need. The numbers get converted to
  integers, the password to a Map with letter frequencies.
  """
  @spec process_line(String.t(), String.t(), String.t(), String.t()) ::
      {pos_integer(), pos_integer(), String.t(), %{String.t() => pos_integer()}}
  def process_line(min, max, letter, pwd) do
    {
      String.to_integer(min),
      String.to_integer(max),
      letter,
      String.graphemes(pwd) |> Enum.frequencies()
    }
  end

  @doc """
  Use combination of String.graphemes() to get the letters and the
  Enum.frequencies function to get a Map of characters => counts. Filter out
  any entries for which `l` (letter) count is not within range.

  Final answer: 560
  """
  @spec part_a() :: pos_integer()
  def part_a() do
    get_input()
    |> Enum.map(fn [min, max, l, s] -> process_line(min, max, l, s) end)
    |> Enum.filter(fn {min, max, l, counts} -> counts[l] >= min && counts[l] <= max end)
    |> Enum.count()
  end

  @doc """
  Same parsing as before. Check if either of the two positions contains the
  specified letter, but not both.

  Final answer: 303
  """
  @spec part_b() :: pos_integer()
  def part_b() do
    get_input()
    |> Enum.map(fn [min, max, l, s] -> {String.to_integer(min) - 1, String.to_integer(max) - 1, l, s} end)
    |> Enum.map(fn {x, y, l, s} -> {l, String.at(s, x), String.at(s, y)} end)
    |> Enum.filter(fn {l, first, second} -> (l == first || l == second) && first != second end)
    |> Enum.count()
  end
end
