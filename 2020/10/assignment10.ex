defmodule Assignment10 do
  def part_a() do
    "aoc_2020_10_input.txt"
    # "test.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_integer/1)
    |> (fn x -> [0 | x] end).()
    |> (fn x -> [Enum.max(x) + 3 | x] end).()
    |> Enum.sort()
    |> Enum.chunk_every(2, 1)
    |> Enum.reject(&(length(&1) == 1))
    |> Enum.map(fn [x, y] -> y - x end)
    |> Enum.frequencies()
    |> (fn counts -> counts[1] * counts[3] end).()
  end

  def count_permutations([head | tail], current) do
    case tail do
      [] -> 0
      [_x] -> 1
      [next | long_tail] ->
        if current + 3 >= next do
          count_permutations(tail, head) + count_permutations(long_tail, next)
        else
          count_permutations(tail, head)
        end
    end
  end

  def part_b() do
    # "aoc_2020_10_input.txt"
    "test.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_integer/1)
    |> (fn x -> [0 | x] end).()
    |> (fn x -> [Enum.max(x) + 3 | x] end).()
    |> Enum.sort()
    |> count_permutations(0)
  end
end
