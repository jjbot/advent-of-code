defmodule Assignment8 do
  @line_pattern ~r"^(\w{3})\s([+ | -]\d+)$"

  def parse_line(line) do
    [_, instruction, number] = Regex.run(@line_pattern, line)
    {instruction, String.to_integer(number)}
  end

  def run_instruction(instructions, index, acc, seen) do
    if index in seen do
      acc
    else
      case Enum.at(instructions, index) do
        {"acc", nr} -> run_instruction(instructions, index + 1, acc + nr, [index | seen])
        {"nop", nr} -> run_instruction(instructions, index + 1, acc, [index | seen])
        {"jmp", nr} -> run_instruction(instructions, index + nr, acc, [index | seen])
        _ -> acc
      end
    end
  end

  def terminates?(instructions, index, acc, seen) do
    if index in seen do
      false
    else
      case Enum.at(instructions, index) do
        {"acc", nr} -> terminates?(instructions, index + 1, acc + nr, [index | seen])
        {"nop", nr} -> terminates?(instructions, index + 1, acc, [index | seen])
        {"jmp", nr} -> terminates?(instructions, index + nr, acc, [index | seen])
        _ -> index
      end
    end
  end

  def get_input() do
    "aoc_2020_8_input.txt"
    |> File.read!()
    |> String.split("\n")
    |> Stream.reject(&(&1 == ""))
    |> Enum.map(&parse_line/1)
  end

  def part_a() do

    # A, answer: 1137
    run_instruction(get_input(), 0, 0, [])
  end

  def part_b() do
    # B, answer: 1125
    instructions = get_input()
    l = length(instructions)

    0 .. length(instructions) - 1
    |> Enum.map(fn i ->
        {changed, new_instructions} =
          case Enum.at(instructions, i) do
            {"nop", nr} -> {true, List.update_at(instructions, i, fn _ -> {"jmp", nr} end)}
            {"jmp", nr} -> {true, List.update_at(instructions, i, fn _ -> {"nop", nr} end)}
            _ -> {false, instructions}
          end

        if changed do
          case terminates?(new_instructions, 0, 0, []) do
            index when is_integer(index) ->
              if index == l do
                IO.puts(run_instruction(new_instructions, 0, 0, []))
              end
            _ -> :ok
          end
        end
      end)

  end
end
