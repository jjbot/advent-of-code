# Advent of code: 2020 - 1
# https://adventofcode.com/2020/day/1
#
# Part A
# Find two numbers in the input that sum to 2020. Multiply those two numbers to
# get the answer.
#
# Part B
# Find three numbers in the input that sum to 2020. Multiply all of them to get
# the answer.

defmodule Assignment1 do
  def get_input() do
    "input.txt"
    |> File.read!()
    |> String.split("\n")
    |> Enum.filter(&(&1 != ""))
    |> Enum.map(&String.to_integer/1)
  end

  @doc """
  Simple solution for part a: use a double for loop to find all the combinations,
  when they add up to 2020, return their product.
  """
  def part1() do
    numbers = get_input()
    for x <- numbers, y <- numbers, x + y == 2020, do: x * y
  end

  @doc """
  Same as before, but now for three numbers.
  """
  def part2() do
    numbers = get_input()
    for x <- numbers, y <- numbers, z <- numbers, x + y + z == 2020, do: x * y * z
  end
end
