defmodule Assignment6 do

  # part a: 5042
  # part b: 1086

  def distribute(memory, _pos, 0) do
    memory
  end

  def distribute(memory, pos, amount) do
    distribute(Map.update!(memory, rem(pos, 16), &(&1 + 1)), pos + 1, amount - 1)
  end

  def loop(memory, {max_pos, amount}, seen, count) do
    memory = Map.put(memory, max_pos, 0)
    memory = distribute(memory, max_pos + 1, amount)

    if memory in seen do
      IO.puts(inspect memory)
      {count, memory}
    else
      loop(memory, Enum.max_by(memory, fn {_k, v} -> v end), [memory | seen], count + 1)
    end
  end

  def part_a() do
    input = 
      [5, 1, 10, 0, 1, 7, 13, 14, 3, 12, 8, 10, 7, 12, 0, 6]
      |> Enum.with_index()
      |> Enum.reduce(%{}, fn {value, index}, acc -> Map.put(acc, index, value) end)

    {count, memory} = loop(input, Enum.max_by(input, fn {_k, v} -> v end), [], 1)
    IO.puts("Part a: #{count}")

    {count, memory} = loop(memory, Enum.max_by(memory, fn {_k, v} -> v end), [memory], 1)
    IO.puts("Part b: #{count}")
  end
end

