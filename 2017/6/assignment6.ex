defmodule Assignment6 do

  # part a: 5042
  # part b: 1086
  
  # Distribute and distribute2 do the exact same thing, but distribute does it
  # through recursion, distribute2 through a map_reduce function

  @doc """
  Distribute `amount` of resources across the memory banks starting from `pos`. Adds
  one to each of the locations. Assumes 16 memory banks are available.
  """
  @spec distribute(%{integer() => integer()}, integer(), integer()) :: %{integer() => integer()}
  def distribute(memory, _pos, 0) do
    memory
  end

  def distribute(memory, pos, amount) do
    distribute(
      Map.update!(memory, rem(pos, 16), &(&1 + 1)),
      pos + 1,
      amount - 1
    )
  end

  @doc """
  Distribute `amount` of resources across the memory banks starting from `pos`. Adds
  one to each of the locations.
  """
  @spec distribute2(%{integer() => integer()}, integer(), integer()) :: %{integer() => integer()}
  def distribute2(memory, pos, amount) do
    Enum.map_reduce(
      0..amount - 1,
      memory,
      fn x, acc -> {
        x, Map.update!(acc, rem(pos + x, 16), &(&1 + 1))
      } end)
  end

  @doc """
  Alternative way of solving the assignment. This time, using Stream.iterate to
  generate the memory bank configurations, and Stream.take_while to find out when
  a duplicate element occurs.
  """
  def loop2() do
    memory = 
      [5, 1, 10, 0, 1, 7, 13, 14, 3, 12, 8, 10, 7, 12, 0, 6]
      |> Enum.with_index()
      |> Enum.reduce(%{}, fn {value, index}, acc -> Map.put(acc, index, value) end)

    Stream.iterate(
      {memory, []},
      fn {memory, seen} ->
        {max_pos, amount} = Enum.max_by(memory, fn {_k, v} -> v end)
        {_, memory_new} = distribute2(Map.put(memory, max_pos, 0), max_pos + 1, amount)
        {memory_new, [memory | seen]}
      end)
    |> Stream.take_while(fn {memory, seen} -> memory not in seen end)
    |> Enum.reverse()
    # |> Enum.take(6000)
  end

  @doc """
  Original implementation, relying on explicit recuresion.
  """
  def loop(memory, {max_pos, amount}, seen, count) do
    memory = 
      memory
      |> Map.put(max_pos, 0)
      |> distribute2(max_pos + 1, amount)
      |> elem(1)

    if memory in seen do
      IO.puts(inspect memory)
      {count, memory}
    else
      loop(
        memory,
        Enum.max_by(memory, fn {_k, v} -> v end),
        [memory | seen],
        count + 1
      )
    end
  end

  def part_a() do
    input = 
      [5, 1, 10, 0, 1, 7, 13, 14, 3, 12, 8, 10, 7, 12, 0, 6]
      |> Enum.with_index()
      |> Enum.reduce(%{}, fn {value, index}, acc -> Map.put(acc, index, value) end)

    {count, memory} = loop(input, Enum.max_by(input, fn {_k, v} -> v end), [], 1)
    IO.puts("Part a: #{count}")

    {count, memory} = loop(memory, Enum.max_by(memory, fn {_k, v} -> v end), [memory], 1)
    IO.puts("Part b: #{count}")
  end

  @doc """
  Test function to figure out how the hell Stream.iterate and Stream.take_while
  work. Here, we generate a list of numbers and stop once we see the first multiple
  of 5.
  """
  def test() do
    Stream.iterate({0, []}, fn {x, seen} -> {x + 1, [rem(x, 5) | seen]} end)
    |> IO.inspect()
    |> Stream.take_while(fn {x, seen} -> rem(x, 5) not in seen end)
    |> Enum.take(10)
    |> IO.inspect()
  end
end

