
def distribute(memory):
    m = max(memory)
    i = memory.index(m)
    memory[i] = 0
    for x in range(m):
        idx = (i + x + 1) % 16
        memory[idx] = memory[idx] + 1

    return memory

def loop(memory, seen):
    count = 0
    while True:
        count += 1
        memory = distribute(memory)
        if memory in seen:
            return memory, count
        else:
            seen.append(memory.copy())

def main():
    memory = [5, 1, 10, 0, 1, 7, 13, 14, 3, 12, 8, 10, 7, 12, 0, 6]
    memory, count = loop(memory, [])
    print(f"Part a: {count}")

    memory, count = loop(memory, [memory.copy()])
    print(f"Part b: {count}")


if __name__ == "__main__":
    main()
