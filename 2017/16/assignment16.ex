defmodule Assignment16 do
  @exchange_pattern ~r"^(\d+)\/(\d+)$"
  @partners_pattern ~r"(\w)\/(\w)$"
  @spin_pattern ~r"^(\d+)$"

  def parse("x" <> s) do
    [_, x, y] = Regex.run(@exchange_pattern, s)
    {"x", String.to_integer(x), String.to_integer(y)}
  end

  def parse("p" <> s) do
    [_, x, y] = Regex.run(@partners_pattern, s)
    {"p", x, y}
  end

  def parse("s" <> s) do
    [_, x] = Regex.run(@spin_pattern, s)
    {"s", String.to_integer(x)}
  end

  def invert(m) do
    Enum.reduce(m, %{}, fn {k, v}, acc -> Map.put(acc, v, k) end)
  end

  def swap(m, x, y) do
    x_val = m[x]
    y_val = m[y]
    new_m = Map.put(m, x, y_val)
    Map.put(new_m, y, x_val)
  end

  def dance({"x", x, y}, {by_pos, _by_letter}) do
    new_by_pos = swap(by_pos, x, y)
    {new_by_pos, invert(new_by_pos)}
  end

  def dance({"p", x, y}, {_by_pos, by_letter}) do
    new_by_letter = swap(by_letter, x, y)
    {invert(new_by_letter), new_by_letter}
  end

  def dance({"s", nr}, {by_pos, _by_letter}) do
    head = for i <- 15 - (nr - 1) .. 15, do: by_pos[i]
    tail = for i <- 0 .. 15 - nr, do: by_pos[i]

    new_by_pos =
      head ++ tail
      |> Enum.with_index()
      |> Enum.reduce(%{}, fn {letter, index}, acc -> Map.put(acc, index, letter) end )

    {new_by_pos, invert(new_by_pos)}
  end

  def get_input() do
    "aoc_2017_16_input.txt"
    |> File.read!
    |> String.split(",")
    |> Enum.reject(&(&1 == ""))
    |> Enum.map(&parse/1)
  end

  def get_start() do
    "abcdefghijklmnop"
    |> String.codepoints()
    |> Enum.with_index()
    |> Enum.reduce(%{}, fn {letter, index}, acc -> Map.put(acc, index, letter) end )
  end
  
  def run(input, maps) do
    Enum.reduce(input, maps, fn d, acc -> dance(d, acc) end )
  end

  def part_a() do
    start = get_start()
    input = get_input()

    {by_pos, _} = run(input, {start, invert(start)})

    Enum.reduce(0..15, "", fn i, s -> s <> by_pos[i] end)
  end

  def run_b(input, maps, acc, todo) when todo > 0 do
    maps = {m, _} = run(input, maps)
    s = Enum.reduce(m, "", fn {_k, v}, acc -> acc <> v end)
      
    if s in acc do
      {s, acc, todo}
    else
      run_b(input, maps, [s | acc], todo - 1)
    end
  end

  def run_b(_input, _maps, [h | _acc], _todo) do
    h
  end

  def part_b() do
    start = get_start()
    input = get_input()

    # Found 40 by running a larger number and knowing when first repeat occurs
    # turns out that is at 60 dances. rem(1b, 60) == 40
    run_b(input, {start, invert(start)}, [], 40)
  end
end
