import math

def parta():
    nr = 361527

    # Figure out how big the square needs to be to contain the nr.
    x = math.ceil(math.sqrt(nr))
    if x % 2 == 0:
        x = x + 1
    print(f"x: {x}")

    # Determine the long edge: regardless of where on the edge it is, it needs
    # to always be at the maxium distance. This is defined by the size of the
    # square.
    long_edge = (x - 1) / 2

    # The middle for square side you know to have an even number of cells:
    middle = (x - 1) / 2 + 1

    # How many sqaures between bottom right corner and target?
    too_much = (x ** 2) - nr

    # We don't care which edge we are on, so normalise for that.
    edge_pos = too_much % (x - 1)

    # How far are we from the middle of the edge?
    short_edge = abs(edge_pos - (middle - 1))

    # Manhattan distance is the sum of the short and long edge.
    distance = short_edge + long_edge
    print(f"distance: {distance}")

# Part B
def sum_neighbours(x_pos, y_pos, locations):
    neighbour_sum = 0
    for dx in [-1, 0, 1]:
        for dy in [-1, 0, 1]:
            x = x_pos + dx
            y = y_pos + dy
            print(f"x: {x}, y: {y} value: {locations.get((x, y), 0)}")
            neighbour_sum += locations.get((x, y), 0)
    return neighbour_sum


def partb():
    locations = {}
    target = 361527
    x_pos = 0
    y_pos = 0
    locations[(x_pos, y_pos)] = 1
    direction = "right"

    for x in range(100):
        if direction == "right":
           x_pos += 1
           if locations.get((x_pos, y_pos + 1), 0) == 0:
               direction = "up"
        elif direction == "up":
           y_pos += 1
           if locations.get((x_pos -1, y_pos), 0) == 0:
               direction = "left"
        elif direction == "left":
           x_pos -= 1
           if locations.get((x_pos, y_pos - 1), 0) == 0:
               direction = "down"
        elif direction == "down":
           y_pos -= 1
           if locations.get((x_pos + 1, y_pos), 0) == 0:
               direction = "right"
        else:
            raise Exception("This should never happen")
        
        neighbour_sum = sum_neighbours(x_pos, y_pos, locations)
        print(locations)
        print(neighbour_sum)
        if neighbour_sum > target:
            exit(0)
        else:
            print(f"{x_pos}, {y_pos}")
            locations[(x_pos, y_pos)] = neighbour_sum

if __name__ == "__main__":
    # parta()
    partb()
