defmodule Assignment2 do

  @pattern ~r"(\d+)"

  def parse_line(s) do
    Regex.scan(@pattern, s)
    |> Enum.map(fn [_, v] -> v end)
    |> Enum.map(&String.to_integer/1)
  end

  def part_a() do
    "aoc_2017_2_input.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_line/1)
    |> Enum.map(fn l -> Enum.max(l) - Enum.min(l) end)
    |> Enum.sum()
  end

  def find_div(nrs) do
    for x <- nrs, y <- nrs, x != y, rem(x, y) == 0 do
      div(x, y)
    end
  end

  def part_b() do
    "aoc_2017_2_input.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_line/1)
    |> Enum.flat_map(&find_div/1)
    |> Enum.sum()
  end
end
