defmodule Assignment4 do
  def get_input(filename \\ "aoc_2017_04_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(fn line -> String.split(line, ~r"\s+", trim: true) end)
  end

  def part1() do
    get_input()
    |> Enum.map(fn line -> {line, MapSet.new(line)} end)
    |> Enum.filter(fn {line, set} -> length(line) == MapSet.size(set) end)
    |> length()
  end

  def sort_line(line) do
    line
    |> Enum.map(fn word -> String.split(word, "") |> Enum.sort() |> Enum.join("") end)
    |> Enum.sort()
  end

  def filter_line(line) do
    line
    |> sort_line()
    |> Enum.chunk_every(2, 1, :discard)
    |> Enum.any?(fn [a, b] -> a == b end)
  end

  def part2() do
    get_input()
    |> Enum.reject(&filter_line/1)
    |> length()
  end
end
