defmodule Assignment1 do
  def match([nr, nr]), do: nr
  def match([_, _]), do: 0
  def match([nr]), do: nr

  def part_a() do
    "aoc_2017_01_input.txt"
    |> File.read!()
    |> String.trim()
    |> String.split("")
    |> Enum.reject(&(&1 == ""))
    |> Enum.map(&String.to_integer(&1))
    |> Enum.chunk_every(2, 1)
    |> Enum.map(&match/1)
    |> Enum.sum()
  end

  def split(l), do: Enum.split(l, div(length(l), 2))
  def combine({x, y}), do: Enum.zip(x, y)

  def part_b() do
    "aoc_2017_01_input.txt"
    |> File.read!()
    |> String.trim()
    |> String.split("")
    |> Enum.reject(&(&1 == ""))
    |> Enum.map(&String.to_integer/1)
    |> split()
    |> combine()
    |> Enum.map(&Tuple.to_list/1)
    |> Enum.map(&match/1)
    |> Enum.sum()
    |> Kernel.*(2)
  end
end
