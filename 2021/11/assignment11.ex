defmodule Assignment11 do
  @neighbours [{-1, -1}, {0, -1}, {1, -1}, {-1, 0}, {1, 0}, {-1, 1}, {0, 1}, {1, 1}]

  def process_line({l, y}) do
    l
    |> String.split("", trim: true)
    |> Enum.map(&String.to_integer/1)
    |> Enum.with_index()
    |> Enum.map(fn {v, x} -> {{x, y}, {v, false}} end)
  end

  def get_input(filename \\ "aoc_2021_11_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.with_index()
    |> Enum.flat_map(&process_line/1)
    |> Enum.into(%{})
  end

  def in_board?({x, y}), do: x >= 0 && x <= 9 && y >= 0 && y <= 9

  def add_delta({x, y}, {dx, dy}), do: {x + dx, y + dy}

  def increase_neighbours(pos, board) do
    @neighbours
    |> Enum.map(&add_delta(pos, &1))
    |> Enum.filter(&in_board?/1)
    |> Enum.reduce(board, fn pos, board ->
      {_value, flashed} = board[pos]

      if flashed do
        board
      else
        Map.update!(board, pos, fn {value, flashed} -> {value + 1, flashed} end)
      end
    end)
  end

  def flash(board, flash_count) do
    IO.inspect(flash_count, label: "flash count")

    Enum.reduce(
      Map.keys(board),
      {board, flash_count, false},
      fn pos, {board, flash_count, any_flashed} ->
        case board[pos] do
          {value, false} when value >= 10 ->
            board = increase_neighbours(pos, board)
            {Map.put(board, pos, {0, true}), flash_count + 1, true}

          _ ->
            {board, flash_count, any_flashed}
        end
      end
    )
  end

  def step(board, flash_count) do
    # Increase value and reset flash tracker
    board =
      Enum.reduce(Map.keys(board), board, fn pos, board ->
        Map.update!(board, pos, fn {value, _} -> {value + 1, false} end)
      end)

    Enum.reduce_while(
      0..10,
      {board, flash_count},
      fn i, {board, flash_count} ->
        case flash(board, flash_count) do
          {board, flash_count, true} -> {:cont, {board, flash_count}}
          {board, flash_count, false} -> {:halt, {board, flash_count}}
        end
      end
    )
  end

  def part1() do
    board = get_input()
    Enum.reduce(1..100, {board, 0}, fn _i, {board, flash_count} -> step(board, flash_count) end)
  end

  def part2() do
    board = get_input()

    Enum.reduce_while(
      1..1_000,
      {board, 0},
      fn i, {board, flash_count} ->
        {board, flash_count} = step(board, flash_count)

        if Enum.all?(board, fn {_pos, {value, _flashed}} -> value == 0 end) do
          {:halt, i}
        else
          {:cont, {board, flash_count}}
        end
      end
    )
  end
end
