defmodule Assignment3 do
  def get_input(filename \\ "aoc_2021_03_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(fn line ->
      String.split(line, "", trim: true) |> Enum.map(&String.to_integer/1)
    end)
  end

  def get_rate_bits(%{0 => zero_count, 1 => one_count}) when zero_count > one_count, do: {0, 1}
  def get_rate_bits(_), do: {1, 0}

  def part1() do
    {gamma_list, epsilon_list} =
      get_input()
      |> Enum.zip()
      |> Enum.map(&Tuple.to_list/1)
      |> Enum.map(&Enum.frequencies/1)
      |> Enum.map(&get_rate_bits/1)
      |> Enum.reduce({[], []}, fn {g, e}, {gl, el} -> {[g | gl], [e | el]} end)
      |> (fn {g, e} -> {Enum.reverse(g), Enum.reverse(e)} end).()

    gamma = Enum.join(gamma_list, "") |> String.to_integer(2)
    epsilon = Enum.join(epsilon_list, "") |> String.to_integer(2)

    gamma * epsilon
  end

  def filter_on_bit(lines, i, type) do
    {most_frequent, least_frequent} =
      lines
      |> Enum.map(& &1[i])
      |> Enum.frequencies()
      |> get_rate_bits()

    to_filter_on =
      case type do
        :oxygen -> most_frequent
        :scrubber -> least_frequent
      end

    Enum.filter(lines, fn line -> line[i] == to_filter_on end)
  end

  def part2() do
    lines =
      get_input()
      |> Enum.map(fn line ->
        Enum.with_index(line) |> Enum.reduce(%{}, fn {v, i}, acc -> Map.put(acc, i, v) end)
      end)

    oxygen =
      Enum.reduce_while(
        0..11,
        lines,
        fn i, lines ->
          new_lines = filter_on_bit(lines, i, :oxygen)

          if length(new_lines) > 1 do
            {:cont, new_lines}
          else
            {:halt, new_lines}
          end
        end
      )
      |> IO.inspect()
      |> extract()

    scrubber =
      Enum.reduce_while(
        0..11,
        lines,
        fn i, lines ->
          new_lines = filter_on_bit(lines, i, :scrubber)

          if length(new_lines) > 1 do
            {:cont, new_lines}
          else
            {:halt, new_lines}
          end
        end
      )
      |> IO.inspect()
      |> extract()

    oxygen * scrubber
  end

  def extract(lines) do
    line = List.first(lines)

    Enum.map(0..11, fn i -> line[i] end)
    |> Enum.join("")
    |> String.to_integer(2)
  end
end
