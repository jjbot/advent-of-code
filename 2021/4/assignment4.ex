defmodule Card do
  defstruct [:grid, :matched]

  defp extract_numbers(line) do
    line
    |> String.split(~r"\s+", trim: true)
    |> Enum.map(fn s -> String.to_integer(s) end)
  end

  defp lines_to_map(lines) do
    lines
    |> Enum.flat_map(fn {line, y} -> Enum.map(line, fn {value, x} -> {value, {x, y}} end) end)
    |> Enum.into(%{})
  end

  @doc """
  Create a card from a set of lines.

  Expects there to be 5 lines with 5 numbers each.
  """
  def card_from_lines(lines) do
    grid =
      lines
      |> Enum.map(fn l -> extract_numbers(l) |> Enum.with_index() end)
      |> Enum.with_index()
      |> lines_to_map()

    %Card{grid: grid, matched: MapSet.new()}
  end

  defp check_rows(%Card{matched: matched}) do
    locs =
      0..4
      |> Enum.map(fn y -> Enum.map(0..4, fn x -> {x, y} end) end)

    locs
    |> Enum.any?(fn row -> Enum.all?(row, fn loc -> MapSet.member?(matched, loc) end) end)
  end

  defp check_columns(%Card{matched: matched}) do
    locs =
      0..4
      |> Enum.map(fn x -> Enum.map(0..4, fn y -> {x, y} end) end)

    locs
    |> Enum.any?(fn row -> Enum.all?(row, fn loc -> MapSet.member?(matched, loc) end) end)
  end

  @doc """
  Check if the provided `card` has Bingo.
  """
  def bingo?(card) do
    check_rows(card) || check_columns(card)
  end

  @doc """
  Check off the provided `number` on the `card` when it's present.

  Returns a tuple: {has_bingo, card}
  """
  def tick_number(%Card{grid: grid, matched: matched} = c, nr) do
    if Map.has_key?(grid, nr) do
      card = %{c | matched: MapSet.put(matched, grid[nr])}
      {bingo?(card), card}
    else
      {false, c}
    end
  end

  def calulate_score(card, nr) do
    locs =
      0..4
      |> Enum.flat_map(fn x -> Enum.map(0..4, fn y -> {x, y} end) end)

    all_locs = MapSet.new(locs)
    unmarked = MapSet.difference(all_locs, card.matched)

    card.grid
    |> Enum.filter(fn {value, loc} -> MapSet.member?(unmarked, loc) end)
    |> Enum.reduce(0, fn {value, _loc}, acc -> acc + value end)
    |> Kernel.*(nr)
  end
end

defmodule Assignment4 do
  def get_input(filename \\ "aoc_2021_04_input.txt") do
    [[drawn] | cards] =
      filename
      |> File.read!()
      |> String.split("\n")
      |> Enum.chunk_by(&(&1 == ""))
      |> Enum.reject(&(&1 == [""]))

    drawn =
      drawn
      |> String.split(",")
      |> Enum.map(&String.to_integer/1)

    cards = Enum.map(cards, &Card.card_from_lines/1)

    {drawn, cards}
  end

  # Update all cards by ticking of the provided number.
  # For part 1, we're only interested in the first card that achieves bingo, so
  # we make sure to halt the reduce_while function as soon as we find the first
  # card that has bingo.
  defp update_cards(cards, nr) do
    {done, todo} =
      cards
      |> Enum.map(fn card -> Card.tick_number(card, nr) end)
      |> Enum.split_with(fn {done, card} -> done end)

    if length(done) > 0 do
      [{_, card}] = done
      {:halt, {nr, card}}
    else
      cleaned =
        todo
        |> Enum.map(fn {_, card} -> card end)

      {:cont, cleaned}
    end
  end

  # Update all card by ticking of the provided number.
  # For part 2, we want the last card that achieves bingo, so we keep track of
  # all cards that acieved bingo, and update the accumulator with them. The first
  # card in the updated bingo list will be the last card to have achieved bingo.
  defp update_cards2(cards, nr) do
    {done, todo} =
      cards
      |> Enum.map(fn card -> Card.tick_number(card, nr) end)
      |> Enum.split_with(fn {done, card} -> done end)

    todo_cleaned = Enum.map(todo, fn {_, card} -> card end)

    done_cleaned =
      done
      |> Enum.map(fn {_, card} -> {nr, card} end)

    {todo_cleaned, done_cleaned}
  end

  def part1() do
    {drawn, cards} = get_input()

    {nr, card} =
      Enum.reduce_while(
        drawn,
        cards,
        fn nr, cards -> update_cards(cards, nr) end
      )

    Card.calulate_score(card, nr)
  end

  def part2() do
    {drawn, cards} = get_input()

    {todo, [last | done]} =
      Enum.reduce_while(
        drawn,
        {cards, []},
        fn nr, {todo, done} ->
          {new_todo, new_done} = update_cards2(todo, nr)
          {:cont, {new_todo, new_done ++ done}}
        end
      )

    {nr, card} = last

    Card.calulate_score(card, nr)
  end
end
