defmodule Assignment8 do
  def to_set(str) do
    str
    |> String.split("", trim: true)
    |> MapSet.new()
  end

  def process_line(line) do
    [input, output] =
      line
      |> String.split(" | ")
      |> Enum.map(&String.split/1)

    {Enum.map(input, &to_set/1), Enum.map(output, &to_set/1)}
  end

  def get_input(filename \\ "aoc_2021_08_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&process_line/1)
  end

  def is_one?(seq), do: MapSet.size(seq) == 2

  def is_four?(seq), do: MapSet.size(seq) == 4

  def is_seven?(seq), do: MapSet.size(seq) == 3

  def is_eight?(seq), do: MapSet.size(seq) == 7

  def is_1478(seq), do: is_one?(seq) + is_four?(seq) + is_seven?(seq) + is_eight?(seq)

  def check_part1(output) do
    output
    |> Enum.map(&is_1478/1)
    |> Enum.sum()
  end

  def part1() do
    get_input()
    |> Enum.map(fn {_input, output} -> check_part1(output) end)
    |> Enum.sum()
  end

  def find_1478(input) do
    {[one], remainder} = Enum.split_with(input, &is_one?/1)
    {[four], remainder} = Enum.split_with(remainder, &is_four?/1)
    {[seven], remainder} = Enum.split_with(remainder, &is_seven?/1)
    {[eight], remainder} = Enum.split_with(remainder, &is_eight?/1)

    {{one, four, seven, eight}, remainder}
  end

  def check_size(elem, size), do: MapSet.size(elem) == size

  def map_numbers(input) do
    {{one, four, seven, eight}, remainder} = find_1478(input)

    [top] = MapSet.difference(seven, one) |> MapSet.to_list()

    l_fives = Enum.filter(remainder, fn elem -> MapSet.size(elem) == 5 end)
    l_sixes = Enum.filter(remainder, fn elem -> MapSet.size(elem) == 6 end)

    {[three], r_fives} = Enum.split_with(l_fives, fn elem -> MapSet.intersection(elem, one) |> check_size(2) end)

    middle = MapSet.intersection(three, four) |> MapSet.difference(one)
    zero = MapSet.difference(eight, middle)

    {[nine], [six]} =
      l_sixes
      |> Enum.reject(&(&1 == zero))
      |> Enum.split_with(fn elem -> MapSet.intersection(elem, three) |> check_size(5) end)

    bottom_left = MapSet.difference(eight, nine)
    {[two], [five]} =
      r_fives
      |> Enum.split_with(fn elem -> MapSet.intersection(elem, bottom_left) |> check_size(1) end)

    %{zero => 0, one => 1, two => 2, three => 3, four => 4, five => 5, six => 6, seven => 7, eight => 8, nine => 9}
  end

  def determine_line(input, output) do
    mapping = map_numbers(input)
    output
    |> Enum.map(&(mapping[&1]))
    |> Enum.join("")
    |> String.to_integer()
  end

  def part2() do
    get_input()
    |> Enum.map(fn {input, output} -> determine_line(input, output) end)
    |> Enum.sum()
  end
end
