defmodule Assignment10 do
  @scores %{")" => 3, "]" => 57, "}" => 1197, ">" => 25137}
  @completion %{"(" => 1, "[" => 2, "{" => 3, "<" => 4}

  def get_input(filename \\ "aoc_2021_10_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.split(&1, "", trim: true))
  end

  def check_line([], []), do: {:valid, 0}

  def check_line([], remainder), do: {:incomplete, remainder}

  def check_line([line_h | line_r], []), do: check_line(line_r, [line_h])

  def check_line([line_h | line_r], [seen_h | seen_r] = seen) do
    case {seen_h, line_h} do
      {"(", ")"} -> check_line(line_r, seen_r)
      {"[", "]"} -> check_line(line_r, seen_r)
      {"{", "}"} -> check_line(line_r, seen_r)
      {"<", ">"} -> check_line(line_r, seen_r)
      {_, "("} -> check_line(line_r, [line_h | seen])
      {_, "["} -> check_line(line_r, [line_h | seen])
      {_, "{"} -> check_line(line_r, [line_h | seen])
      {_, "<"} -> check_line(line_r, [line_h | seen])
      _ -> {:corrupted, line_h}
    end
  end

  def find_corrupted(line) do
    check_line(line, [])
  end

  def part1() do
    get_input()
    |> Enum.map(&find_corrupted/1)
    |> Enum.filter(fn
      {:corrupted, _} -> true
      _ -> false
    end)
    |> Enum.map(fn {_, char} -> @scores[char] end)
    |> Enum.sum()
  end

  def find_incomplete(lines) do
    lines
    |> Enum.map(fn line -> check_line(line, []) end)
    |> Enum.filter(fn
      {:incomplete, _} -> true
      _ -> false
    end)
  end

  def score_completion(remainder) do
    remainder
    |> Enum.reduce(0, fn c, acc -> acc * 5 + @completion[c] end)
  end

  def part2() do
    scores =
      get_input()
      |> find_incomplete()
      |> Enum.map(fn {_, remainder} -> score_completion(remainder) end)
      |> Enum.sort()

    i = div(length(scores), 2)
    Enum.at(scores, i)
  end
end
