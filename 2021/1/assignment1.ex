defmodule Assignment1 do
  def get_input(filename \\ "aoc_2021_01_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  def part1() do
    get_input()
    |> Enum.chunk_every(2, 1, :discard)
    |> Enum.filter(fn [l, r] -> l < r end)
    |> length()
  end

  def part2() do
    get_input()
    |> Enum.chunk_every(3, 1, :discard)
    |> Enum.map(&Enum.sum/1)
    |> Enum.chunk_every(2, 1, :discard)
    |> Enum.filter(fn [l, r] -> l < r end)
    |> length()
  end
end
