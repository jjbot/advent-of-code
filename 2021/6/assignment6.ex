defmodule Assignment6 do
  def get_input(filename \\ "aoc_2021_06_input.txt") do
    filename
    |> File.read!()
    |> String.trim()
    |> String.split(",", trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  def update_fish(0), do: 6
  def update_fish(i), do: i - 1

  def step(fish) do
    counts = Enum.frequencies(fish)
    zeros = Map.get(counts, 0, 0)
    updated_fish = Enum.map(fish, &update_fish/1)
    updated_fish ++ List.duplicate(8, zeros)
  end

  def part1() do
    fish = get_input()
    Enum.reduce(1..80, fish, fn _i, fish -> step(fish) end)
  end

  def update_fish_dict(fish) do
    fish
    |> Enum.flat_map(fn {k, v} -> if k > 0, do: [{k - 1, v}], else: [{6, v}, {8, v}] end)
    |> Enum.reduce(%{}, fn {k, v}, acc -> Map.update(acc, k, v, fn v_old -> v_old + v end) end)
  end

  def part2() do
    fish = get_input() |> Enum.frequencies()

    Enum.reduce(1..256, fish, fn _i, fish -> update_fish_dict(fish) end)
    |> Map.values()
    |> Enum.sum()
  end
end
