defmodule Assignment5 do
  @line_re ~r"(\d+),(\d+) -> (\d+),(\d+)"

  def parse_line(line) do
    [x_start, y_start, x_end, y_end] =
      Regex.run(@line_re, line, capture: :all_but_first)
      |> Enum.map(&String.to_integer/1)

    {{x_start, y_start}, {x_end, y_end}}
  end

  def get_input(filename \\ "aoc_2021_05_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_line/1)
  end

  def generate_line({x, ys}, {x, ye}), do: for(y <- ys..ye, do: {x, y})
  def generate_line({xs, y}, {xe, y}), do: for(x <- xs..xe, do: {x, y})

  def generate_line({xs, ys}, {xe, ye}) do
    x_increment = if xs < xe, do: 1, else: -1
    y_increment = if ys < ye, do: 1, else: -1

    for inc <- 0..abs(xs - xe), do: {xs + inc * x_increment, ys + inc * y_increment}
  end

  def part1() do
    get_input()
    |> Enum.filter(fn {{xs, ys}, {xe, ye}} -> xs == xe || ys == ye end)
    |> Enum.flat_map(fn {start, stop} -> generate_line(start, stop) end)
    |> Enum.frequencies()
    |> Enum.filter(fn {_loc, count} -> count > 1 end)
    |> length()
  end

  def part2() do
    get_input()
    |> Enum.flat_map(fn {start, stop} -> generate_line(start, stop) end)
    |> Enum.frequencies()
    |> Enum.filter(fn {_loc, count} -> count > 1 end)
    |> length()
  end
end
