defmodule Assignment7 do
  def get_input(filename \\ "aoc_2021_07_input.txt") do
    filename
    |> File.read!()
    |> String.trim()
    |> String.split(",", trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  def total_distance(crabs, center) do
    crabs
    |> Enum.map(fn x -> abs(x - center) end)
    |> Enum.sum()
  end

  def part1(filename) do
    # Not it: 343122 too high
    crabs = get_input(filename)

    {small, large} =
      crabs
      |> Statistics.mean()
      |> IO.inspect()
      |> (fn x -> {Float.floor(x) |> Kernel.round(), Float.ceil(x) |> Kernel.round()} end).()

    distance_small = total_distance(crabs, small)
    distance_large = total_distance(crabs, large)

    {distance_small, distance_large}
  end

  def part1_weird(filename) do
    crabs = get_input(filename)

    Enum.map(0..1_000, fn i -> total_distance(crabs, i) end)
    |> Enum.min()
  end

  def total_expensive_distance(crabs, center, costs) do
    crabs
    |> Enum.map(fn x -> Map.get(costs, abs(x - center), 0) end)
    |> Enum.sum()
  end

  def part2_weird(filename) do
    crabs = get_input(filename)

    costs =
      Enum.map(0..5_000, fn i -> {i, Enum.sum(0..i)} end)
      |> Enum.into(%{})

    Enum.map(0..1_000, fn i -> total_expensive_distance(crabs, i, costs) end)
    |> Enum.min()
  end
end
