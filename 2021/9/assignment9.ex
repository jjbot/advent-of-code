defmodule Assignment9 do
  @neighbours [{0, -1}, {1, 0}, {0, 1}, {-1, 0}]

  def parse_line(line, y) do
    line
    |> String.split("", trim: true)
    |> Enum.with_index()
    |> Enum.map(fn {str, x} -> {{x, y}, String.to_integer(str)} end)
  end

  def get_input(filename \\ "aoc_2021_09_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.with_index()
    |> Enum.flat_map(fn {str, y} -> parse_line(str, y) end)
    |> Enum.into(%{})
  end

  def add_loc_offset({x, y} = _loc, {dx, dy} = _inc), do: {x + dx, y + dy}

  def is_low_point?({loc, value}, board) do
    @neighbours
    |> Enum.all?(fn delta ->
      l = add_loc_offset(loc, delta)
      Map.get(board, l, 10) > value
    end)
  end

  def part1() do
    board = get_input()

    board
    |> Enum.filter(&is_low_point?(&1, board))
    |> IO.inspect()
    |> Enum.map(fn {_loc, value} -> value + 1 end)
    |> Enum.sum()
  end

  def grow_basin(todo, done, board) do
    case MapSet.to_list(todo) do
      [] ->
        done

      [point | remainder] ->
        new_todo = MapSet.new(remainder)

        new_todo =
          @neighbours
          |> Enum.map(&add_loc_offset(point, &1))
          |> Enum.filter(fn loc ->
            Map.get(board, loc, 9) < 9 && !MapSet.member?(todo, loc) && !MapSet.member?(done, loc)
          end)
          |> Enum.reduce(new_todo, fn point, new_todo -> MapSet.put(new_todo, point) end)

        grow_basin(new_todo, MapSet.put(done, point), board)
    end
  end

  def find_basins(low_points, board) do
    low_points
    |> Enum.map(fn point -> grow_basin(MapSet.new([point]), MapSet.new(), board) end)
  end

  def part2() do
    board = get_input()

    low_points =
      board
      |> Enum.filter(&is_low_point?(&1, board))
      |> Enum.map(fn {loc, _value} -> loc end)

    find_basins(low_points, board)
    |> Enum.map(&MapSet.size/1)
    |> Enum.sort(:desc)
    |> Enum.take(3)
    |> Enum.reduce(1, fn x, acc -> x * acc end)
  end
end
