defmodule Assignment2 do
  @line_re ~r"(\w+) (\d+)"

  def parse_line(line) do
    [direction, value] = Regex.run(@line_re, line, capture: :all_but_first)
    {direction, String.to_integer(value)}
  end

  def move("forward", value, {x, y}), do: {x + value, y}

  def move("up", value, {x, y}), do: {x, y - value}

  def move("down", value, {x, y}), do: {x, y + value}

  def get_input(filename \\ "aoc_2021_02_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(fn line -> parse_line(line) end)
  end

  def part1() do
    {x, y} =
      get_input()
      |> Enum.reduce({0, 0}, fn {direction, value}, acc -> move(direction, value, acc) end)

    x * y
  end

  def target("forward", value, {x, y, aim}), do: {x + value, (value * aim) + y, aim}

  def target("up", value, {x, y, aim}), do: {x, y, aim - value}

  def target("down", value, {x, y, aim}), do: {x, y, aim + value}

  def part2() do
    {x, y, _aim} =
      get_input()
      |> Enum.reduce({0, 0, 0}, fn {direction, value}, acc -> target(direction, value, acc) end)

    x * y
  end
end
