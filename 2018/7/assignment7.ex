defmodule Worker do
  @enforce_keys [:letter, :steps_remaining]
  defstruct [:letter, :steps_remaining]

  def step(worker) do
    case worker.steps_remaining - 1 do
      steps when steps == 0 ->
        {:done, worker.letter}
      steps ->
        {:cont, %{worker | steps_remaining: steps}}
    end
  end
end

defmodule Assignment7 do
  @line_rg ~r"Step (\w) must be finished before step (\w) can begin."
  @letters "ABCDEFGHIJKLMNOPQRSTUVWXYZ" |> String.graphemes() |> Enum.with_index(1) |> Enum.into(%{})
  @workers 5

  def parse_line(line) do
    [bef, af] = Regex.run(@line_rg, line, capture: :all_but_first)
    {bef, af}
  end

  def get_input(filename \\ "aoc_2018_07_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_line/1)
  end

  def convert_to_requires_map(node_list) do
    node_list
    |> Enum.reduce(
      %{},
      fn {bef, af}, acc -> 
        Map.update(acc, af, [bef], fn existing -> [bef | existing] end)
      end
    )
    |> Enum.reduce(%{}, fn {bef, af}, acc -> Map.put(acc, bef, MapSet.new(af)) end)
  end

  def get_seed(mapping) do
    targets = MapSet.new(Map.keys(mapping))
    sources =
      mapping
      |> Map.values()
      |> Enum.reduce(MapSet.new(), fn e, acc -> MapSet.union(acc, e) end)

    MapSet.difference(sources, targets)
  end

  def resolve(solved, available, mapping) do
    if MapSet.size(available) == 0 do
      solved
    else
      [n | _] =
        available
        |> MapSet.to_list()
        |> Enum.sort()

      solved = [n | solved]
      solved_set = MapSet.new(solved)
      available = MapSet.delete(available, n)

      {available, mapping} =
        Enum.reduce(
          mapping,
          {available, mapping},
          fn {af, bef}, {available, mapping} ->
            if MapSet.subset?(bef, solved_set) do
              {MapSet.put(available, af), Map.delete(mapping, af)}
            else
              {available, mapping}
            end
          end
        )

      resolve(solved, available, mapping)
    end
  end


  def part1() do
    mapping = get_input() |> convert_to_requires_map()

    seed = get_seed(mapping)

    resolve([], seed, mapping)
    |> Enum.reverse()
    |> Enum.join("")
  end

  def update_workers(workers) do
    workers
    |> Enum.reduce(
      {[], []},
      fn worker, {letters, workers} ->
        case Worker.step(worker) do
          {:done, letter} -> {[letter | letters], workers}
          {:cont, worker} -> {letters, [worker | workers]}
        end
      end
    )
  end

  def start_workers(workers, []), do: {workers, []}

  def start_workers(workers, letters) when length(workers) >= @workers, do: {workers, letters}

  def start_workers(workers, [letter | letters_remaining]) do
    worker = %Worker{letter: letter, steps_remaining: 60 + @letters[letter]}
    start_workers([worker | workers], letters_remaining)
  end

  def step(solved, available, mapping, current_step, workers)

  def step(_, [], _, current_step, []), do: current_step

  def step(solved, available, mapping, current_step, workers) do
    {workers, available} = start_workers(workers, available)

    {finished_letters, workers} = update_workers(workers)
    solved = finished_letters ++ solved
    solved_set = MapSet.new(solved)

    {available, mapping} =
      Enum.reduce(
        mapping,
        {available, mapping},
        fn {af, bef}, {available, mapping} ->
          if MapSet.subset?(bef, solved_set) do
            {[af | available], Map.delete(mapping, af)}
          else
            {available, mapping}
          end
        end
      )

    step(solved, Enum.sort(available), mapping, current_step + 1, workers)
  end

  def part2() do
    mapping = get_input() |> convert_to_requires_map()

    seed = get_seed(mapping) |> MapSet.to_list() |> Enum.sort()

    step([], seed, mapping, 0, [])
  end
end
