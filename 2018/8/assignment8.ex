defmodule Assignment8 do
  def read_nodes(seq, nodes, 0), do: {seq, nodes}

  def read_nodes(seq, nodes, amount) do
    [child_count, meta_count | seq] = seq

    {seq, nodes} = read_nodes(seq, nodes, child_count)

    {metadata, seq} = Enum.split(seq, meta_count)

    read_nodes(seq, [metadata | nodes], amount - 1)
  end

  def get_input(filename \\ "aoc_2018_08_input.txt") do
    filename
    |> File.read!()
    |> String.trim()
    |> String.split(" ", trim: true)
    |> Enum.map(&String.to_integer/1)
  end

  def part1() do
    get_input()
    |> read_nodes([], 1)
    |> elem(1)
    |> Enum.flat_map(& &1)
    |> Enum.sum()
  end

  def read_nodes2(seq, sibling_nodes, 0), do: {seq, sibling_nodes}

  def read_nodes2(seq, sibling_nodes, amount) do
    [child_count, meta_count | seq] = seq

    {seq, child_nodes} = read_nodes2(seq, [], child_count)

    {metadata, seq} = Enum.split(seq, meta_count)

    child_nodes =
      child_nodes
      |> Enum.reverse()
      |> Enum.with_index(1)
      |> Enum.into(%{}, fn {e, i} -> {i, e} end)

    value =
      case child_count do
        0 -> Enum.sum(metadata)
        nr when nr > 0 ->
          metadata
          |> Enum.map(&Map.get(child_nodes, &1, 0))
          |> Enum.sum()
      end

    read_nodes2(seq, [value | sibling_nodes], amount - 1)
  end

  def part2() do
    get_input()
    |> read_nodes2([], 1)
    |> elem(1)
    |> List.first()
  end
end
