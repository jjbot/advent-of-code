defmodule Assignment6 do
  def split_line(line) do
    [x, y] = String.split(line, ", ", trim: true)
    {String.to_integer(x), String.to_integer(y)}
  end

  def get_input(filename \\ "aoc_2018_06_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&split_line/1)
  end

  def get_ranges(points) do
    {{min_x, _}, {max_x, _}} = Enum.min_max_by(points, fn {x, _y} -> x end)
    {{_, min_y}, {_, max_y}} = Enum.min_max_by(points, fn {_x, y} -> y end)
    
    {min_x, max_x, min_y, max_y}
  end

  def distance({x1, y1} = _location, {x2, y2} = _point) do
    abs(x1 - x2) + abs(y1 - y2)
  end

  def distance({x1, y1} = _location, {{x2, y2}, nr} = _point) do
    {abs(x1 - x2) + abs(y1 - y2), nr}
  end

  def get_closest_points(location, points) do
    distances =
      points
      |> Enum.with_index()
      |> Enum.map(&distance(location, &1))

    {min_distance, _} = Enum.min_by(distances, fn {distance, _nr} -> distance end)

    case Enum.filter(distances, fn {distance, _nr} -> distance == min_distance end) do
      [{_, nr}] -> {location, nr}
      _ -> {location, "."}
    end
  end

  def contained({x, y}, min_x, max_x, min_y, max_y) do
    x > min_x && x < max_x && y > min_y && y < max_y
  end

  def find_area(areas, min_x, max_x, min_y, max_y) do
    areas
    |> Enum.filter(fn {nr, points} -> Enum.all?(points, &contained(&1, min_x, max_x, min_y, max_y)) end)
  end

  def part1() do
    input = get_input()
    {min_x, max_x, min_y, max_y} = get_ranges(input)

    locations = for x <- min_x..max_x, y <- min_y..max_y, do: {x, y}

    locations
    |> Enum.map(&get_closest_points(&1, input))
    |> Enum.reduce(%{}, fn {location, nr}, acc -> Map.update(acc, nr, [location], fn existing -> [location | existing] end) end)
    |> Enum.sort_by(fn {nr, locations} -> length(locations) end)
    |> Enum.reverse()
    |> find_area(min_x, max_x, min_y, max_y)
    |> (fn [{_nr, locations} | _] -> length(locations) end).()
  end

  def get_total_distance(point, locations) do
    locations
    |> Enum.map(fn location -> distance(location, point) end)
    |> Enum.sum()
  end

  def part2() do
    input = get_input()
    {min_x, max_x, min_y, max_y} = get_ranges(input)

    locations = for x <- min_x..max_x, y <- min_y..max_y, do: {x, y}

    locations
    |> Enum.map(&get_total_distance(&1, input))
    |> Enum.filter(fn distance -> distance < 10_000 end)
    |> length()
  end
end
