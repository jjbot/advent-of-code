defmodule Assignment2 do
  def process_id(id) do
    id
    |> String.codepoints()
    |> Enum.frequencies()
    |> Enum.reduce(%{}, fn {letter, count}, counts -> Map.update(counts, count, [letter], fn existing -> [letter | existing] end) end)
    |> Enum.reduce(%{}, fn {letter_count, letters}, acc -> Map.put(acc, letter_count, length(letters)) end)
  end

  def multiply(%{2 => twos, 3 => threes}), do: twos * threes

  def get_input(filename \\ "aoc_2018_02_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
  end

  def part1() do
    get_input()
    |> Stream.map(&process_id/1)
    |> Enum.reduce(
      %{2 => 0, 3 => 0},
      fn letter_counts, acc ->
        case {letter_counts[2], letter_counts[3]} do
          {twos, threes} when is_integer(twos) and is_integer(threes) ->
            Map.update(acc, 2, 1, &(&1 + 1))
            |> Map.update(3, 1, &(&1 + 1))

          {twos, nil} when is_integer(twos) ->
            Map.update(acc, 2, 1, &(&1 + 1))

          {_, threes} when is_integer(threes) ->
            Map.update(acc, 3, 1, &(&1 + 1))

          _ ->
            acc
        end
      end)
    |> multiply()
  end

  def join(difference) do
    {[l, r], _} = Keyword.pop_values(difference, :eq)
    l <> r
  end

  def part2() do
    ids = get_input()

    combis = for x <- ids, y <- ids, do: String.myers_difference(x, y)

    combis
    |> Enum.filter(&(is_list(&1)))
    |> Enum.filter(&(length(&1) == 4))
    |> Enum.filter(
      fn l ->
        {:ins, l} = List.keyfind(l, :ins, 0)
        String.length(l) == 1
      end)
    |> List.first()
    |> join()
  end
end
