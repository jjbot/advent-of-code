defmodule Cart do
  defstruct [:direction, :position, :ncd]

  def new(location, direction_char) do
    direction =
      case direction_char do
        ">" -> :east
        "<" -> :west
        "^" -> :north
        "v" -> :south
      end

    %Cart{direction: direction, position: location, ncd: :left}
  end

  def get_cart_update(track, direction, ncd) do
    case {track, direction, ncd} do
      {"|", direction, ncd} -> {direction, ncd}
      {"-", direction, ncd} -> {direction, ncd}
      {"/", :north, ncd} -> {:east, ncd}
      {"/", :east, ncd} -> {:north, ncd}
      {"/", :south, ncd} -> {:west, ncd}
      {"/", :west, ncd} -> {:south, ncd}
      {"\\", :north, ncd} -> {:west, ncd}
      {"\\", :east, ncd} -> {:south, ncd}
      {"\\", :south, ncd} -> {:east, ncd}
      {"\\", :west, ncd} -> {:north, ncd}
      {"+", :north, :left} -> {:west, :straight}
      {"+", :north, :straight} -> {:north, :right}
      {"+", :north, :right} -> {:east, :left}
      {"+", :east, :left} -> {:north, :straight}
      {"+", :east, :straight} -> {:east, :right}
      {"+", :east, :right} -> {:south, :left}
      {"+", :south, :left} -> {:east, :straight}
      {"+", :south, :straight} -> {:south, :right}
      {"+", :south, :right} -> {:west, :left}
      {"+", :west, :left} -> {:south, :straight}
      {"+", :west, :straight} -> {:west, :right}
      {"+", :west, :right} -> {:north, :left}
    end
  end

  def move(cart, tracks) do
    new_pos = get_next_position(cart)

    {new_dir, new_ncd} = get_cart_update(tracks[new_pos], cart.direction, cart.ncd)

    %{cart | position: new_pos, direction: new_dir, ncd: new_ncd}
  end

  defp get_next_position(cart)
  defp get_next_position(%Cart{position: {x, y}, direction: :east}), do: {x + 1, y}
  defp get_next_position(%Cart{position: {x, y}, direction: :west}), do: {x - 1, y}
  defp get_next_position(%Cart{position: {x, y}, direction: :north}), do: {x, y - 1}
  defp get_next_position(%Cart{position: {x, y}, direction: :south}), do: {x, y + 1}
end

defmodule World do
  defstruct [:tracks, :carts]

  def check_collision(cart, carts) do
    collisions =
      carts
      |> Enum.map(& &1.position)
      |> Enum.filter(fn loc -> loc == cart.position end)

    case collisions do
      [] -> false
      [collision_location] -> {true, collision_location}
    end
  end

  @doc """
  Carts need to be sorted by the current row they are on, and then on the column.
  """
  def order_carts(carts), do: Enum.sort_by(carts, fn %Cart{position: {x, y}} -> {y, x} end)

  def move_carts([], moved, _tracks), do: {:done, moved}

  def move_carts([cart | to_move], moved, tracks) do
    cart = Cart.move(cart, tracks)

    case check_collision(cart, to_move ++ moved) do
      {true, location} ->
        {:collision, location}

      false ->
        move_carts(to_move, [cart | moved], tracks)
    end
  end

  def tick(world) do
    carts = order_carts(world.carts)
    move_carts(carts, [], world.tracks)
  end

  def move_carts2([], moved, _tracks), do: {:done, moved}

  def move_carts2([cart | to_move], moved, tracks) do
    cart = Cart.move(cart, tracks)

    case check_collision(cart, to_move ++ moved) do
      {true, location} ->
        to_move = Enum.reject(to_move, fn cart -> cart.position == location end)
        moved = Enum.reject(moved, fn cart -> cart.position == location end)
        move_carts2(to_move, moved, tracks)

      false ->
        move_carts2(to_move, [cart | moved], tracks)
    end
  end

  def tick2(world) do
    {:done, carts} =
      world.carts
      |> order_carts()
      |> move_carts2([], world.tracks)

    %{world | carts: carts}
  end
end

defmodule Assignment13 do
  def get_input(filename \\ "aoc_2018_13_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.codepoints/1)
    |> Enum.with_index()
    |> Enum.reduce(
      %{},
      fn {line, line_nr}, acc ->
        line
        |> Enum.with_index()
        |> Enum.reduce(acc, fn {value, col_nr}, acc -> Map.put(acc, {col_nr, line_nr}, value) end)
      end
    )
  end

  def replace_carts(tracks, locations, track) do
    locations
    |> Enum.reduce(tracks, fn {location, _}, tracks -> Map.put(tracks, location, track) end)
  end

  def get_world(tracks) do
    to_east = tracks |> Enum.filter(fn {_, v} -> v == ">" end)
    to_west = tracks |> Enum.filter(fn {_, v} -> v == "<" end)
    to_north = tracks |> Enum.filter(fn {_, v} -> v == "^" end)
    to_south = tracks |> Enum.filter(fn {_, v} -> v == "v" end)

    tracks =
      tracks
      |> replace_carts(to_east, "-")
      |> replace_carts(to_west, "-")
      |> replace_carts(to_north, "|")
      |> replace_carts(to_south, "|")

    carts =
      (to_east ++ to_west ++ to_north ++ to_south)
      |> Enum.map(fn {location, direction} -> Cart.new(location, direction) end)

    %World{tracks: tracks, carts: carts}
  end

  def find_collision(world) do
    Enum.reduce_while(
      1..1_000,
      world,
      fn _i, world ->
        case World.tick(world) do
          {:collision, location} -> {:halt, location}
          {:done, moved} -> {:cont, %{world | carts: moved}}
        end
      end
    )
  end

  def part1() do
    # Aswer: 26,92
    "aoc_2018_13_input.txt"
    |> get_input()
    |> get_world()
    |> find_collision()
  end

  def find_last_position(world) do
    Enum.reduce_while(
      1..100_000,
      world,
      fn _i, world ->
        world = World.tick2(world)

        case length(world.carts) do
          1 ->
            {:halt, world}

          _ ->
            {:cont, world}
        end
      end
    )
  end

  def part2() do
    # Not it: 9,33
    # Not it: 8,33
    # Not it: 10,33
    # Not it: 86,19
    "aoc_2018_13_input.txt"
    # "example.txt"
    |> get_input()
    |> get_world()
    |> find_last_position()
  end
end
