defmodule Assignment11 do
  @input 9306

  def get_power_level({x, y}, serial \\ @input) do 
    rack_id = x + 10
    power_level = ((rack_id * y) + serial) * rack_id
    rem(power_level, 1_000) |> div(100) |> Kernel.-(5)
  end

  def generate_grid() do
    power_levels = for x <- 1..300, y <- 1..300, do: {{x, y}, get_power_level({x, y})}
    Enum.into(power_levels, %{})
  end

  def sub_power(grid, {x, y}, size) do
    mini_grid = for dx <- 0..size - 1, dy <- 0..size - 1, do: {dx, dy}
    Enum.reduce(mini_grid, 0, fn {dx, dy}, acc -> acc + Map.get(grid, {x + dx, y + dy}, 0) end)
  end

  def part1() do
    # Answer: 235,38
    grid = generate_grid()
    sub_grids = for x <- 1..298, y <- 1..298, do: {{x, y}, sub_power(grid, {x, y}, 3)}
    Enum.max_by(sub_grids, fn {_coord, power} -> power end)
  end

  def find_best_sub_grid(grid, size) do
    IO.puts(size)
    limit = 300 - size + 1
    to_check = for x <- 1..limit, y <- 1..limit, do: {x, y}

    to_check
    |> Enum.map(fn coord -> {coord, sub_power(grid, coord, size), size} end)
    |> Enum.max_by(fn {_coord, power, _size} -> power end)
    |> IO.inspect()
  end

  def part2() do
    # Yeah, done with brute force
    # Answer: 233,146,13
    # How do you make this faster? Not sure if the Task.async helps or hinders.
    grid = generate_grid()
    
    1..300
    |> Enum.map(fn size -> Task.async(fn -> find_best_sub_grid(grid, size) end) end)
    |> Enum.map(&Task.await(&1, :infinity))
    |> Enum.max_by(fn {_coord, power, _size} -> power end)
  end
end
