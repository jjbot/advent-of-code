defmodule Assignment5 do
  @chars "abcdefghijklmnopqrstuvwxyz"
  |> String.graphemes()
  |> Enum.flat_map(fn c -> [{c, String.upcase(c)}, {String.upcase(c), c}] end)
  |> Enum.into(%{})

  def get_chars() do
    @chars
  end

  def react(polymer), do: react(polymer, [], length(polymer))

  def react([], reduced, l) do
    if length(reduced) == l do
      reduced
    else
      react(reduced, [], length(reduced))
    end
  end

  def react([h | []], reduced, l) do
    react([], [h | reduced], l)
  end

  def react([h1 | [h2 | tail2] = tail1], reduced, l) do
    if h1 == @chars[h2] do
      react(tail2, reduced, l)
    else
      react(tail1, [h1 | reduced], l)
    end
  end

  def get_input(filename \\ "aoc_2018_05_input.txt") do
    filename
    |> File.read!()
    |> String.trim()
    |> String.graphemes()
  end

  def part1() do
    get_input()
    |> react()
  end

  def remove_and_react(letter, input) do
    IO.puts(letter)

    input
    |> Enum.reject(fn c -> c == letter || c == @chars[letter] end)
    |> react()
    |> (fn result -> {letter, result} end).()
  end

  def part2() do
    input = get_input()
    "abcdefghijklmnopqrstuvwxyz"
    |> String.graphemes()
    |> Enum.map(&remove_and_react(&1, input))
  end
end
