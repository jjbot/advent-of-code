from collections import namedtuple, defaultdict
from operator import attrgetter
import re

Rectangle = namedtuple("Rectangle", "id, bx, by, dx, dy")
PATTERN = re.compile("#(?P<id>\d+)\s@\s(?P<x>\d+),(?P<y>\d+):\s(?P<dx>\d+)x(?P<dy>\d+)")

def parse_line(line):
    m = PATTERN.match(line)
    return (
            m.group("id"),
            int(m.group("x")),
            int(m.group("y")),
            int(m.group("dx")),
            int(m.group("dy")),
            )


def read_input(fn):
    pieces = []
    with open(fn) as f:
        for line in f:
            i, x, y, dx, dy = parse_line(line)
            pieces.append(Rectangle(i, x, y, dx, dy))
    return pieces


def main():
    rectangles = read_input("input.txt")

    d = defaultdict(int)

    for r in rectangles:
        for dx in range(r.dx):
            for dy in range(r.dy):
                d[r.bx + dx, r.by + dy] += 1

    overlap = 0
    for value in d.values():
        if value > 1:
            overlap += 1
    print(overlap)


if __name__ == "__main__":
    main()
