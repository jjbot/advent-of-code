data = open("ad3.txt", "r")

# data = ["#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"]

bulk = {}
patches = []

for d in data:
    claim = d.rstrip()

    cf = claim.find("@")
    cs = claim.find(":")

    coords = claim[cf+1:cs].split(",")
    size = claim[cs+1:].split("x")

    patch = []
    for i in range(int(coords[0]), int(coords[0]) + int(size[0])):
        for j in range(int(coords[1]), int(coords[1]) + int(size[1])):
            patch.append((i, j))

    bulk[claim[1:cf-1]] = patch
    patches.append(patch)

data.close()


# PART I

def overlap_patches(p1, p2):
    pset1 = set(p1)
    pset2 = set(p2)

    overlap = pset1 & pset2
#    print(len(overlap))
    return overlap


x = len(patches)
overlaps = set()

for i in range(x):
    for j in range(i+1,x):
        print(i, len(patches[i]))
        print(j, len(patches[j]))
        overlap = overlap_patches(patches[i], patches[j])
        if len(overlap) > 0:
            print(len(overlap))
            for s in overlap:
                overlaps.add(s)
        else:
            print("boo")
#        print(overlaps)
        print()

# resposta: 101781

# PART II

data = open("ad3.txt", "r")

bulk = {}
patches = []

for d in data:
    claim = d.rstrip()
    
    cf = claim.find("@")
    cs = claim.find(":")
    
    coords = claim[cf+1:cs].split(",")
    size = claim[cs+1:].split("x")
    
    patch = []
    for i in range(int(coords[0]), int(coords[0]) + int(size[0])):
        for j in range(int(coords[1]), int(coords[1]) + int(size[1])):
            patch.append((i,j))
    
    bulk[claim[1:cf-1]] = patch
    patches.append(patch)
    
data.close()


data = open("ad3overlaps.txt", "r")

overlaps = []

for d in data:
    o = d.rstrip()
   
data.close()

ddd = o.split(")(")
# I did this to avoid having to process the long thing again

new_overlaps = []

for d in ddd:
    xx = d.split(", ")
    yy = (int(xx[0]), int(xx[1]))
    new_overlaps.append(yy)


for claim in bulk.keys():
    print(claim)
    not_found = True
    for item in bulk[claim]:
        if item in new_overlaps:
            not_found = False
    if not_found == True:
        print(">>>>", claim)
        break
# result: 909 
