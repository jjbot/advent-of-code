from collections import namedtuple
from operator import attrgetter
import re

Rectangle = namedtuple("Rectangle", "id, bx, by, tx, ty")
PATTERN = re.compile("#(?P<id>\d+)\s@\s(?P<x>\d+),(?P<y>\d+):\s(?P<dx>\d+)x(?P<dy>\d+)")


def parse_line(line):
    m = PATTERN.match(line)
    return (
            m.group("id"),
            int(m.group("x")),
            int(m.group("y")),
            int(m.group("dx")),
            int(m.group("dy")),
            )


def read_input(fn):
    pieces = []
    with open(fn) as f:
        for line in f:
            i, x, y, dx, dy = parse_line(line)
            pieces.append(Rectangle(i, x, y, x + dx, y + dy))
    return pieces


def has_overlap(r1, r2):
    if r1.id == r2.id:
        return False

    if r1.tx <= r2.bx or r2.tx <= r1.bx:
        return False

    if r1.ty <= r2.by or r2.ty <= r1.by:
        return False

    return True


def main():
    rectangles = read_input("input.txt")
    rectangles = sorted(rectangles, key=attrgetter("bx"))
    
    total = 0
    for r1 in rectangles:
        for r2 in rectangles:
            if has_overlap(r1, r2):
                x_dist = min(r1.tx, r2.tx) - max(r1.bx, r2.bx)
                y_dist = min(r1.ty, r2.ty) - max(r1.by, r2.by)
                overlap = (x_dist * y_dist)
                print(f"{r1} - {r2} - {overlap}")
                total += overlap
    print(total)
    print(total / 2)

if __name__ == "__main__":
    main()
