defmodule Rectangle do
  defstruct [:id, :x, :y, :dx, :dy]
end

defmodule Solution do
  @pattern ~r"#(\d+)\s@\s(\d+),(\d+):\s(\d+)x(\d+)"

  def parse_line(line) do
    [_, id, x, y, dx, dy] = Regex.run(@pattern, line)
    %Rectangle{
      id: String.to_integer(id),
      x: String.to_integer(x),
      y: String.to_integer(y),
      dx: String.to_integer(dx),
      dy: String.to_integer(dy)
    }
  end

  def generate_coordinates(rectangle) do
    for dx <- 0..rectangle.dx - 1, dy <- 0..rectangle.dy - 1 do
      {rectangle.x + dx, rectangle.y + dy}
    end
  end

  def run() do
    File.stream!("input.txt")
    |> Stream.map(&parse_line/1)
    |> Stream.map(&generate_coordinates/1)
    |> Stream.flat_map(&(&1))
    |> Enum.reduce(%{}, fn key, acc -> Map.update(acc, key, 1, &(&1 + 1)) end)
    |> Stream.filter(fn {_, count} -> count > 1 end)
    |> Enum.count()
  end
end
