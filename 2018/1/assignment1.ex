defmodule Assignment1 do
  def part1() do
    "aoc_2018_01_input.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_integer/1)
    |> Enum.reduce(0, fn i, acc -> acc + i end)
  end

  def part2() do
    "aoc_2018_01_input.txt"
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&String.to_integer/1)
    |> Stream.cycle()
    |> Enum.reduce_while(
      {0, MapSet.new([0])},
      fn i, {acc, seen} ->
        n = acc + i
        if MapSet.member?(seen, n) do
          {:halt, n}
        else
          {:cont, {n, MapSet.put(seen, n)}}
        end
      end)
  end
end
