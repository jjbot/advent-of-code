defmodule Assignment4 do
  # @line_re ~r"\[(.*)\s(\d+)\:(\d+)\]\s(.*)"
  @line_re ~r"\[(.*)\]\s(.*)"
  @guard_starts_re ~r"Guard \#(\d+) begins shift"

  def parse_activity(message) do
    case message do
      "wakes up" ->
        :wakes_up

      "falls asleep" ->
        :falls_asleep

      message ->
        [guard_nr] = Regex.run(@guard_starts_re, message, capture: :all_but_first)
        {:starts, String.to_integer(guard_nr)}
    end
  end

  def parse_record(line) do
    [dt, message] = Regex.run(@line_re, line, capture: :all_but_first)
    dt = NaiveDateTime.from_iso8601!(dt <> ":00")
    activity = parse_activity(message)
    {dt, activity}
  end

  def get_input(filename \\ "aoc_2018_04_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&parse_record/1)
    |> Enum.sort_by(&elem(&1, 0), NaiveDateTime)
  end

  def chunk_fun(el, []) do
    {:cont, [el]}
  end

  def chunk_fun(el, acc) do
    if is_tuple(elem(el, 1)) do
      {:cont, Enum.reverse(acc), [el]}
    else
      {:cont, [el | acc]}
    end
  end

  def after_fun(acc), do: {:cont, Enum.reverse(acc), []}

  def sleep_time(sleeps) do
    sleeps
    |> Enum.reduce(0, fn {s, w}, acc -> acc + (w - s) end)
  end

  def sleep_minutes(sleeps) do
    sleeps
    |> Enum.flat_map(fn {s, w} -> s..w-1 end)
    |> Enum.frequencies()
  end

  def get_score({guard, _time, minutes}) do
    {max_minute, _count} = Enum.max_by(minutes, fn {_k, v} -> v end)
    guard * max_minute
  end

  def part1() do
    get_input()
    # Chunk the input into nights where one guard is being monitored
    |> Enum.chunk_while([], &chunk_fun/2, &after_fun/1)
    # Chunk the (already sorted) falling asleep and waking up times so they can be easily compared
    |> Enum.map(fn [{_, {_, guard}} | tail] -> {guard, Enum.chunk_every(tail, 2)} end)
    # Join all the entries for each of the guards together.
    |> Enum.reduce(%{}, fn {guard, cycles}, acc -> Map.update(acc, guard, cycles, fn existing -> existing ++ cycles end) end)
    # Convert the DateTimes to minutes
    |> Enum.map(fn {guard, cycles} -> {guard, Enum.map(cycles, fn [s, w] -> {elem(s, 0).minute, elem(w, 0).minute} end)} end)
    # Run the sleep_time calculation and count of the minutes a guard is asleep
    |> Enum.map(fn {guard, sleeps} -> {guard, sleep_time(sleeps), sleep_minutes(sleeps)} end)
    # Find the guard with the longest sleep time
    |> Enum.max_by(&elem(&1, 1))
    # Calculate the score
    |> get_score()
  end

  def part2() do
    get_input()
    # Check the input into nights where one guard is being monitored
    |> Enum.chunk_while([], &chunk_fun/2, &after_fun/1)
    # Chunk the (already sorted) falling asleep and waking up times so they can be compared
    |> Enum.map(fn [{_, {_, guard}} | tail] -> {guard, Enum.chunk_every(tail, 2)} end)
    # Join all the entries for each of the guards together
    |> Enum.reduce(%{}, fn {guard, cycles}, acc -> Map.update(acc, guard, cycles, fn existing -> existing ++ cycles end) end)
    # Convert the date time to minutes
    |> Enum.map(fn {guard, cycles} -> {guard, Enum.map(cycles, fn [s, w] -> {elem(s, 0).minute, elem(w, 0).minute} end)} end)
    # Get the amount of counts of the minutes the guards where asleep
    |> Enum.map(fn {guard, sleeps} -> {guard, sleep_minutes(sleeps)} end)
    # Remove the records of the guards that didn't sleep
    |> Enum.reject(fn {_guard, sleeps} -> Enum.empty?(sleeps) end)
    # Find minute for each guard that they slept most
    |> Enum.map(fn {guard, sleeps} -> {guard, Enum.max_by(sleeps, fn {_k, v} -> v end)} end)
    # Compare the guards and find the one that slept most in a single minute
    |> Enum.max_by(fn {_guard, {_minute, count}} -> count end)
    # Calculate the final score
    |> (fn {guard, {minute, _count}} -> guard * minute end).()
  end
end
