defmodule Star do
  @fields_regex ~r"<([\d | \s | , | -]+)>"
  @nr_regex ~r"(-?\d+)"
  defstruct position: {0, 0}, velocity: {0, 0}

  def distance(star1, star2)
  def distance(%Star{position: {x1, y1}}, %Star{position: {x2, y2}}) do
    max(x1, x2) - min(x1, x2)
  end

  def find_zero_step_count(%Star{position: {x, y}, velocity: {dx, dy}}) do
    max(
      abs(div(x, dx)),
      abs(div(y, dy))
    )
  end

  def from_line(line) do
    [position, velocity] =
      line
      |> extract_fields()
      |> Enum.map(&extract_nrs/1)

    %Star{position: position, velocity: velocity}
  end

  def move(star)
  def move(%Star{position: p, velocity: v} = star), do: %{star | position: add(p, v)}

  def move_many(star, steps)
  def move_many(%Star{position: p, velocity: {dx, dy}} = star, steps) do
    %{star | position: add(p, {dx * steps, dy * steps})}
  end

  defp add(position, velocity)
  defp add({x, y}, {dx, dy}), do: {x + dx, y + dy}

  defp extract_fields(line) do
    Regex.scan(@fields_regex, line, capture: :all_but_first)
    |> Enum.flat_map(& &1)
  end

  defp extract_nrs(s) do
    [x, y] =
      Regex.scan(@nr_regex, s, capture: :all_but_first)
      |> Enum.flat_map(& &1)
      |> Enum.map(&String.to_integer/1)
    {x, y}
  end
end

defmodule Assignment10 do
  def get_input(filename \\ "aoc_2018_10_input.txt") do
    filename
    |> File.read!()
    |> String.split("\n", trim: true)
    |> Enum.map(&Star.from_line/1)
  end

  def get_max_distance(star, stars) do
    stars
    |> Enum.map(&Star.distance(&1, star))
    |> Enum.max()
  end

  def get_max_distance(stars) do
    stars
    |> Enum.map(&get_max_distance(&1, stars))
    |> Enum.max()
  end

  def get_print_ranges(stars) do
    x_positions = Enum.map(stars, fn star -> star.position |> elem(0) end)
    y_positions = Enum.map(stars, fn star -> star.position |> elem(1) end)

    x_range = Enum.min_max(x_positions)
    y_range = Enum.min_max(y_positions)
    {x_range, y_range}
  end

  def print_line(positions, y, {x_min, x_max}) do
    Enum.map(
      x_min..x_max,
      fn x -> Map.get(positions, {x, y}, " ") end
    )
    |> Enum.join("")
    |> IO.puts()
  end

  def print_stars(stars) do
    {x_range, {y_min, y_max}} = get_print_ranges(stars)

    positions = Enum.reduce(stars, %{}, fn star, acc -> Map.put(acc, star.position, "*") end)

    Enum.each(y_min..y_max, fn y -> print_line(positions, y, x_range) end)
  end

  def move_all(stars), do: Enum.map(stars, &Star.move/1)

  def part1() do
    # Answers
    # part 1: JJXZHKFP
    # part 2: 10036 (below: min_steps + i - 2)
    stars = get_input()

    steps = Enum.map(stars, &Star.find_zero_step_count/1)
    min_steps = Enum.min(steps)
    stars = Enum.map(stars, fn star -> Star.move_many(star, min_steps) end)

    Enum.reduce_while(
      1..500,
      {10_000, stars},
      fn i, {dis, stars} ->
        IO.puts(i)
        distance = get_max_distance(stars)
        if distance > dis do
          IO.inspect({i, distance})
          {:halt, {min_steps, i, stars}}
        else
          print_stars(stars)
          IO.puts("\n\n\n")
          {:cont, {distance, move_all(stars)}}
        end
      end
    )
  end
end
