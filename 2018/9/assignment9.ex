defmodule Assignment9 do

  def add_marble(circle, marble, idx, l) do
    place_at = rem(idx + 1, l) + 1
    circle = List.insert_at(circle, place_at, marble)
    {circle, place_at}
  end

  def place_marble(circle, marble, idx, players, player, l) do
    case rem(marble, 23) do
      0 ->
        remove_idx = rem(idx + l - 7, l)
        players =
          players
          |> Map.update!(player, &(&1 + marble))
          |> Map.update!(player, fn old -> old + Enum.at(circle, remove_idx) end)

        circle = List.delete_at(circle, remove_idx)

        {circle, remove_idx, players, l - 1}

      _ ->
        {circle, new_idx} = add_marble(circle, marble, idx, l)
        {circle, new_idx, players, l + 1}
    end
  end

  def part1(nr_players, nr_marbles) do
    players = 1..nr_players |> Enum.map(& {&1, 0}) |> Enum.into(%{})
    circle = [0]

    Enum.reduce(
      1..nr_marbles,
      {circle, 0, players, 1},
      fn marble, {circle, idx, players, l} ->
        if rem(marble, 1_000_000) == 0, do: IO.puts(marble)
        player = rem(marble, nr_players) + 1
        place_marble(circle, marble, idx, players, player, l)
      end
    )
    |> elem(2)
    |> Enum.max_by(fn {_k, v} -> v end)
  end

  ##########################
  # Part 2
  # ########################

  def add_marble2(current_marble, value) do
    {_, nm, _} = Agent.get(current_marble, fn state -> state end)
    {_, nnm, _} = Agent.get(nm, fn state -> state end)

    {:ok, new_marble} = Agent.start(fn -> {value, nnm, nm} end)
    Agent.update(nm, fn {value, _cw, ccw} -> {value, new_marble, ccw} end)
    Agent.update(nnm, fn {value, cw, _ccw} -> {value, cw, new_marble} end)

    {0, new_marble}
  end

  def remove_marble(current_marble) do
    left =
      Enum.reduce(
        1..8,
        current_marble,
        fn _nr, cm ->
          {_value, _cw, ccw} = Agent.get(cm, & &1)
          ccw
        end
      )

    {_, to_remove, _} = Agent.get(left, & &1)
    {to_remove_value, right, _} = Agent.get(to_remove, & &1)

    Agent.stop(to_remove, :normal)

    Agent.update(left, fn {value, _cw, ccw} -> {value, right, ccw} end)
    Agent.update(right, fn {value, cw, _ccw} -> {value, cw, left} end)

    {to_remove_value, right}
  end

  def place_marble2(current_marble, value) do
    case rem(value, 23) do
      0 ->
        {removed_value, next_marble} = remove_marble(current_marble)
        {removed_value + value, next_marble}

      _ ->
        add_marble2(current_marble, value)
    end
  end

  def print_circle(agent) do
    {value, cw, _} = Agent.get(agent, & &1)
    values =
      Enum.reduce_while(
        1..100,
        {[value], cw},
        fn _nr, {values, agent} ->
          {v, cw, _} = Agent.get(agent, & &1)
          if v == value do
            {:halt, values}
          else
            {:cont, {[v | values], cw}}
          end
        end
      )

    values
    |> Enum.reverse()
    |> IO.inspect()
  end

  def part2(nr_players, nr_marbles) do
    players = 1..nr_players |> Enum.map(& {&1, 0}) |> Enum.into(%{})

    {:ok, zero} = Agent.start(fn -> {0, None, None} end)
    {:ok, one} = Agent.start(fn -> {1, None, None} end)
    {:ok, two} = Agent.start(fn -> {2, one, zero} end)

    Agent.update(zero, fn _ -> {0, two, one} end)
    Agent.update(one, fn _ -> {1, zero, two} end)

    {nn, players} =
      Enum.reduce(
        3..nr_marbles,
        {two, players},
        fn value, {current_marble, players} ->
          {additional_value, nn} = place_marble2(current_marble, value)
          players = Map.update!(players, rem(value, nr_players) + 1, &(&1 + additional_value))
          if rem(value, 100_000) == 0, do: IO.puts(value)
          # print_circle(zero)
          {nn, players}
        end
      )

    {zero, nn, players}

  end

end
